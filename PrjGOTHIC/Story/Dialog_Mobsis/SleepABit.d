
func void PC_Sleep(var int t)
{
	AI_StopProcessInfos(self);
	player_mobsi_production = MOBSI_NONE;
	self.aivar[AIV_INVINCIBLE] = FALSE;
	if(Wld_IsTime(0,0,t,0))
	{
		Wld_SetTime(t,0);
	}
	else
	{
		t += 24;
		Wld_SetTime(t,0);
	};
	Wld_StopEffect("DEMENTOR_FX");
	if(SC_IsObsessed == TRUE)
	{
		PrintScreen(PRINT_SleepOverObsessed,-1,-1,FONT_Screen,2);
	}
	else
	{
		PrintScreen(PRINT_SleepOver,-1,-1,FONT_Screen,2);
		hero.attribute[ATR_HITPOINTS] = hero.attribute[ATR_HITPOINTS_MAX];
		hero.attribute[ATR_MANA] = hero.attribute[ATR_MANA_MAX];
	};
	PrintGlobals(PD_ITEM_MOBSI);
	Npc_SendPassivePerc(hero,PERC_ASSESSENTERROOM,NULL,hero);
};

func void sleepabit_s1()
{
	var C_Npc her;
	var C_Npc rock;
	her = Hlp_GetNpc(PC_Hero);
	rock = Hlp_GetNpc(PC_Rockefeller);
	if((Hlp_GetInstanceID(self) == Hlp_GetInstanceID(her)) || (Hlp_GetInstanceID(self) == Hlp_GetInstanceID(rock)))
	{
		self.aivar[AIV_INVINCIBLE] = TRUE;
		player_mobsi_production = MOBSI_SleepAbit;
		AI_ProcessInfos(her);
		if(SC_IsObsessed == TRUE)
		{
			Wld_PlayEffect("DEMENTOR_FX",hero,hero,0,0,0,FALSE);
		};
	};
};


var int SleepI;

instance PC_NoSleep(C_Info)
{
	npc = PC_Hero;
	nr = 999;
	condition = PC_NoSleep_Condition;
	information = PC_NoSleep_Info;
	important = 0;
	permanent = 1;
	description = Dialog_Ende;
};


func int PC_NoSleep_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == FALSE))
	{
		return TRUE;
	};
};

func void PC_NoSleep_Info()
{
	AI_StopProcessInfos(self);
	Wld_StopEffect("DEMENTOR_FX");
	self.aivar[AIV_INVINCIBLE] = FALSE;
	player_mobsi_production = MOBSI_NONE;
};


instance PC_SleepI(C_Info)
{
	npc = PC_Hero;
	condition = PC_SleepI_Condition;
	information = PC_SleepI_Info;
	permanent = TRUE;
	description = "�����...";
};


func int PC_SleepI_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == FALSE))
	{
		return TRUE;
	};
};

func void PC_SleepI_Info()
{
	SleepI = TRUE;
};


instance PC_SleepTime_Morning(C_Info)
{
	npc = PC_Hero;
	condition = PC_SleepTime_Morning_Condition;
	information = PC_SleepTime_Morning_Info;
	important = 0;
	permanent = 1;
	description = "...�� ����";
};


func int PC_SleepTime_Morning_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == TRUE))
	{
		return TRUE;
	};
};

func void PC_SleepTime_Morning_Info()
{
	PC_Sleep(6);
};


instance PC_SleepTime_Noon(C_Info)
{
	npc = PC_Hero;
	condition = PC_SleepTime_Noon_Condition;
	information = PC_SleepTime_Noon_Info;
	important = 0;
	permanent = 1;
	description = "...�� �������";
};


func int PC_SleepTime_Noon_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == TRUE))
	{
		return TRUE;
	};
};

func void PC_SleepTime_Noon_Info()
{
	PC_Sleep(12);
};


instance PC_SleepTime_Evening(C_Info)
{
	npc = PC_Hero;
	condition = PC_SleepTime_Evening_Condition;
	information = PC_SleepTime_Evening_Info;
	important = 0;
	permanent = 1;
	description = "...�� ������";
};


func int PC_SleepTime_Evening_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == TRUE))
	{
		return TRUE;
	};
};

func void PC_SleepTime_Evening_Info()
{
	PC_Sleep(18);
};


instance PC_SleepTime_Midnight(C_Info)
{
	npc = PC_Hero;
	condition = PC_SleepTime_Midnight_Condition;
	information = PC_SleepTime_Midnight_Info;
	important = 0;
	permanent = 1;
	description = "...�� ��������";
};


func int PC_SleepTime_Midnight_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == TRUE))
	{
		return TRUE;
	};
};

func void PC_SleepTime_Midnight_Info()
{
	PC_Sleep(0);
};


instance PC_SleepIBACK(C_Info)
{
	npc = PC_Hero;
	nr = 99;
	condition = PC_SleepIBACK_Condition;
	information = PC_SleepIBACK_Info;
	permanent = TRUE;
	description = Dialog_Back;
};


func int PC_SleepIBACK_Condition()
{
	if((player_mobsi_production == MOBSI_SleepAbit) && (SleepI == TRUE))
	{
		return TRUE;
	};
};

func void PC_SleepIBACK_Info()
{
	SleepI = FALSE;
};

