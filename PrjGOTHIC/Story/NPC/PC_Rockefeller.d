
instance PC_Rockefeller(Npc_Default)
{
	name[0] = "���������";
	npctype = npctype_main;
	guild = GIL_NONE;
	level = 100;
	voice = 15;
	id = 0;
	attribute[ATR_STRENGTH] = 200;
	attribute[ATR_Dexterity] = 200;
	attribute[ATR_MANA_MAX] = 1000;
	attribute[ATR_MANA] = 1000;
	attribute[ATR_HITPOINTS_MAX] = 1000;
	attribute[ATR_HITPOINTS] = 1000;
	exp = 500 * ((level + 1) / 2) * (level + 1);
	exp_next = 500 * ((level + 2) / 2) * (level + 1);
	Mdl_SetVisual(self,"HUMANS.MDS");
	Mdl_SetVisualBody(self,"hum_body_Naked0",BodyTex_Player,0,"Hum_Head_Pony",Face_N_Player,0,itar_pal_h);
	Npc_SetTalentSkill(self,NPC_TALENT_MAGE,6);
	Npc_SetTalentSkill(self,NPC_TALENT_PICKLOCK,1);
	Npc_SetTalentSkill(self,NPC_TALENT_SNEAK,1);
	Npc_SetTalentSkill(self,NPC_TALENT_ACROBAT,0);
	Npc_SetTalentSkill(self,NPC_TALENT_PICKPOCKET,1);
	Npc_SetTalentSkill(self,NPC_TALENT_SMITH,1);
	Npc_SetTalentSkill(self,NPC_TALENT_RUNES,1);
	Npc_SetTalentSkill(self,NPC_TALENT_ALCHEMY,1);
	Npc_SetTalentSkill(self,NPC_TALENT_TAKEANIMALTROPHY,1);
	player_talent_alchemy[POTION_Health_01] = TRUE;
	player_talent_alchemy[POTION_Health_02] = TRUE;
	player_talent_alchemy[POTION_Health_03] = TRUE;
	player_talent_alchemy[POTION_Mana_01] = TRUE;
	player_talent_alchemy[POTION_Mana_02] = TRUE;
	player_talent_alchemy[POTION_Mana_03] = TRUE;
	player_talent_alchemy[POTION_Speed] = TRUE;
	player_talent_alchemy[POTION_Perm_STR] = TRUE;
	player_talent_alchemy[POTION_Perm_DEX] = TRUE;
	player_talent_alchemy[POTION_Perm_Mana] = TRUE;
	player_talent_alchemy[POTION_Perm_Health] = TRUE;
	player_talent_smith[WEAPON_Common] = TRUE;
	player_talent_smith[WEAPON_1H_Special_01] = TRUE;
	player_talent_smith[WEAPON_2H_Special_01] = TRUE;
	player_talent_smith[WEAPON_1H_Special_02] = TRUE;
	player_talent_smith[WEAPON_2H_Special_02] = TRUE;
	player_talent_smith[WEAPON_1H_Special_03] = TRUE;
	player_talent_smith[WEAPON_2H_Special_03] = TRUE;
	player_talent_smith[WEAPON_1H_Special_04] = TRUE;
	player_talent_smith[WEAPON_2H_Special_04] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_Teeth] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_Claws] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_Fur] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_Heart] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_ShadowHorn] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_FireTongue] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_BFWing] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_BFSting] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_Mandibles] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_CrawlerPlate] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_DrgSnapperHorn] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_DragonScale] = TRUE;
	player_talent_takeanimaltrophy[TROPHY_DragonBlood] = TRUE;
	player_talent_runes[SPL_Light] = TRUE;
	player_talent_runes[SPL_Firebolt] = TRUE;
	player_talent_runes[SPL_Icebolt] = TRUE;
	player_talent_runes[SPL_LightHeal] = TRUE;
	player_talent_runes[SPL_SummonGoblinSkeleton] = TRUE;
	player_talent_runes[SPL_InstantFireball] = TRUE;
	player_talent_runes[SPL_Zap] = TRUE;
	player_talent_runes[SPL_SummonWolf] = TRUE;
	player_talent_runes[SPL_WindFist] = TRUE;
	player_talent_runes[SPL_Sleep] = TRUE;
	player_talent_runes[SPL_MediumHeal] = TRUE;
	player_talent_runes[SPL_LightningFlash] = TRUE;
	player_talent_runes[SPL_ChargeFireball] = TRUE;
	player_talent_runes[SPL_SummonSkeleton] = TRUE;
	player_talent_runes[SPL_Fear] = TRUE;
	player_talent_runes[SPL_IceCube] = TRUE;
	player_talent_runes[SPL_ChargeZap] = TRUE;
	player_talent_runes[SPL_SummonGolem] = TRUE;
	player_talent_runes[SPL_DestroyUndead] = TRUE;
	player_talent_runes[SPL_Pyrokinesis] = TRUE;
	player_talent_runes[SPL_Firestorm] = TRUE;
	player_talent_runes[SPL_IceWave] = TRUE;
	player_talent_runes[SPL_SummonDemon] = TRUE;
	player_talent_runes[SPL_FullHeal] = TRUE;
	player_talent_runes[SPL_Firerain] = TRUE;
	player_talent_runes[SPL_BreathOfDeath] = TRUE;
	player_talent_runes[SPL_MassDeath] = TRUE;
	player_talent_runes[SPL_ArmyOfDarkness] = TRUE;
	player_talent_runes[SPL_Shrink] = TRUE;
	B_SetFightSkills(self,100);
	EquipItem(self,ItMw_2h_Pal_Sword);
	CreateInvItem(self,ItRu_PalLight);
	CreateInvItem(self,ItRu_PalLightHeal);
	CreateInvItem(self,ItRu_PalHolyBolt);
	CreateInvItem(self,ItRu_PalMediumHeal);
	CreateInvItem(self,ItRu_PalRepelEvil);
	CreateInvItem(self,ItRu_PalFullHeal);
	CreateInvItem(self,ItRu_PalDestroyEvil);
	CreateInvItem(self,ItRu_Light);
	CreateInvItem(self,ItRu_FireBolt);
	CreateInvItem(self,ItRu_Icebolt);
	CreateInvItem(self,ItRu_LightHeal);
	CreateInvItem(self,ItRu_SumGobSkel);
	CreateInvItem(self,ItRu_InstantFireball);
	CreateInvItem(self,ItRu_Zap);
	CreateInvItem(self,ItRu_SumWolf);
	CreateInvItem(self,ItRu_Windfist);
	CreateInvItem(self,ItRu_Sleep);
	CreateInvItem(self,ItRu_MediumHeal);
	CreateInvItem(self,ItRu_LightningFlash);
	CreateInvItem(self,ItRu_ChargeFireball);
	CreateInvItem(self,ItRu_SumSkel);
	CreateInvItem(self,ItRu_Fear);
	CreateInvItem(self,ItRu_IceCube);
	CreateInvItem(self,ItRu_ThunderBall);
	CreateInvItem(self,ItRu_SumGol);
	CreateInvItem(self,ItRu_HarmUndead);
	CreateInvItem(self,ItRu_Pyrokinesis);
	CreateInvItem(self,ItRu_Firestorm);
	CreateInvItem(self,ItRu_IceWave);
	CreateInvItem(self,ItRu_SumDemon);
	CreateInvItem(self,ItRu_FullHeal);
	CreateInvItem(self,ItRu_Firerain);
	CreateInvItem(self,ItRu_BreathOfDeath);
	CreateInvItem(self,ItRu_MassDeath);
	CreateInvItem(self,ItRu_MasterOfDisaster);
	CreateInvItem(self,ItRu_ArmyOfDarkness);
	CreateInvItem(self,ItRu_Shrink);
	CreateInvItem(self,ItSc_PalLight);
	CreateInvItem(self,ItSc_PalLightHeal);
	CreateInvItem(self,ItSc_PalHolyBolt);
	CreateInvItem(self,ItSc_PalMediumHeal);
	CreateInvItem(self,ItSc_PalRepelEvil);
	CreateInvItem(self,ItSc_PalFullHeal);
	CreateInvItem(self,ItSc_PalDestroyEvil);
	CreateInvItems(self,ItSc_Charm,10);
	CreateInvItem(self,ItSc_Light);
	CreateInvItem(self,ItSc_Firebolt);
	CreateInvItem(self,ItSc_Icebolt);
	CreateInvItem(self,ItSc_LightHeal);
	CreateInvItem(self,ItSc_SumGobSkel);
	CreateInvItem(self,ItSc_InstantFireball);
	CreateInvItem(self,ItSc_Zap);
	CreateInvItem(self,ItSc_SumWolf);
	CreateInvItem(self,ItSc_Windfist);
	CreateInvItem(self,ItSc_Sleep);
	CreateInvItem(self,ItSc_MediumHeal);
	CreateInvItem(self,ItSc_LightningFlash);
	CreateInvItem(self,ItSc_ChargeFireBall);
	CreateInvItem(self,ItSc_SumSkel);
	CreateInvItem(self,ItSc_Fear);
	CreateInvItem(self,ItSc_IceCube);
	CreateInvItem(self,ItSc_ThunderBall);
	CreateInvItem(self,ItSc_SumGol);
	CreateInvItem(self,ItSc_HarmUndead);
	CreateInvItem(self,ItSc_Pyrokinesis);
	CreateInvItem(self,ItSc_Firestorm);
	CreateInvItem(self,ItSc_IceWave);
	CreateInvItem(self,ItSc_SumDemon);
	CreateInvItem(self,ItSc_FullHeal);
	CreateInvItem(self,ItSc_Firerain);
	CreateInvItem(self,ItSc_BreathOfDeath);
	CreateInvItem(self,ItSc_MassDeath);
	CreateInvItem(self,ItSc_ArmyOfDarkness);
	CreateInvItem(self,ItSc_Shrink);
	CreateInvItems(self,ItSc_TrfSheep,10);
	CreateInvItems(self,ItSc_TrfScavenger,10);
	CreateInvItems(self,ItSc_TrfGiantRat,10);
	CreateInvItems(self,ItSc_TrfMeatBug,10);
	CreateInvItems(self,ItSc_TrfWolf,10);
	CreateInvItems(self,ItSc_TrfWaran,10);
	CreateInvItems(self,ItSc_TrfSnapper,10);
	CreateInvItems(self,ItSc_TrfWarg,10);
	CreateInvItems(self,ItSc_TrfFireWaran,10);
	CreateInvItems(self,ItSc_TrfLurker,10);
	CreateInvItems(self,ItSc_TrfShadowbeast,10);
	CreateInvItems(self,ItSc_TrfDragonSnapper,10);
};

