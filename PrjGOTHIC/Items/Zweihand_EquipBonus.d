
var int hero_2h_max_bonus;

func void Equip_2H_01()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_01) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_01);
			b_meleeweaponchange(0,Waffenbonus_01,0);
		};
	};
};

func void UnEquip_2H_01()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_01);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_02()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_02) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_02);
			b_meleeweaponchange(0,Waffenbonus_02,0);
		};
	};
};

func void UnEquip_2H_02()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_02);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_03()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_03) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_03);
			b_meleeweaponchange(0,Waffenbonus_03,0);
		};
	};
};

func void UnEquip_2H_03()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_03);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_04()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_04) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_04);
			b_meleeweaponchange(0,Waffenbonus_04,0);
		};
	};
};

func void UnEquip_2H_04()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_04);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_05()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_05) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_05);
			b_meleeweaponchange(0,Waffenbonus_05,0);
		};
	};
};

func void UnEquip_2H_05()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_05);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_06()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_06) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_06);
			b_meleeweaponchange(0,Waffenbonus_06,0);
		};
	};
};

func void UnEquip_2H_06()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_06);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_07()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_07) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_07);
			b_meleeweaponchange(0,Waffenbonus_07,0);
		};
	};
};

func void UnEquip_2H_07()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_07);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_08()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_08) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_08);
			b_meleeweaponchange(0,Waffenbonus_08,0);
		};
	};
};

func void UnEquip_2H_08()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_08);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_09()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_09) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_09);
			b_meleeweaponchange(0,Waffenbonus_09,0);
		};
	};
};

func void UnEquip_2H_09()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_09);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_10()
{
	if(Npc_IsPlayer(self))
	{
		if((self.HitChance[NPC_TALENT_2H] + Waffenbonus_10) > 100)
		{
			hero_2h_max_bonus = 100 - self.HitChance[NPC_TALENT_2H];
			B_AddFightSkill(self,NPC_TALENT_2H,hero_2h_max_bonus);
			b_meleeweaponchange(0,hero_2h_max_bonus,0);
		}
		else
		{
			hero_2h_max_bonus = -1;
			B_AddFightSkill(self,NPC_TALENT_2H,Waffenbonus_10);
			b_meleeweaponchange(0,Waffenbonus_10,0);
		};
	};
};

func void UnEquip_2H_10()
{
	if(Npc_IsPlayer(self) && (MeleeWeaponChangedHero || (ScriptPatchWeaponChange == FALSE)))
	{
		if(hero_2h_max_bonus < 0)
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-Waffenbonus_10);
			b_meleeweaponundochange();
		}
		else
		{
			B_AddFightSkill(self,NPC_TALENT_2H,-hero_2h_max_bonus);
			b_meleeweaponundochange();
		};
	};
};

func void Equip_2H_KDF_5()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDF)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,5);
			Npc_ChangeAttribute(self,ATR_MANA,5);
		}
		else
		{
			Snd_Play("DEM_Die");
			AI_Wait(self,3);
			AI_PlayAni(self,"S_FIRE_VICTIM");
			Wld_PlayEffect("VOB_MAGICBURN",self,self,0,0,0,FALSE);
			B_Say(self,self,"$Dead");
			AI_StopFX(self,"VOB_MAGICBURN");
			Npc_ChangeAttribute(self,ATR_HITPOINTS_MAX,-self.attribute[ATR_HITPOINTS_MAX]);
			Npc_ChangeAttribute(self,ATR_HITPOINTS,-self.attribute);
			Npc_StopAni(self,"S_FIRE_VICTIM");
			PrintScreen("����� ��������� �� ���!",-1,YPOS_ItemTaken,FONT_Screen,3);
		};
	};
};

func void UnEquip_2H_KDF_5()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDF)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,-5);
			if(self.attribute[ATR_MANA] >= 5)
			{
				Npc_ChangeAttribute(self,ATR_MANA,-5);
			}
			else
			{
				self.attribute[ATR_MANA] = 0;
			};
		}
		else
		{
			Wld_PlayEffect("FX_EarthQuake",self,self,0,0,0,FALSE);
		};
	};
};

func void Equip_2H_KDF_10()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDF)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,10);
			Npc_ChangeAttribute(self,ATR_MANA,10);
		}
		else
		{
			Snd_Play("DEM_Die");
			AI_Wait(self,3);
			AI_PlayAni(self,"S_FIRE_VICTIM");
			Wld_PlayEffect("VOB_MAGICBURN",self,self,0,0,0,FALSE);
			B_Say(self,self,"$Dead");
			AI_StopFX(self,"VOB_MAGICBURN");
			Npc_ChangeAttribute(self,ATR_HITPOINTS_MAX,-self.attribute[ATR_HITPOINTS_MAX]);
			Npc_ChangeAttribute(self,ATR_HITPOINTS,-self.attribute);
			Npc_StopAni(self,"S_FIRE_VICTIM");
			PrintScreen("����� ��������� �� ���!",-1,YPOS_ItemTaken,FONT_Screen,3);
		};
	};
};

func void UnEquip_2H_KDF_10()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDF)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,-10);
			if(self.attribute[ATR_MANA] >= 10)
			{
				Npc_ChangeAttribute(self,ATR_MANA,-10);
			}
			else
			{
				self.attribute[ATR_MANA] = 0;
			};
		}
		else
		{
			Wld_PlayEffect("FX_EarthQuake",self,self,0,0,0,FALSE);
		};
	};
};

func void Equip_2H_KDW_5()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDW)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,5);
			Npc_ChangeAttribute(self,ATR_MANA,5);
		}
		else
		{
			Snd_Play("DEM_Die");
			AI_Wait(self,3);
			AI_PlayAni(self,"S_FIRE_VICTIM");
			Wld_PlayEffect("spellFX_BELIARSRAGE",self,self,0,0,0,FALSE);
			B_Say(self,self,"$Dead");
			AI_StopFX(self,"VOB_MAGICBURN");
			Npc_ChangeAttribute(self,ATR_HITPOINTS_MAX,-self.attribute[ATR_HITPOINTS_MAX]);
			Npc_ChangeAttribute(self,ATR_HITPOINTS,-self.attribute);
			Npc_StopAni(self,"S_FIRE_VICTIM");
			PrintScreen("������ ��������� �� ���!",-1,YPOS_ItemTaken,FONT_Screen,3);
		};
	};
};

func void UnEquip_2H_KDW_5()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDW)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,-5);
			if(self.attribute[ATR_MANA] >= 5)
			{
				Npc_ChangeAttribute(self,ATR_MANA,-5);
			}
			else
			{
				self.attribute[ATR_MANA] = 0;
			};
		}
		else
		{
			Wld_PlayEffect("FX_EarthQuake",self,self,0,0,0,FALSE);
		};
	};
};

func void Equip_2H_KDW_10()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDW)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,10);
			Npc_ChangeAttribute(self,ATR_MANA,10);
		}
		else
		{
			Snd_Play("DEM_Die");
			AI_Wait(self,3);
			AI_PlayAni(self,"S_FIRE_VICTIM");
			Wld_PlayEffect("spellFX_BELIARSRAGE",self,self,0,0,0,FALSE);
			B_Say(self,self,"$Dead");
			AI_StopFX(self,"VOB_MAGICBURN");
			Npc_ChangeAttribute(self,ATR_HITPOINTS_MAX,-self.attribute[ATR_HITPOINTS_MAX]);
			Npc_ChangeAttribute(self,ATR_HITPOINTS,-self.attribute);
			Npc_StopAni(self,"S_FIRE_VICTIM");
			PrintScreen("������ ��������� �� ���!",-1,YPOS_ItemTaken,FONT_Screen,3);
		};
	};
};

func void UnEquip_2H_KDW_10()
{
	if(self.voice == 15)
	{
		if(self.guild == GIL_KDW)
		{
			Npc_ChangeAttribute(self,ATR_MANA_MAX,-10);
			if(self.attribute[ATR_MANA] >= 10)
			{
				Npc_ChangeAttribute(self,ATR_MANA,-10);
			}
			else
			{
				self.attribute[ATR_MANA] = 0;
			};
		}
		else
		{
			Wld_PlayEffect("FX_EarthQuake",self,self,0,0,0,FALSE);
		};
	};
};

