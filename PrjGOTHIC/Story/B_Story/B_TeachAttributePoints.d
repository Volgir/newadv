
func int B_TeachAttributePoints(var C_Npc slf,var C_Npc oth,var int attrib,var int points,var int teacherMAX)
{
	var string concatText;
	var int kosten;
	var int realAttribute;
	var int money;
	kosten = B_GetLearnCostAttribute(oth,attrib) * points;
	if(hero.level >= 100)
	{
		money = kosten * 70;
	}
	else if(hero.level >= 70)
	{
		money = kosten * 65;
	}
	else if(hero.level >= 60)
	{
		money = kosten * 60;
	}
	else if(hero.level >= 50)
	{
		money = kosten * 55;
	}
	else if(hero.level >= 45)
	{
		money = kosten * 50;
	}
	else if(hero.level >= 40)
	{
		money = kosten * 45;
	}
	else if(hero.level >= 35)
	{
		money = kosten * 40;
	}
	else if(hero.level >= 30)
	{
		money = kosten * 35;
	}
	else if(hero.level >= 25)
	{
		money = kosten * 30;
	}
	else if(hero.level >= 20)
	{
		money = kosten * 25;
	}
	else if(hero.level >= 15)
	{
		money = kosten * 20;
	}
	else if(hero.level >= 10)
	{
		money = kosten * 15;
	}
	else if(hero.level >= 5)
	{
		money = kosten * 10;
	}
	else
	{
		money = kosten * 5;
	};
	if((attrib != ATR_STRENGTH) && (attrib != ATR_Dexterity) && (attrib != ATR_MANA_MAX))
	{
		Print("*** ERROR: Wrong Parameter ***");
		return FALSE;
	};
	if(attrib == ATR_STRENGTH)
	{
		realAttribute = oth.attribute[ATR_STRENGTH];
	}
	else if(attrib == ATR_Dexterity)
	{
		realAttribute = oth.attribute[ATR_Dexterity];
	}
	else if(attrib == ATR_MANA_MAX)
	{
		realAttribute = oth.attribute[ATR_MANA_MAX];
	};
	if(realAttribute >= teacherMAX)
	{
		concatText = ConcatStrings(PRINT_NoLearnOverPersonalMAX,IntToString(teacherMAX));
		PrintScreen(concatText,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNYOUREBETTER");
		return FALSE;
	};
	if((realAttribute + points) > teacherMAX)
	{
		concatText = ConcatStrings(PRINT_NoLearnOverPersonalMAX,IntToString(teacherMAX));
		PrintScreen(concatText,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNOVERPERSONALMAX");
		return FALSE;
	};
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLP,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	B_RaiseAttribute(oth,attrib,points);
	return TRUE;
};

