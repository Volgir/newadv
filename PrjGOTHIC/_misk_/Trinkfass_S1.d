
func void Trinkfass_S1()
{
	var C_Npc her;
	her = Hlp_GetNpc(PC_Hero);
	if(Hlp_GetInstanceID(self) == Hlp_GetInstanceID(her))
	{
		self.aivar[AIV_INVINCIBLE] = TRUE;
		player_mobsi_production = MOBSI_TRINKFASS;
		AI_ProcessInfos(her);
	};
};


instance PC_Trinkfass_End(C_Info)
{
	npc = PC_Hero;
	nr = 999;
	condition = PC_Trinkfass_End_Condition;
	information = PC_Trinkfass_End_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};


func int PC_Trinkfass_End_Condition()
{
	if(player_mobsi_production == MOBSI_TRINKFASS)
	{
		return TRUE;
	};
};

func void PC_Trinkfass_End_Info()
{
	b_endproductiondialog();
};


instance TRINKFASS_NWasser(C_Info)
{
	npc = PC_Hero;
	nr = 555;
	condition = TRINKFASS_NWasser_Condition;
	information = TRINKFASS_NWasser_Info;
	permanent = TRUE;
	description = "������ ����(�������������: 5 HP)";
};


func int TRINKFASS_NWasser_Condition()
{
	if(player_mobsi_production == MOBSI_TRINKFASS)
	{
		return TRUE;
	};
};

func void TRINKFASS_NWasser_Info()
{
	if(hero.attribute[ATR_HITPOINTS] < hero.attribute[ATR_HITPOINTS_MAX])
	{
		hero.attribute[ATR_HITPOINTS] += 5;
		PrintScreen("������������: 5 HP",-1,-1,FONT_Screen,2);
		AI_PlayAni(hero,"T_TRINKFASS_SEP");
	}
	else if(hero.attribute[ATR_HITPOINTS] >= hero.attribute[ATR_HITPOINTS_MAX])
	{
		hero.attribute[ATR_HITPOINTS] = hero.attribute[ATR_HITPOINTS_MAX];
		PrintScreen("��� ������ ��������",-1,-1,FONT_Screen,2);
		AI_PlayAni(hero,"T_TRINKFASS_SEP");
	};
	b_endproductiondialog();
};

