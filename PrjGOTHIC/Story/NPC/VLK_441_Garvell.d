
instance VLK_441_Garvell(Npc_Default)
{
	name[0] = "�������";
	guild = GIL_VLK;
	id = 441;
	voice = 4;
	flags = 0;
	npctype = npctype_main;
	aivar[AIV_ToughGuy] = TRUE;
	B_SetAttributesToChapter(self,3);
	fight_tactic = FAI_HUMAN_STRONG;
	EquipItem(self,ItMw_1h_Vlk_Axe);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_Bald",Face_N_Lefty,BodyTex_N,ITAR_Vlk_L);
	Mdl_SetModelFatness(self,0);
	Mdl_ApplyOverlayMds(self,"Humans_Tired.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,30);
	daily_routine = Rtn_Start_441;
};


func void Rtn_Start_441()
{
	TA_Stand_Drinking(4,50,19,11,"NW_CITY_HABOUR_07");
	TA_Stand_Drinking(4,50,19,11,"NW_CITY_HABOUR_07");
};

func void rtn_runaway_441()
{
	TA_FleeToWp(8,0,23,0,"NW_CITY_HABOUR_KASERN_05_01");
	TA_FleeToWp(23,0,8,0,"NW_CITY_HABOUR_KASERN_05_01");
};

