
prototype Mst_Default_OrcBow(C_Npc)
{
	name[0] = "���-����������";
	guild = GIL_ORC;
	aivar[AIV_MM_REAL_ID] = ID_OrcBow;
	voice = 18;
	level = 25;
	attribute[ATR_STRENGTH] = 90;
	attribute[ATR_Dexterity] = 150;
	attribute[ATR_HITPOINTS_MAX] = 250;
	attribute[ATR_HITPOINTS] = 250;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 120;
	protection[PROT_EDGE] = 120;
	protection[PROT_POINT] = 120;
	protection[PROT_FIRE] = 35;
	protection[PROT_FLY] = 120;
	protection[PROT_MAGIC] = 20;
	HitChance[NPC_TALENT_1H] = 50;
	HitChance[NPC_TALENT_2H] = 50;
	HitChance[NPC_TALENT_BOW] = 50;
	HitChance[NPC_TALENT_CROSSBOW] = 65;
	damagetype = DAM_EDGE;
	fight_tactic = FAI_ORC;
	senses = SENSE_HEAR | SENSE_SEE | SENSE_SMELL;
	senses_range = PERC_DIST_ORC_ACTIVE_MAX;
	aivar[AIV_MM_FollowTime] = FOLLOWTIME_MEDIUM;
	aivar[AIV_MM_FollowInWater] = FALSE;
};

func void B_SetVisuals_OrcBow()
{
	Mdl_SetVisual(self,"Orc.mds");
	Mdl_SetVisualBody(self,"Orc_BodyWarrior",DEFAULT,DEFAULT,"Orc_HeadWarrior",DEFAULT,DEFAULT,-1);
};


instance OrcBow(Mst_Default_OrcBow)
{
	B_SetVisuals_OrcBow();
	EquipItem(self,ItMw_2H_OrcAxe_01);
	EquipItem(self,ItRw_OrcCrossbow);
	start_aistate = ZS_MM_AllScheduler;
	aivar[AIV_MM_RoamStart] = OnlyRoutine;
};

