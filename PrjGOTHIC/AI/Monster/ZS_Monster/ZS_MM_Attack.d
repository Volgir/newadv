
func void B_MM_AssessSurprise()
{
	Npc_SetTarget(self,other);
};

func void ZS_MM_Attack()
{
	Npc_SetPercTime(self,1);
	Npc_PercEnable(self,PERC_ASSESSBODY,B_MM_AssessBody);
	Npc_PercEnable(self,PERC_ASSESSMAGIC,B_AssessMagic);
	Npc_PercEnable(self,PERC_ASSESSDAMAGE,B_MM_AssessDamage);
	Npc_PercEnable(self,PERC_ASSESSWARN,B_MM_AssessWarn);
	Npc_PercEnable(self,PERC_ASSESSSURPRISE,B_MM_AssessSurprise);
	B_ValidateOther();
	if(C_WantToFlee(self,other))
	{
		B_MM_Flee();
		return;
	};
	AI_Standup(self);
	AI_SetWalkMode(self,NPC_RUN);
	Npc_SendPassivePerc(self,PERC_ASSESSWARN,other,self);
	self.aivar[AIV_PursuitEnd] = FALSE;
	self.aivar[AIV_StateTime] = 0;
	self.aivar[AIV_HitByOtherNpc] = 0;
	self.aivar[AIV_SelectSpell] = 0;
	self.aivar[AIV_TAPOSITION] = 0;
};

func int ZS_MM_Attack_Loop()
{
	Npc_GetTarget(self);
	if(C_WantToFlee(self,other))
	{
		return LOOP_END;
	};
	if(self.guild == GIL_DRAGON)
	{
		self.aivar[AIV_TAPOSITION] += 1;
		if((self.attribute[ATR_HITPOINTS] < self.attribute[ATR_HITPOINTS_MAX]) && (self.aivar[AIV_TAPOSITION] >= 2))
		{
			self.attribute[ATR_HITPOINTS] += 1;
			self.aivar[AIV_TAPOSITION] = 0;
		};
	};
	if(self.guild == GIL_DRAGON)
	{
		self.aivar[AIV_TAPOSITION] += 1;
		if((self.attribute[ATR_HITPOINTS] < self.attribute[ATR_HITPOINTS_MAX]) && (self.aivar[AIV_TAPOSITION] >= 2))
		{
			self.attribute[ATR_HITPOINTS] += 1;
			self.aivar[AIV_TAPOSITION] = 0;
		};
		if((Npc_GetDistToNpc(self,other) >= 650) && (Npc_GetDistToNpc(self,other) < 3000) && !C_BodyStateContains(self,BS_JUMP) && (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_FIRE))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,0);
			Wld_PlayEffect("SPELLFX_FIREBALL",self,other,2,200,DAM_FIRE,TRUE);
		}
		else if((Npc_GetDistToNpc(self,other) >= 650) && (Npc_GetDistToNpc(self,other) < 3000) && !C_BodyStateContains(self,BS_JUMP) && (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_UNDEAD))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_DEATHBOLT",self,other,2,300,DAM_Magic,TRUE);
		}
		else if((Npc_GetDistToNpc(self,other) >= 650) && (Npc_GetDistToNpc(self,other) < 3000) && !C_BodyStateContains(self,BS_JUMP) && (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_ICE))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_ICEBALL",self,other,2,250,DAM_Magic,TRUE);
		}
		else if((Npc_GetDistToNpc(self,other) >= 650) && (Npc_GetDistToNpc(self,other) < 3000) && !C_BodyStateContains(self,BS_JUMP) && (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_ROCK))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_CONCUSSIONBOLT",self,other,2,150,DAM_FLY,TRUE);
		}
		else if((Npc_GetDistToNpc(self,other) >= 650) && (Npc_GetDistToNpc(self,other) < 3000) && !C_BodyStateContains(self,BS_JUMP) && (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_SWAMP))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_SWAMPFIST",self,other,2,100,DAM_Magic,TRUE);
		};
	};
	if(self.aivar[AIV_MM_REAL_ID] == ID_HARPY)
	{
		if(self.attribute[ATR_HITPOINTS] < 150)
		{
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_HEAL",self,self,0,0,0,FALSE);
			Npc_ChangeAttribute(self,ATR_HITPOINTS,100);
		};
	};
	if(self.aivar[AIV_MM_REAL_ID] == ID_Summoned_SkeletonMage)
	{
		if(Npc_GetDistToNpc(self,other) >= FIGHT_DIST_MELEE)
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_FIBCAST_2_FIBSHOOT");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_ICELANCE",self,other,2,75,DAM_Magic,TRUE);
		};
	};
	if((Npc_GetDistToNpc(self,other) >= FIGHT_DIST_MELEE) && (Hlp_GetInstanceID(self) == Hlp_GetInstanceID(Summoned_FireGolem)))
	{
		AI_TurnToNPC(self,other);
		AI_PlayAni(self,"T_WARN");
		AI_Wait(self,1);
		Wld_PlayEffect("SPELLFX_INSTANTFIREBALL",self,other,2,75,DAM_FIRE,TRUE);
	};
	if((Npc_GetDistToNpc(self,other) >= FIGHT_DIST_MELEE) && (Hlp_GetInstanceID(self) == Hlp_GetInstanceID(Summoned_IceGolem)))
	{
		AI_TurnToNPC(self,other);
		AI_PlayAni(self,"T_WARN");
		AI_Wait(self,1);
		Wld_PlayEffect("SPELLFX_ICEBALL",self,other,2,75,DAM_Magic,TRUE);
	};
	if(self.aivar[AIV_MM_REAL_ID] == ID_Swampgolem)
	{
		if((Npc_GetDistToNpc(self,other) >= FIGHT_DIST_MELEE) && (Kapitel == 1))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_SWAMPFIST",self,other,2,75,DAM_Magic,TRUE);
		}
		else if(((Npc_GetDistToNpc(self,other) >= FIGHT_DIST_MELEE) && (Kapitel == 2)) || (Kapitel == 3) || (Kapitel == 4) || (Kapitel == 5))
		{
			AI_TurnToNPC(self,other);
			AI_PlayAni(self,"T_WARN");
			AI_Wait(self,1);
			Wld_PlayEffect("SPELLFX_SWAMPFIST",self,other,2,100,DAM_Magic,TRUE);
		};
	};
	if((RavenIsDead == TRUE) && (self.guild == GIL_Stoneguardian))
	{
		B_KillNpc(self);
	};
	if(CurrentLevel == OldWorld_Zen)
	{
		if(Npc_GetDistToWP(self,"OC_RAMP_07") <= 500)
		{
			Npc_ClearAIQueue(self);
			AI_Standup(self);
			AI_PlayAni(self,"T_WARN");
			self.aivar[AIV_PursuitEnd] = TRUE;
			return LOOP_END;
		};
	};
	if(Npc_GetDistToNpc(self,other) > FIGHT_DIST_CANCEL)
	{
		Npc_ClearAIQueue(self);
		AI_Standup(self);
		self.aivar[AIV_PursuitEnd] = TRUE;
		return LOOP_END;
	};
	if((Npc_GetStateTime(self) > self.aivar[AIV_MM_FollowTime]) && (self.aivar[AIV_PursuitEnd] == FALSE))
	{
		Npc_ClearAIQueue(self);
		AI_Standup(self);
		self.aivar[AIV_PursuitEnd] = TRUE;
		self.aivar[AIV_Dist] = Npc_GetDistToNpc(self,other);
		self.aivar[AIV_StateTime] = Npc_GetStateTime(self);
		AI_PlayAni(self,"T_WARN");
	};
	if(self.aivar[AIV_PursuitEnd] == TRUE)
	{
		if(Npc_GetDistToNpc(self,other) > self.senses_range)
		{
			return LOOP_END;
		};
		if(Npc_GetStateTime(self) > self.aivar[AIV_StateTime])
		{
			if((Npc_GetDistToNpc(self,other) < self.aivar[AIV_Dist]) || (!C_BodyStateContains(other,BS_RUN) && !C_BodyStateContains(other,BS_JUMP)))
			{
				self.aivar[AIV_PursuitEnd] = FALSE;
				Npc_SetStateTime(self,0);
				self.aivar[AIV_StateTime] = 0;
			}
			else
			{
				AI_TurnToNPC(self,other);
				self.aivar[AIV_Dist] = Npc_GetDistToNpc(self,other);
				self.aivar[AIV_StateTime] = Npc_GetStateTime(self);
			};
		};
		return LOOP_CONTINUE;
	};
	if((C_BodyStateContains(other,BS_SWIM) || C_BodyStateContains(other,BS_DIVE)) && (self.aivar[AIV_MM_FollowInWater] == FALSE))
	{
		Npc_ClearAIQueue(self);
		AI_Standup(self);
		return LOOP_END;
	};
	if(self.aivar[AIV_WaitBeforeAttack] == 1)
	{
		AI_Wait(self,0.8);
		self.aivar[AIV_WaitBeforeAttack] = 0;
	};
	if(self.level == 0)
	{
		if(Npc_GetStateTime(self) > self.aivar[AIV_StateTime])
		{
			self.aivar[AIV_SummonTime] += 1;
			self.aivar[AIV_StateTime] = Npc_GetStateTime(self);
		};
		if((self.aivar[AIV_SummonTime] >= MONSTER_SUMMON_TIME) || (hero.attribute[ATR_HITPOINTS] <= 0) || (Npc_GetDistToNpc(self,hero) > 3000))
		{
			Npc_ChangeAttribute(self,ATR_HITPOINTS,-self.attribute[ATR_HITPOINTS_MAX]);
		};
	};
	if(!C_BodyStateContains(other,BS_RUN) && !C_BodyStateContains(other,BS_JUMP) && (Npc_GetStateTime(self) > 0))
	{
		Npc_SetStateTime(self,0);
		self.aivar[AIV_StateTime] = 0;
	};
	if(self.aivar[AIV_MaxDistToWp] > 0)
	{
		if((Npc_GetDistToWP(self,self.wp) > self.aivar[AIV_MaxDistToWp]) && (Npc_GetDistToWP(other,self.wp) > self.aivar[AIV_MaxDistToWp]))
		{
			self.fight_tactic = FAI_NAILED;
		}
		else
		{
			self.fight_tactic = self.aivar[AIV_OriginalFightTactic];
		};
	};
	if(C_NpcIsMonsterMage(self) || (self.guild == GIL_SKELETON) || (self.guild == gil_summoned_skeleton) || (self.guild == GIL_GOBBO) || (self.guild == GIL_GOBBO_SKELETON) || (self.guild == gil_summoned_gobbo_skeleton) || (self.guild > GIL_SEPERATOR_ORC))
	{
		B_CreateAmmo(self);
		Npc_ChangeAttribute(self,ATR_MANA,ATR_MANA_MAX);
		B_SelectWeapon(self,other);
	};
	if((self.aivar[AIV_MM_REAL_ID] == ID_OrcBow) && (Npc_GetDistToNpc(self,other) <= FIGHT_DIST_RANGED_OUTER))
	{
		B_SelectWeapon(self,other);
	};
	if(Hlp_IsValidNpc(other) && !C_NpcIsDown(other))
	{
		if(other.aivar[AIV_INVINCIBLE] == FALSE)
		{
			AI_Attack(self);
		}
		else
		{
			Npc_ClearAIQueue(self);
		};
		self.aivar[AIV_LASTTARGET] = Hlp_GetInstanceID(other);
		return LOOP_CONTINUE;
	}
	else
	{
		if((self.aivar[AIV_MM_PRIORITY] == PRIO_EAT) && C_WantToEat(self,other))
		{
			Npc_ClearAIQueue(self);
			AI_Standup(self);
			return LOOP_END;
		};
		Npc_PerceiveAll(self);
		Npc_GetNextTarget(self);
		if(Hlp_IsValidNpc(other) && !C_NpcIsDown(other) && ((Npc_GetDistToNpc(self,other) < PERC_DIST_INTERMEDIAT) || Npc_IsPlayer(other)) && (other.aivar[AIV_INVINCIBLE] == FALSE))
		{
			self.aivar[AIV_LASTTARGET] = Hlp_GetInstanceID(other);
			return LOOP_CONTINUE;
		}
		else
		{
			Npc_ClearAIQueue(self);
			AI_Standup(self);
			return LOOP_END;
		};
	};
};

func void ZS_MM_Attack_End()
{
	other = Hlp_GetNpc(self.aivar[AIV_LASTTARGET]);
	if(C_NpcIsMonsterMage(self) || (self.guild == GIL_SKELETON) || (self.guild == gil_summoned_skeleton) || (self.guild == GIL_GOBBO) || (self.guild == GIL_GOBBO_SKELETON) || (self.guild == gil_summoned_gobbo_skeleton) || (self.guild > GIL_SEPERATOR_ORC))
	{
		AI_RemoveWeapon(self);
	};
	if(C_WantToFlee(self,other))
	{
		B_MM_Flee();
		return;
	};
	if(Npc_IsDead(other) && C_WantToEat(self,other))
	{
		Npc_ClearAIQueue(self);
		AI_StartState(self,ZS_MM_EatBody,0,"");
		return;
	};
};

