
func int B_TeachPlayerTalentAlchemy(var C_Npc slf,var C_Npc oth,var int potion)
{
	var int kosten;
	var int money;
	kosten = B_GetLearnCostTalent(oth,NPC_TALENT_ALCHEMY,potion);
	money = kosten * 50;
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLearnPoints,-1,-1,FONT_ScreenSmall,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	Log_CreateTopic(TOPIC_TalentAlchemy,LOG_NOTE);
	B_LogEntry(TOPIC_TalentAlchemy,"����� ������� �����, ��� ����� ������ �������� � ����������� ��� ����� ����� �����������. �� ���� ������������, � ���� ����������� ����� �� ����� ��������.");
	if(potion == POTION_Health_01)
	{
		player_talent_alchemy[POTION_Health_01] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ��������': 2 �������� ����� � 1 ������� �����.");
	};
	if(potion == POTION_Health_02)
	{
		player_talent_alchemy[POTION_Health_02] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '��������� ���������': 2 �������� �������� � 1 ������� �����.");
	};
	if(potion == POTION_Health_03)
	{
		player_talent_alchemy[POTION_Health_03] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '��������� ��������': 2 �������� ����� � 1 ������� �����.");
	};
	if(potion == POTION_Mana_01)
	{
		player_talent_alchemy[POTION_Mana_01] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ����': 2 �������� ������� � 1 ������� �����.");
	};
	if(potion == POTION_Mana_02)
	{
		player_talent_alchemy[POTION_Mana_02] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '��������� ����': 2 �������� ����� � 1 ������� �����.");
	};
	if(potion == POTION_Mana_03)
	{
		player_talent_alchemy[POTION_Mana_03] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ����': 2 �������� ����� � 1 ������� �����");
	};
	if(potion == POTION_Speed)
	{
		player_talent_alchemy[POTION_Speed] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '����� ���������': 1 �������-����� � 1 ������� �����");
	};
	if(potion == POTION_Perm_STR)
	{
		player_talent_alchemy[POTION_Perm_STR] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ����': 1 �������� ������ � 1 ������� ������.");
	};
	if(potion == POTION_Perm_DEX)
	{
		player_talent_alchemy[POTION_Perm_DEX] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ��������': 1 ���������� ����� � 1 ������� ������.");
	};
	if(potion == POTION_Perm_Mana)
	{
		player_talent_alchemy[POTION_Perm_Mana] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� ����': 1 �������� ������ � 1 ������� ������.");
	};
	if(potion == POTION_Perm_Health)
	{
		player_talent_alchemy[POTION_Perm_Health] = TRUE;
		B_LogEntry(TOPIC_TalentAlchemy,"����������� ��� '�������� �����': 1 �������� ������ � 1 ������� ������.");
	};
	PrintScreen(PRINT_LearnAlchemy,-1,-1,FONT_Screen,2);
	Npc_SetTalentSkill(oth,NPC_TALENT_ALCHEMY,1);
	Snd_Play("LevelUP");
	return TRUE;
};

