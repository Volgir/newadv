
instance BAU_972_Bauer(Npc_Default)
{
	name[0] = NAME_Bauer;
	guild = GIL_OUT;
	id = 972;
	voice = 7;
	flags = 0;
	npctype = NPCTYPE_AMBIENT;
	B_SetAttributesToChapter(self,1);
	fight_tactic = FAI_HUMAN_COWARD;
	EquipItem(self,ItMw_1h_Bau_Mace);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_FatBald",Face_N_NormalBart16,BodyTex_N,ITAR_Bau_L);
	Mdl_SetModelFatness(self,0);
	Mdl_ApplyOverlayMds(self,"Humans_Relaxed.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,20);
	daily_routine = Rtn_Start_972;
};


func void Rtn_Start_972()
{
	TA_Smalltalk(8,0,22,0,"NW_TAVERNE_IN_05");
	TA_Smalltalk(22,0,8,0,"NW_TAVERNE_IN_05");
};

func void Rtn_ExitT_972()
{
	TA_Sit_Campfire(8,0,22,0,"WAYPOINT_02");
	TA_Sit_Campfire(22,0,8,0,"WAYPOINT_02");
};

