
const int AIV_ArdoAttitude = 91;
const int AIV_ArdoNervous = 92;

instance DIA_Ardo_Walk_Hello(C_INFO)
{
	npc = VLK_6000_Ardo;
	nr = 1;
	permanent = FALSE;
	condition = DIA_Ardo_Walk_Hello_Condition;
	information = DIA_Ardo_Walk_Hello_Info;
	description = "������";
};

func int DIA_Ardo_Walk_Hello_Condition()
{
    IF !Npc_KnowsInfo(other,DIA_Ardo_Tobacco) && !Hlp_StrCmp(Npc_GetNearestWP(self), "NW_CITY_RAUCH_05") && !Hlp_StrCmp(Npc_GetNearestWP(self), "NW_CITY_HOTEL_BED_07")
	{
		return TRUE;
	};
};

func void DIA_Ardo_Walk_Hello_Info()
{
	AI_Output(other, self, "DIA_OCPAL_4_STANDARD_15_00"); //��� ����?
	AI_Output(self, other, "DIA_Ardo_Walk_Hello_08_00"); //������� �� ��� ������. �������� ����, ���� � ������ ����� � ������� ����.
	Info_ClearChoices(DIA_Ardo_Walk_Hello);
	Info_AddChoice(DIA_Ardo_Walk_Hello,"����� �������, � ������� ��� �������?",DIA_Ardo_Walk_Hello_Difference);
	Info_AddChoice(DIA_Ardo_Walk_Hello, Dialog_Back, DIA_Ardo_Walk_Hello_Back);
};

func void DIA_Ardo_Walk_Hello_Back()
{
	Info_ClearChoices(DIA_Ardo_Walk_Hello);
};

func void DIA_Ardo_Walk_Hello_Difference()
{
	AI_Output(other, self, "DIA_Ardo_Walk_Hello_Difference_15_00"); //����� �������, � ������� ��� �������?
	AI_Output(self, other, "DIA_Ardo_Walk_Hello_Difference_08_01"); //�����, ��� � ������� ���� ������ �� ����������.
	AI_Output(self, other, "DIA_Ardo_Walk_Hello_Difference_08_02"); //��� �� ������� � ����� ���� � ������� �������, ���������, � ������� �������������.
	AI_Output(self, other, "DIA_Ardo_Walk_Hello_Difference_08_03"); //������� ��������� �������� ������.
	Info_ClearChoices(DIA_Ardo_Walk_Hello);
};

instance DIA_Ardo_Night_Hello(C_INFO)
{
	npc = VLK_6000_Ardo;
	nr = 1;
	permanent = FALSE;
	condition = DIA_Ardo_Night_Hello_Condition;
	information = DIA_Ardo_Night_Hello_Info;
	description = "������";
};

func int DIA_Ardo_Night_Hello_Condition()
{
    IF Hlp_StrCmp(Npc_GetNearestWP(self), "NW_CITY_HOTEL_BED_07")
	{
		return TRUE;
	};
};

func void DIA_Ardo_Night_Hello_Info()
{
	AI_Output(self, other, "DIA_Ardo_Night_Hello_08_00"); //���? ��� ���������?..
	AI_Output(other, self, "DIA_OCPAL_4_STANDARD_15_00"); //��� ����?
	AI_DrawWeapon(self);
	AI_Output(self, other, "DIA_Ardo_Night_Hello_08_02"); //������ ���� ���� ����� �����! ��������!
	self.aivar[AIV_ArdoAttitude] = self.aivar[AIV_ArdoAttitude] - 1;
	self.aivar[AIV_ArdoNervous] = TRUE;
	AI_StopProcessInfos(self);
	AI_RemoveWeapon(self);
};

instance DIA_Ardo_Nervous(C_INFO)
{
	npc = VLK_6000_Ardo;
	nr = 1;
	permanent = FALSE;
	condition = DIA_Ardo_Nervous_Condition;
	information = DIA_Ardo_Nervous_Info;
	description = "�� �������.";
	important = TRUE;
};

func int DIA_Ardo_Nervous_Condition()
{
	if Npc_IsInState(self,ZS_Talk) && self.aivar[AIV_ArdoNervous] == TRUE
	{
		return TRUE;
	};
};

func void DIA_Ardo_Nervous_Info()
{
	AI_Output(other, self, "DIA_Ardo_Nervous_15_00"); //�����-�� �� �������.
	AI_Output(self, other, "DIA_Ardo_Nervous_08_01"); //������ �� ���� ���� ����� ����, ������?
	self.aivar[AIV_ArdoNervous] = FALSE;
};

instance DIA_Ardo_Tobacco(C_INFO)
{
	npc = VLK_6000_Ardo;
	nr = 2;
	permanent = FALSE;
	condition = DIA_Ardo_Tobacco_Condition;
	information = DIA_Ardo_Tobacco_Info;
	description = "��� �����?";
};

func int DIA_Ardo_Tobacco_Condition()
{
    IF Hlp_StrCmp(Npc_GetNearestWP(self), "NW_CITY_RAUCH_05")
	{
		return TRUE;
	};
};

func void DIA_Ardo_Tobacco_Info()
{
	AI_Output(other, self, "DIA_Ardo_Tobacco_15_00"); //��� �����?
	AI_Output(self, other, "DIA_Ardo_Tobacco_08_01"); //�������. ������������� - �����, ����������� ���-������ �� ���� �������.
	AI_Output(other, self, "DIA_Ardo_Tobacco_15_02"); //����� �� ���������, ��� �� ������ ������� �� ��� �����?
	AI_Output(self, other, "DIA_Ardo_Tobacco_08_03"); //��� ����������� �������� �� ����������.
	AI_Output(self, other, "DIA_Ardo_Tobacco_08_04"); //������� � ������-�� ���� ������ �� ����, � ������, ����� ��� ������� ���������, ������� � ����� ����������.
	AI_Output(self, other, "DIA_Ardo_Tobacco_08_05"); //� ���� �� ��������� ��������� �����.
};

instance DIA_Ardo_Wares(C_Info)
{
	npc = VLK_6000_Ardo;
	nr = 2;
	condition = DIA_Ardo_Wares_Condition;
	information = DIA_Ardo_Wares_Info;
	permanent = TRUE;
	trade = TRUE;
	description = DIALOG_TRADE_v4;
};

func int DIA_Ardo_Wares_Condition()
{
	if (Npc_KnowsInfo(other,DIA_Ardo_Tobacco) || Npc_KnowsInfo(other,DIA_Ardo_Walk_Hello)) && !Hlp_StrCmp(Npc_GetNearestWP(self), "NW_CITY_HOTEL_BED_07")
	{
		return TRUE;
	};
};

func void DIA_Ardo_Wares_Info()
{
	if(self.aivar[AIV_ChapterInv] <= Kapitel)
	{
		B_ClearJunkTradeInv(self);
		B_GiveTradeInv_Ardo(self);
	};
	if(Npc_IsInState(self,ZS_Dead) || Npc_IsInState(self,ZS_Unconscious))
	{
		B_ClearDeadTrader(self);
	};
	AI_Output(other,self,"DIA_Zuris_WAREZ_15_00"); //������ ��� ���� ������
};

instance DIA_Ardo_Exit(C_Info)
{
	npc = VLK_6000_Ardo;
	nr = 999;
	condition = DIA_Ardo_Exit_Condition;
	information = DIA_Ardo_Exit_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};

func int DIA_Ardo_Exit_Condition()
{
	return TRUE;
};

func void DIA_Ardo_Exit_Info()
{
	AI_StopProcessInfos(self);
};
