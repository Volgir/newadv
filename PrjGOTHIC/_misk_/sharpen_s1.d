
var int Sharpen_Old;

func void sharpen_s1()
{
	if(Hlp_GetInstanceID(self) == Hlp_GetInstanceID(hero))
	{
		self.aivar[AIV_INVINCIBLE] = TRUE;
		player_mobsi_production = MOBSI_Sharpen;
		AI_ProcessInfos(hero);
	};
};


instance PC_Sharpen_End(C_Info)
{
	npc = PC_Hero;
	nr = 999;
	condition = PC_Sharpen_End_Condition;
	information = PC_Sharpen_End_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};


func int PC_Sharpen_End_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (Sharpen_Old == FALSE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_End_Info()
{
	CreateInvItems(hero,ItMiSwordblade,1);
	b_endproductiondialog();
};


instance PC_Sharpen(C_Info)
{
	npc = PC_Hero;
	condition = PC_Sharpen_Condition;
	information = PC_Sharpen_Info;
	permanent = TRUE;
	description = "�������� ������ ������";
};


func int PC_Sharpen_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (Sharpen_Old == FALSE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_Info()
{
	if(Brian_SmithTeach == TRUE)
	{
		Sharpen_Old = TRUE;
	}
	else
	{
		B_Say_Overlay(self,self,"$MISSINGITEM");
		PrintScreen("��� ������������ ������!",-1,-1,FONT_Screen,2);
		b_endproductiondialog();
	};
};


instance PC_SharpenBACK(C_Info)
{
	npc = PC_Hero;
	nr = 99;
	condition = PC_SharpenBACK_Condition;
	information = PC_SharpenBACK_Info;
	permanent = TRUE;
	description = Dialog_Back;
};


func int PC_SharpenBACK_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (Sharpen_Old == TRUE))
	{
		return TRUE;
	};
};

func void PC_SharpenBACK_Info()
{
	Sharpen_Old = FALSE;
};


instance PC_Sharpen_1h_Old(C_Info)
{
	npc = PC_Hero;
	condition = PC_Sharpen_1h_Old_Condition;
	information = PC_Sharpen_1h_Old_Info;
	permanent = TRUE;
	description = "�������� ������ ���������� ���";
};


func int PC_Sharpen_1h_Old_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (player_talent_smith[13] == TRUE) && (Sharpen_Old == TRUE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_1h_Old_Info()
{
	if(Npc_HasItems(hero,ItMw_1h_MISC_Sword) >= 1)
	{
		Npc_RemoveInvItems(hero,ItMw_1h_MISC_Sword,1);
		AI_Wait(other,3);
		Print(PRINT_SharpenSuccess);
		CreateInvItems(hero,ItMw_1h_Sld_Sword,1);
		AI_PrintScreen("������ ��������!",-1,52,FONT_Screen,2);
	}
	else
	{
		Print(PRINT_SharpenNot);
	};
	CreateInvItems(hero,ItMiSwordblade,1);
	b_endproductiondialog();
	Sharpen_Old = FALSE;
};


instance PC_Sharpen_1h_Old2(C_Info)
{
	npc = PC_Hero;
	condition = PC_Sharpen_1h_Old2_Condition;
	information = PC_Sharpen_1h_Old2_Info;
	permanent = TRUE;
	description = "�������� ������ ������";
};


func int PC_Sharpen_1h_Old2_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (player_talent_smith[13] == TRUE) && (Sharpen_Old == TRUE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_1h_Old2_Info()
{
	if(Npc_HasItems(hero,itmw_1h_misc_rapier) >= 1)
	{
		Npc_RemoveInvItems(hero,itmw_1h_misc_rapier,1);
		AI_Wait(other,3);
		Print(PRINT_SharpenSuccess);
		CreateInvItems(hero,ItMw_Rapier,1);
		AI_PrintScreen("������ ��������!",-1,52,FONT_Screen,2);
	}
	else
	{
		Print(PRINT_SharpenNot);
	};
	CreateInvItems(hero,ItMiSwordblade,1);
	b_endproductiondialog();
	Sharpen_Old = FALSE;
};


instance PC_Sharpen_2h_Old(C_Info)
{
	npc = PC_Hero;
	condition = PC_Sharpen_2h_Old_Condition;
	information = PC_Sharpen_2h_Old_Info;
	permanent = TRUE;
	description = "�������� ������ ��������� ���";
};


func int PC_Sharpen_2h_Old_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (player_talent_smith[13] == TRUE) && (Sharpen_Old == TRUE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_2h_Old_Info()
{
	if(Npc_HasItems(hero,ItMw_2H_Sword_M_01) >= 1)
	{
		Npc_RemoveInvItems(hero,ItMw_2H_Sword_M_01,1);
		AI_Wait(other,3);
		Print(PRINT_SharpenSuccess);
		CreateInvItems(hero,ItMw_Zweihaender1,1);
		AI_PrintScreen("������ ��������!",-1,52,FONT_Screen,2);
	}
	else
	{
		Print(PRINT_SharpenNot);
	};
	CreateInvItems(hero,ItMiSwordblade,1);
	b_endproductiondialog();
	Sharpen_Old = FALSE;
};


instance PC_Sharpen_2h_Old2(C_Info)
{
	npc = PC_Hero;
	condition = PC_Sharpen_2h_Old2_Condition;
	information = PC_Sharpen_2h_Old2_Info;
	permanent = TRUE;
	description = "�������� ������ ��������� �����";
};


func int PC_Sharpen_2h_Old2_Condition()
{
	if((player_mobsi_production == MOBSI_Sharpen) && (player_talent_smith[13] == TRUE) && (Sharpen_Old == TRUE))
	{
		return TRUE;
	};
};

func void PC_Sharpen_2h_Old2_Info()
{
	if(Npc_HasItems(hero,ItMw_1h_Misc_Axe) >= 1)
	{
		Npc_RemoveInvItems(hero,ItMw_1h_Misc_Axe,1);
		AI_Wait(other,3);
		Print(PRINT_SharpenSuccess);
		CreateInvItems(hero,ItMw_2h_Sld_Axe,1);
		AI_PrintScreen("������ ��������!",-1,52,FONT_Screen,2);
	}
	else
	{
		Print(PRINT_SharpenNot);
	};
	CreateInvItems(hero,ItMiSwordblade,1);
	b_endproductiondialog();
	Sharpen_Old = FALSE;
};

