
var int sprint_active;
var int sbsprint;
var int sbmode;

func void B_Hotkey_Mana_Potion()
{
	if(Npc_IsInState(hero,ZS_Dead) == FALSE)
	{
		if((Npc_HasItems(hero,ItPo_Mana_Addon_04) >= 1) && (hero.attribute[ATR_MANA_MAX] > hero.attribute[ATR_MANA]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Mana_Addon_04);
		}
		else if((Npc_HasItems(hero,ItPo_Mana_03) >= 1) && (hero.attribute[ATR_MANA_MAX] > hero.attribute[ATR_MANA]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Mana_03);
		}
		else if((Npc_HasItems(hero,ItPo_Mana_02) >= 1) && (hero.attribute[ATR_MANA_MAX] > hero.attribute[ATR_MANA]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Mana_02);
		}
		else if((Npc_HasItems(hero,ItPo_Mana_01) >= 1) && (hero.attribute[ATR_MANA_MAX] > hero.attribute[ATR_MANA]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Mana_01);
		}
		else if(hero.attribute[ATR_MANA_MAX] == hero.attribute[ATR_MANA])
		{
			Print("� ��� ������������ ���������� ���������� �������! ");
		}
		else
		{
			Print("� ��� ��� �������� �������������� ���������� �������...");
		};
	};
};

func void B_Hotkey_Health_Potion()
{
	if(Npc_IsInState(hero,ZS_Dead) == FALSE)
	{
		if((Npc_HasItems(hero,ItPo_Health_Addon_04) >= 1) && (hero.attribute[ATR_HITPOINTS_MAX] > hero.attribute[ATR_HITPOINTS]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Health_Addon_04);
		}
		else if((Npc_HasItems(hero,ItPo_Health_03) >= 1) && (hero.attribute[ATR_HITPOINTS_MAX] > hero.attribute[ATR_HITPOINTS]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Health_03);
		}
		else if((Npc_HasItems(hero,ItPo_Health_02) >= 1) && (hero.attribute[ATR_HITPOINTS_MAX] > hero.attribute[ATR_HITPOINTS]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Health_02);
		}
		else if((Npc_HasItems(hero,ItPo_Health_01) >= 1) && (hero.attribute[ATR_HITPOINTS_MAX] > hero.attribute[ATR_HITPOINTS]))
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Health_01);
		}
		else if(hero.attribute[ATR_HITPOINTS_MAX] == hero.attribute[ATR_HITPOINTS])
		{
			Print("� ��� ������������ ���������� ��������� �������! ");
		}
		else
		{
			Print("� ��� ��� �������� �������������� ��������� �������...");
		};
	};
};

func void B_Hotkey_Speed_Potion()
{
	if(Npc_IsInState(hero,ZS_Dead) == FALSE)
	{
		if(Npc_HasItems(hero,ItPo_Speed) >= 1)
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItPo_Speed);
		}
		else if(Npc_HasItems(hero,ItFo_Addon_SchnellerHering) >= 1)
		{
			AI_RemoveWeapon(hero);
			AI_StandupQuick(hero);
			AI_UseItem(hero,ItFo_Addon_SchnellerHering);
		}
		else
		{
			Print("� ��� ��� �������� ���������...");
		};
	};
};

func void B_Hotkey_Sprint()
{
	var int sbmode_local;
	if(sbmode <= 1)
	{
		sbmode_local = 1;
	}
	else if(sbmode <= 3)
	{
		sbmode_local = 3;
	}
	else if(sbmode >= 4)
	{
		sbmode_local = 5;
	};
	if(sprint_active == TRUE)
	{
		sprint_active = FALSE;
		Mdl_RemoveOverlayMds(hero,"HUMANS_SPRINT.MDS");
	}
	else if(hero.attribute[ATR_HITPOINTS] > (hero.attribute[ATR_HITPOINTS_MAX] / (sbmode_local * 2)))
	{
		sprint_active = TRUE;
		Mdl_ApplyOverlayMds(hero,"HUMANS_SPRINT.MDS");
	}
	else
	{
		AI_OutputSVM(hero,hero,"$IMPOSSIBLEFORME");
	};
};

