
prototype Mst_Default_Gobbo_Green(C_Npc)
{
	name[0] = "������";
	guild = GIL_GOBBO;
	aivar[AIV_MM_REAL_ID] = ID_GOBBO_GREEN;
	level = 4;
	attribute[ATR_STRENGTH] = 20;
	attribute[ATR_Dexterity] = 20;
	attribute[ATR_HITPOINTS_MAX] = 20;
	attribute[ATR_HITPOINTS] = 20;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 20;
	protection[PROT_EDGE] = 20;
	protection[PROT_POINT] = 20;
	protection[PROT_FIRE] = 20;
	protection[PROT_FLY] = 20;
	protection[PROT_MAGIC] = 0;
	damagetype = DAM_EDGE;
	fight_tactic = FAI_GOBBO;
	senses = SENSE_HEAR | SENSE_SEE | SENSE_SMELL;
	senses_range = PERC_DIST_MONSTER_ACTIVE_MAX;
	aivar[AIV_MM_ThreatenBeforeAttack] = TRUE;
	aivar[AIV_MM_FollowTime] = FOLLOWTIME_MEDIUM;
	aivar[AIV_MM_FollowInWater] = FALSE;
	aivar[AIV_MM_Packhunter] = TRUE;
	start_aistate = ZS_MM_AllScheduler;
	aivar[AIV_MM_RestStart] = OnlyRoutine;
};

func void B_SetVisuals_Gobbo_Green()
{
	Mdl_SetVisual(self,"Gobbo.mds");
	Mdl_SetVisualBody(self,"Gob_Body",0,DEFAULT,"",DEFAULT,DEFAULT,-1);
};


instance Gobbo_Green_H(Mst_Default_Gobbo_Green)
{
	name[0] = "������";
	level = 6;
	attribute[ATR_STRENGTH] = 25;
	attribute[ATR_Dexterity] = 25;
	attribute[ATR_HITPOINTS_MAX] = 40;
	attribute[ATR_HITPOINTS] = 40;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 20;
	protection[PROT_EDGE] = 20;
	protection[PROT_POINT] = 20;
	protection[PROT_FIRE] = 20;
	protection[PROT_FLY] = 20;
	protection[PROT_MAGIC] = 0;
	B_SetVisuals_Gobbo_Green();
	rnd = Hlp_Random(100);
	if(rnd <= 40)
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Mace);
	}
	else
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Axe);
	};
};


var int gobbo_green_h.rnd;

instance YGobbo_Green_H(Mst_Default_Gobbo_Green)
{
	name[0] = "������� ������";
	level = 4;
	attribute[ATR_STRENGTH] = 10;
	attribute[ATR_Dexterity] = 10;
	attribute[ATR_HITPOINTS_MAX] = 30;
	attribute[ATR_HITPOINTS] = 30;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 10;
	protection[PROT_EDGE] = 10;
	protection[PROT_POINT] = 10;
	protection[PROT_FIRE] = 10;
	protection[PROT_FLY] = 10;
	protection[PROT_MAGIC] = 0;
	B_SetVisuals_Gobbo_Green();
	rnd = Hlp_Random(100);
	if(rnd <= 40)
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Mace);
	}
	else
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Axe);
	};
};


var int ygobbo_green_h.rnd;

instance Gobbo_Green(Mst_Default_Gobbo_Green)
{
	B_SetVisuals_Gobbo_Green();
	rnd = Hlp_Random(100);
	if(rnd <= 40)
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Mace);
	}
	else
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Axe);
	};
};


var int gobbo_green.rnd;

instance YGobbo_Green(Mst_Default_Gobbo_Green)
{
	name[0] = "������� ������";
	level = 3;
	attribute[ATR_STRENGTH] = 5;
	attribute[ATR_Dexterity] = 5;
	attribute[ATR_HITPOINTS_MAX] = 20;
	attribute[ATR_HITPOINTS] = 20;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 0;
	protection[PROT_EDGE] = 0;
	protection[PROT_POINT] = 0;
	protection[PROT_FIRE] = 0;
	protection[PROT_FLY] = 0;
	protection[PROT_MAGIC] = 0;
	B_SetVisuals_Gobbo_Green();
	rnd = Hlp_Random(100);
	if(rnd <= 40)
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Mace);
	}
	else
	{
		Npc_SetToFightMode(self,ItMw_1h_Bau_Axe);
	};
};


var int ygobbo_green.rnd;

prototype MST_DEFAULT_GOBBO_BANDIT(C_Npc)
{
	name[0] = "������-���������";
	guild = GIL_GOBBO;
	aivar[AIV_MM_REAL_ID] = ID_GOBBO_GREEN;
	level = 16;
	attribute[ATR_STRENGTH] = 110;
	attribute[ATR_Dexterity] = 110;
	attribute[ATR_HITPOINTS_MAX] = 220;
	attribute[ATR_HITPOINTS] = 220;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 20;
	protection[PROT_EDGE] = 20;
	protection[PROT_POINT] = 20;
	protection[PROT_FIRE] = 20;
	protection[PROT_FLY] = 0;
	protection[PROT_MAGIC] = 0;
	damagetype = DAM_EDGE;
	fight_tactic = FAI_GOBBO;
	senses = SENSE_HEAR | SENSE_SEE | SENSE_SMELL;
	senses_range = PERC_DIST_MONSTER_ACTIVE_MAX;
	aivar[AIV_MM_ThreatenBeforeAttack] = TRUE;
	aivar[AIV_MM_FollowTime] = FOLLOWTIME_MEDIUM;
	aivar[AIV_MM_FollowInWater] = FALSE;
	aivar[AIV_MM_Packhunter] = TRUE;
	start_aistate = ZS_MM_AllScheduler;
	aivar[AIV_MM_RestStart] = OnlyRoutine;
};

func void b_setvisuals_gobbo_bandit()
{
	Mdl_SetVisual(self,"Gobbo_Bandit.mds");
	Mdl_SetVisualBody(self,"GOBBDT_BODY",0,DEFAULT,"",DEFAULT,DEFAULT,-1);
};


instance GOBBO_BANDIT(MST_DEFAULT_GOBBO_BANDIT)
{
	b_setvisuals_gobbo_bandit();
	Npc_SetToFistMode(self);
};

