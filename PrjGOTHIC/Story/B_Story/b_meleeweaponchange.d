
var int PlayerIsTransformed;
var int HitChanceBackupOneH;
var int HitChanceBackupTwoH;
var int ManaMaxBackup;
var int MeleeWeaponChangedHero;
var int ChangedManaHero;
var int ChangedOneHHero;
var int ChangedTwoHHero;
var int ScriptPatchWeaponChange;

func void b_meleeweaponchange(var int oneh,var int twoh,var int manamax)
{
	MeleeWeaponChangedHero = TRUE;
	ChangedManaHero = manamax;
	ChangedOneHHero = oneh;
	ChangedTwoHHero = twoh;
	ScriptPatchWeaponChange = TRUE;
};

func void b_meleeweaponundochange()
{
	MeleeWeaponChangedHero = FALSE;
	ChangedManaHero = 0;
	ChangedOneHHero = 0;
	ChangedTwoHHero = 0;
	ScriptPatchWeaponChange = TRUE;
};

func void b_startmagictransform(var int level)
{
	if(PlayerIsTransformed == FALSE)
	{
		HitChanceBackupOneH = hero.HitChance[NPC_TALENT_1H] - ChangedOneHHero;
		HitChanceBackupTwoH = hero.HitChance[NPC_TALENT_2H] - ChangedTwoHHero;
		ManaMaxBackup = hero.attribute[ATR_MANA_MAX] - ChangedManaHero;
		b_meleeweaponundochange();
		MonsterTransformLevel = level;
		if(level == 1)
		{
			CreateInvItem(hero,ItSc_TrfSheep);
		}
		else if(level == 7)
		{
			CreateInvItem(hero,ItSc_TrfScavenger);
		}
		else if(level == 3)
		{
			CreateInvItem(hero,ItSc_TrfGiantRat);
		}
		else if(level == 8)
		{
			CreateInvItem(hero,ItSc_TrfMeatBug);
		}
		else if(level == 6)
		{
			CreateInvItem(hero,ItSc_TrfWolf);
		}
		else if(level == 12)
		{
			CreateInvItem(hero,ItSc_TrfWaran);
			CreateInvItem(hero,ItSc_TrfSnapper);
			CreateInvItem(hero,ItSc_TrfLurker);
		}
		else if(level == 30)
		{
			CreateInvItem(hero,ItSc_TrfWarg);
			CreateInvItem(hero,ItSc_TrfFireWaran);
			CreateInvItem(hero,ItSc_TrfShadowbeast);
		}
		else if(level == 40)
		{
			CreateInvItem(hero,ItSc_TrfDragonSnapper);
		};
		hero.aivar[90] = hero.level;
		hero.level = level;
		LevelUpsDuringTransform = 0;
		PlayerIsTransformed = TRUE;
	};
};

func void b_stopmagictransform()
{
	if(PlayerIsTransformed == TRUE)
	{
		if(HitChanceBackupOneH != hero.HitChance[NPC_TALENT_1H])
		{
			B_AddFightSkill(hero,NPC_TALENT_1H,(HitChanceBackupOneH - hero.HitChance[NPC_TALENT_1H]) + ChangedOneHHero);
		};
		if(HitChanceBackupTwoH != hero.HitChance[NPC_TALENT_2H])
		{
			B_AddFightSkill(hero,NPC_TALENT_2H,(HitChanceBackupTwoH - hero.HitChance[NPC_TALENT_2H]) + ChangedTwoHHero);
		};
		if(ManaMaxBackup != hero.attribute[ATR_MANA_MAX])
		{
			hero.attribute[ATR_MANA_MAX] = ManaMaxBackup + ChangedManaHero;
		};
		AI_UnequipWeapons(hero);
		AI_UnequipWeapons(hero);
		if(MonsterTransformLevel == 1)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfSheep);
		}
		else if(MonsterTransformLevel == 7)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfScavenger);
		}
		else if(MonsterTransformLevel == 3)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfGiantRat);
		}
		else if(MonsterTransformLevel == 8)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfMeatBug);
		}
		else if(MonsterTransformLevel == 6)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfWolf);
		}
		else if(MonsterTransformLevel == 12)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfWaran);
			Npc_RemoveInvItem(hero,ItSc_TrfSnapper);
			Npc_RemoveInvItem(hero,ItSc_TrfLurker);
		}
		else if(MonsterTransformLevel == 30)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfWarg);
			Npc_RemoveInvItem(hero,ItSc_TrfFireWaran);
			Npc_RemoveInvItem(hero,ItSc_TrfShadowbeast);
		}
		else if(MonsterTransformLevel == 40)
		{
			Npc_RemoveInvItem(hero,ItSc_TrfDragonSnapper);
		};
		hero.level = hero.aivar[90] + LevelUpsDuringTransform;
		hero.attribute[ATR_HITPOINTS_MAX] += LevelUpsDuringTransform * HP_PER_LEVEL;
		hero.attribute[ATR_HITPOINTS] += LevelUpsDuringTransform * HP_PER_LEVEL;
		self.attribute[ATR_HITPOINTS] = 0;
		PlayerIsTransformed = FALSE;
	};
};

