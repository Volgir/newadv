
instance BAU_922_Robbi(Npc_Default)
{
	name[0] = "�����";
	guild = GIL_BAU;
	id = 922;
	voice = 9;
	flags = 0;
	npctype = npctype_main;
	B_SetAttributesToChapter(self,2);
	fight_tactic = FAI_HUMAN_COWARD;
	EquipItem(self,ItMw_1h_Vlk_Axe);
	CreateInvItems(self,ItMw_1H_Axe_WoodChoppin,1);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_FatBald",Face_N_Normal16,BodyTex_N,ITAR_Bau_L);
	Mdl_SetModelFatness(self,2);
	Mdl_ApplyOverlayMds(self,"Humans_Relaxed.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,20);
	daily_routine = Rtn_Start_922;
};


func void Rtn_Start_922()
{
	TA_WoodChoppin(8,0,9,0,"WAYPOINT_1");
	TA_Trinkfass(9,0,9,10,"WAYPOINT_2");
};

