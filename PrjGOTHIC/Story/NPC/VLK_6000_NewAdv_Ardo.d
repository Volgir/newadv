
instance VLK_6000_Ardo(Npc_Default)
{
	name[0] = "����";
	guild = GIL_VLK;
	id = 6000;
	voice = 8;
	flags = NPC_FLAG_IMMORTAL;
	npctype = npctype_main;
	B_SetAttributesToChapter(self,3);
	fight_tactic = FAI_HUMAN_STRONG;
	EquipItem(self,ItMw_1h_Sld_Sword);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_FatBald",Face_P_Gilbert,BodyTex_P,ITAR_Leather_L);
	Mdl_SetModelFatness(self,2);
	Mdl_ApplyOverlayMds(self,"Humans_Relaxed.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,40);
	daily_routine = Rtn_Start_60000;
	aivar[AIV_ArdoAttitude] = 10;
	aivar[AIV_ArdoNervous] = FALSE;
};

func void Rtn_Start_60000()
{
	TA_Smoke_Waterpipe(8,0,19,0,"NW_CITY_RAUCH_05");
	TA_Sleep(19,0,8,0,"NW_CITY_HOTEL_BED_07");
};
