
const int SPL_Cost_SummonSkeleton = 60;

instance Spell_SummonSkeleton(C_Spell_Proto)
{
	time_per_mana = 0;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonSkeleton(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonSkeleton)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};


var int rnd;

func void Spell_Cast_SummonSkeleton()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonSkeleton;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		rnd = Hlp_Random(100);
		if(rnd <= 30)
		{
			Wld_StopEffect("SLOW_MOTION");
			Wld_SpawnNpcRange(self,Summoned_Skeleton,1,500);
		}
		else if(rnd <= 60)
		{
			Wld_StopEffect("SLOW_MOTION");
			Wld_SpawnNpcRange(self,Summoned_Skeleton2,1,500);
		}
		else if(rnd <= 99)
		{
			Wld_StopEffect("SLOW_MOTION");
			Wld_SpawnNpcRange(self,Summoned_Skeleton3,1,500);
		};
	}
	else
	{
		self.lp += 1;
		rnd = Hlp_Random(100);
		if(rnd <= 30)
		{
			Wld_SpawnNpcRange(self,Skeleton,1,500);
		}
		else if(rnd <= 60)
		{
			Wld_SpawnNpcRange(self,Skeleton_Bow,1,500);
		}
		else if(rnd <= 99)
		{
			Wld_SpawnNpcRange(self,SkeletonMage,1,500);
		};
	};
};

