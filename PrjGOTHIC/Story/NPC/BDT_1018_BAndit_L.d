
instance BDT_1018_BAndit_L(Npc_Default)
{
	name[0] = "������";
	guild = GIL_BDT;
	id = 1018;
	voice = 6;
	flags = 0;
	npctype = npctype_main;
	aivar[AIV_EnemyOverride] = TRUE;
	B_SetAttributesToChapter(self,0);
	fight_tactic = FAI_HUMAN_STRONG;
	EquipItem(self,ItMw_1H_Mace_L_03);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_FatBald",Face_P_NormalBart01,BodyTex_P,itar_bdt_l);
	Mdl_SetModelFatness(self,0);
	Mdl_ApplyOverlayMds(self,"Humans_Relaxed.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,30);
	daily_routine = RTN_Start_1018;
};


func void RTN_Start_1018()
{
	TA_Stand_ArmsCrossed(8,0,22,0,"SPECIAL_BANDIT");
	TA_Stand_ArmsCrossed(22,0,8,0,"SPECIAL_BANDIT");
};

