
instance ItRw_Sld_Bow(C_Item)
{
	name = "���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_SldBogen;
	damageTotal = Damage_SldBogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_SldBogen;
	visual = "ItRw_Bow_S_Iron.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Old_Bow(C_Item)
{
	name = "������ ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_SldBogen;
	damageTotal = Damage_SldBogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_SldBogen;
	visual = "ItRw_Sld_Bow.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bdt_Bow(C_Item)
{
	name = "��� �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_SldBogen;
	damageTotal = Damage_SldBogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_SldBogen;
	visual = "ITRW_BOW_G3_RANGER.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_L_01(C_Item)
{
	name = "�������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Kurzbogen;
	damageTotal = Damage_Kurzbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Kurzbogen;
	visual = "ItRw_Bow_L_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_L_02(C_Item)
{
	name = "������ ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Weidenbogen;
	damageTotal = Damage_Weidenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Weidenbogen;
	visual = "ItRw_Bow_L_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_L_03(C_Item)
{
	name = "��������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Jagdbogen;
	damageTotal = Damage_Jagdbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Jagdbogen;
	visual = "ItRw_Bow_M_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_L_04(C_Item)
{
	name = "������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Ulmenbogen;
	damageTotal = Damage_Ulmenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Ulmenbogen;
	visual = "STEELBOW.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_M_01(C_Item)
{
	name = "����������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Kompositbogen;
	damageTotal = Damage_Kompositbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Kompositbogen;
	visual = "ItRw_Bow_M_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_M_02(C_Item)
{
	name = "�������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Eschenbogen;
	damageTotal = Damage_Eschenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Eschenbogen;
	visual = "ItRw_Bow_M_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_M_03(C_Item)
{
	name = "������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Langbogen;
	damageTotal = Damage_Langbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Langbogen;
	visual = "ItRw_Bow_M_03.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_M_04(C_Item)
{
	name = "������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Buchenbogen;
	damageTotal = Damage_Buchenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Buchenbogen;
	visual = "ItRw_Bow_M_04.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_H_01(C_Item)
{
	name = "�������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Knochenbogen;
	damageTotal = Damage_Knochenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Knochenbogen;
	visual = "ItRw_Bow_H_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_H_02(C_Item)
{
	name = "������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Eichenbogen;
	damageTotal = Damage_Eichenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Eichenbogen;
	visual = "ItRw_Bow_H_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_H_03(C_Item)
{
	name = "������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Kriegsbogen;
	damageTotal = Damage_Kriegsbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Kriegsbogen;
	visual = "ItRw_Bow_H_03.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bow_H_04(C_Item)
{
	name = "�������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_Drachenbogen;
	damageTotal = Damage_Drachenbogen;
	damagetype = DAM_POINT;
	munition = ItRw_Arrow;
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_Drachenbogen;
	visual = "ItRw_Bow_H_04.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_MagicBow(C_Item)
{
	name = "���������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_MagicBow;
	damageTotal = Damage_MagicBow;
	damagetype = DAM_Magic;
	munition = ItRw_Addon_MagicArrow;
	wear = WEAR_EFFECT;
	effect = "SPELLFX_BOW";
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_MagicBow;
	visual = "ItRw_Bow_H_04.mms";
	description = name;
	text[2] = NAME_Dam_Magic;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_FireBow(C_Item)
{
	name = "�������� ���";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_BOW;
	material = MAT_Wood;
	value = Value_FireBow;
	damageTotal = Damage_FireBow;
	damagetype = DAM_FIRE;
	munition = ItRw_Addon_FireArrow;
	wear = WEAR_EFFECT;
	effect = "SPELLFX_FIREBOW";
	cond_atr[2] = ATR_Dexterity;
	cond_value[2] = Condition_FireBow;
	visual = "ItRw_Bow_H_04.mms";
	description = name;
	text[2] = NAME_Dam_Fire;
	count[2] = damageTotal;
	text[3] = NAME_Dex_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Mil_Crossbow(C_Item)
{
	name = "������� ���������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_MilArmbrust;
	damageTotal = Damage_MilArmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_MilArmbrust;
	visual = "ItRw_Mil_Crossbow.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_L_01(C_Item)
{
	name = "��������� �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_Jagdarmbrust;
	damageTotal = Damage_Jagdarmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_Jagdarmbrust;
	visual = "ItRw_Crossbow_L_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_L_02(C_Item)
{
	name = "������ �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_LeichteArmbrust;
	damageTotal = Damage_LeichteArmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_LeichteArmbrust;
	visual = "ItRw_Crossbow_L_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_M_01(C_Item)
{
	name = "�������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_Armbrust;
	damageTotal = Damage_Armbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_Armbrust;
	visual = "ItRw_Crossbow_M_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_M_02(C_Item)
{
	name = "������� �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_Kriegsarmbrust;
	damageTotal = Damage_Kriegsarmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_Kriegsarmbrust;
	visual = "ItRw_Crossbow_M_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_H_01(C_Item)
{
	name = "������� �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_SchwereArmbrust;
	damageTotal = Damage_SchwereArmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_SchwereArmbrust;
	visual = "ItRw_Crossbow_H_01.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Crossbow_H_02(C_Item)
{
	name = "������� �������� �� ��������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = Value_Drachenjaegerarmbrust;
	damageTotal = Damage_Drachenjaegerarmbrust;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_Drachenjaegerarmbrust;
	visual = "ItRw_Crossbow_H_02.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_MagicCrossbow(C_Item)
{
	name = "���������� �������";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	wear = WEAR_EFFECT;
	effect = "SPELLFX_CROSSBOW";
	value = Value_MagicCrossbow;
	damageTotal = Damage_MagicCrossbow;
	damagetype = DAM_Magic;
	munition = ItRw_Addon_MagicBolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = Condition_MagicCrossbow;
	visual = "ItRw_Crossbow_H_01.mms";
	description = name;
	text[2] = NAME_Dam_Magic;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_OrcCrossbow(C_Item)
{
	name = "���� �����";
	mainflag = ITEM_KAT_FF;
	flags = ITEM_CROSSBOW;
	material = MAT_Wood;
	value = 650;
	damageTotal = 60;
	damagetype = DAM_POINT;
	munition = ItRw_Bolt;
	cond_atr[2] = ATR_STRENGTH;
	cond_value[2] = 90;
	visual = "ItRw_Crossbow_G3_KrashMorra.mms";
	description = name;
	text[2] = NAME_Dam_Point;
	count[2] = damageTotal;
	text[3] = NAME_Str_needed;
	count[3] = cond_value[2];
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Arrow(C_Item)
{
	name = "������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	scemeName = "MAPSEALED";
	value = Value_Pfeil;
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Bolt(C_Item)
{
	name = "����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Bolzen;
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_MagicArrow(C_Item)
{
	name = "���������� ������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	wear = WEAR_EFFECT;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_ARROW";
	value = Value_Pfeil2;
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_FireArrow(C_Item)
{
	name = "�������� ������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	wear = WEAR_EFFECT;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_FIREARROW";
	value = Value_Pfeil2;
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Arrow_Blessed(C_Item)
{
	name = "���������� ������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	value = Value_Pfeil2;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_WEAKGLIMMER";
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[3] = "���������� ������ ������";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Arrow_Poison(C_Item)
{
	name = "����������� ������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	value = Value_Pfeil2;
	scemeName = "MAPSEALED";
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Arrow_Edge(C_Item)
{
	name = "��������� ������";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_BOW | ITEM_MULTI;
	value = Value_Pfeil2;
	scemeName = "MAPSEALED";
	visual = "ItRw_Arrow.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_Addon_MagicBolt(C_Item)
{
	name = "���������� ����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Bolzen;
	wear = WEAR_EFFECT;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_BOLT";
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_FireBolt(C_Item)
{
	name = "�������� ����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Pfeil2;
	wear = WEAR_EFFECT;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_FIREARROW";
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_PoisonBolt(C_Item)
{
	name = "����������� ����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Pfeil2;
	scemeName = "MAPSEALED";
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_BlessedBolt(C_Item)
{
	name = "���������� ����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Pfeil2;
	wear = WEAR_EFFECT;
	scemeName = "MAPSEALED";
	effect = "SPELLFX_WEAKGLIMMER";
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[3] = "���������� ������ ������";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItRw_EdgeBolt(C_Item)
{
	name = "���������� ����";
	mainflag = ITEM_KAT_MUN;
	flags = ITEM_CROSSBOW | ITEM_MULTI;
	value = Value_Pfeil2;
	scemeName = "MAPSEALED";
	visual = "ItRw_Bolt.3ds";
	material = MAT_Wood;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

