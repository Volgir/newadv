
instance DIA_Mortis_EXIT(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 999;
	condition = DIA_Mortis_EXIT_Condition;
	information = DIA_Mortis_EXIT_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};


func int DIA_Mortis_EXIT_Condition()
{
	return TRUE;
};

func void DIA_Mortis_EXIT_Info()
{
	AI_StopProcessInfos(self);
};


instance DIA_Mortis_Hallo(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 2;
	condition = DIA_Mortis_Hallo_Condition;
	information = DIA_Mortis_Hallo_Info;
	permanent = FALSE;
	important = TRUE;
};


func int DIA_Mortis_Hallo_Condition()
{
	if(Npc_IsInState(self,ZS_Talk) && (!Npc_KnowsInfo(other,DIA_Peck_FOUND_PECK) && (Kapitel < 3)))
	{
		return TRUE;
	};
};

func void DIA_Mortis_Hallo_Info()
{
	AI_Output(self,other,"DIA_Mortis_Hallo_13_00");	//��� ���� �����? ���� ����� ���. � ��� ���� �� ������ �� ��������. ������ �����.
};


var int ArOk4;

instance DIA_Mortis_ArUp(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 12;
	condition = DIA_Mortis_ArUp_Condition;
	information = DIA_Mortis_ArUp_Info;
	permanent = FALSE;
	description = "� ������...";
};


func int DIA_Mortis_ArUp_Condition()
{
	if(Armor_Up == TRUE)
	{
		return TRUE;
	};
};

func void DIA_Mortis_ArUp_Info()
{
	AI_Output(other,self,"Dia_Mortis_ArUp_15_00");	//� ������, ��� �� ������ ������ �������...
	AI_Output(self,other,"Dia_Mortis_ArUp_13_01");	//���...��, ��� ���.
	AI_Output(other,self,"Dia_Mortis_ArUp_15_02");	//� �� ��� �� ������� ���� �����?
	AI_Output(self,other,"Dia_Mortis_ArUp_13_03");	//������ �� � ���?
	AI_Output(self,other,"Dia_Mortis_ArUp_13_04");	//������ ���, ������� ������� ������� �����, � ����� � ���������.
	ArOk4 = TRUE;
};

func void b_mortis_teacharmor_1()
{
	AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_01");	//������, ������ ����������� ��� ��� ��������. ������ ������ ��������� �...
	if(!C_BodyStateContains(self,BS_MOBINTERACT_INTERRUPT) && Wld_IsMobAvailable(self,"BSANVIL"))
	{
		AI_SetWalkMode(self,NPC_WALK);
		AI_GotoWP(self,"NW_CITY_KASERN_ARMORY_ANVIL");
		AI_AlignToWP(self);
		AI_UseMob(self,"BSANVIL",5);
		AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_02");	//...�� ���������� ���������� �� � ������ ������������������ � ���������� �������� ���������...
		AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_03");	//...����� ���� �������� �� ����������� ����� � ���������� � ��������� ��������...
		AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_04");	//...��� ���, ��� ��� ����� � ������...(����������)
		AI_UseMob(self,"BSANVIL",-1);
		B_TurnToNpc(self,other);
		AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_05");	//...��� � ���!
		AI_Output(self,other,"DIA_Mortis_TeachArmor_1_01_06");	//������ �� ������ ����������� ������� ��� ���.
	};
};

func void b_mortis_teacharmor_2()
{
	AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_01");	//�������, ��������� ��������� ���, ��� � ���� ������ ���� ����������. ������ �����, ���� ���������� ����� ��� ����� ��� ��������� ��� �����...
	if(!C_BodyStateContains(self,BS_MOBINTERACT_INTERRUPT) && Wld_IsMobAvailable(self,"BSANVIL"))
	{
		AI_SetWalkMode(self,NPC_WALK);
		AI_GotoWP(self,"NW_CITY_KASERN_ARMORY_ANVIL");
		AI_AlignToWP(self);
		AI_UseMob(self,"BSANVIL",5);
		AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_02");	//...������ ����������� �������� ��������� � ���������� �� �� ��������� ��������� ������...
		AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_03");	//...� ������������ ������������������ �������� ������ ����� � ��������� ����������...
		AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_04");	//...����� ���������� �� � ������ ��������� �����, �������� ��������� ���������� ����� �����. ��� ���, ������...(����������)
		AI_UseMob(self,"BSANVIL",-1);
		B_TurnToNpc(self,other);
		AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_05");	//...� ����� �� �����!
		AI_Output(self,other,"DIA_Mortis_TeachArmor_2_01_06");	//������ ��������, ������ ���. ������� ������� ���������� ������������������ ��������.
	};
};


instance DIA_Mortis_ArUp2(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 15;
	condition = DIA_Mortis_ArUp2_Condition;
	information = DIA_Mortis_ArUp2_Info;
	permanent = TRUE;
	description = "����� ���� ������ �������.";
};


func void b_mortisarmorchoices()
{
	Info_ClearChoices(DIA_Mortis_ArUp2);
	Info_AddChoice(DIA_Mortis_ArUp2,Dialog_Back,dia_arup2_back);
	if(player_talent_smith[14] == FALSE)
	{
		Info_AddChoice(DIA_Mortis_ArUp2,B_BuildLearnString("��������� ���.������ ���������",B_GetLearnCostTalent(other,NPC_TALENT_SMITH,Armor_Mil_L2)),dia_arup2_teach_armor_mil_l2);
	};
	if((player_talent_smith[15] == FALSE) && (player_talent_smith[14] == TRUE))
	{
		Info_AddChoice(DIA_Mortis_ArUp2,B_BuildLearnString("��������� ���.������ ��������� X2",B_GetLearnCostTalent(other,NPC_TALENT_SMITH,Armor_MIl_L3)),dia_arup2_teach_armor_mil_l3);
	};
};

func int DIA_Mortis_ArUp2_Condition()
{
	if(((ArOk4 == TRUE) && (Npc_GetTalentSkill(other,NPC_TALENT_SMITH) > 0) && (other.guild == GIL_MIL)) || (other.guild == GIL_PAL))
	{
		if((player_talent_smith[14] == FALSE) && (player_talent_smith[15] == FALSE) && (player_talent_smith[16] == FALSE) && (player_talent_smith[17] == FALSE) && (player_talent_smith[18] == FALSE))
		{
			return TRUE;
		};
	};
};

func void DIA_Mortis_ArUp2_Info()
{
	AI_Output(other,self,"Dia_Mortis_ArUp2_15_00");	//����� ���� ������ �������.
	if(Wld_IsTime(7,10,20,59))
	{
		AI_Output(self,other,"Dia_Mortis_ArUp2_13_01");	//������, ��� �� ������ �����...
		b_mortisarmorchoices();
	}
	else
	{
		AI_Output(self,other,"Dia_Mortis_ArUp2_13_02");	//������, �� ������ �� ���� �� �����! � � ���� ��, �� ������� � ��� �������� ��������.
		AI_Output(self,other,"Dia_Mortis_ArUp2_13_03");	//������� �����, ����� � ���� � �����. ����� � ��������� �� ����.
	};
};

func void dia_arup2_back()
{
	Info_ClearChoices(DIA_Mortis_ArUp2);
};

func void dia_arup2_teach_armor_mil_l2()
{
	if(Npc_HasItems(hero,ItMi_Gold) >= 100)
	{
		if(B_TeachPlayerTalentSmith(self,other,Armor_Mil_L2))
		{
			Npc_RemoveInvItems(hero,ItMi_Gold,100);
			b_mortis_teacharmor_2();
		};
	}
	else
	{
		AI_Output(self,other,"DIA_Mortis_TeachArmor_01_00");	//�� � ���� �� �� ������� ������! ����� � ���� �� ���� �� �������...(�����������)
	};
	b_mortisarmorchoices();
};

func void dia_arup2_teach_armor_mil_l3()
{
	if(Npc_HasItems(hero,ItMi_Gold) >= 150)
	{
		if(B_TeachPlayerTalentSmith(self,other,Armor_MIl_L3))
		{
			Npc_RemoveInvItems(hero,ItMi_Gold,150);
			b_mortis_teacharmor_1();
		};
	}
	else
	{
		AI_Output(self,other,"DIA_Mortis_TeachArmor_01_00");	//�� � ���� �� �� ������� ������! ����� � ���� �� ���� �� �������...(�����������)
	};
	b_mortisarmorchoices();
};


instance DIA_Mortis_Waffe(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 2;
	condition = DIA_Mortis_Waffe_Condition;
	information = DIA_Mortis_Waffe_Info;
	permanent = FALSE;
	description = "� ��� ���?";
};


func int DIA_Mortis_Waffe_Condition()
{
	if((MIS_Andre_Peck == LOG_Running) && (!Npc_KnowsInfo(other,DIA_Peck_FOUND_PECK) && (Kapitel < 3)))
	{
		return TRUE;
	};
};

func void DIA_Mortis_Waffe_Info()
{
	AI_Output(other,self,"DIA_Mortis_Waffe_15_00");	//� ��� ���?
	AI_Output(self,other,"DIA_Mortis_Waffe_13_01");	//�� ������ ��� ������� � ���� ����? ��, ����� ����� ����������.
	AI_Output(self,other,"DIA_Mortis_Waffe_13_02");	//��� ���� � ����� ��� �����. ����� ���������, ��� �� ���������� ���� � ������� ������.
};


instance DIA_Mortis_Paket(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 2;
	condition = DIA_Mortis_Paket_Condition;
	information = DIA_Mortis_Paket_Info;
	permanent = FALSE;
	description = "�� ���-������ ������ � ���� ������?";
};


func int DIA_Mortis_Paket_Condition()
{
	if(MIS_Andre_WAREHOUSE == LOG_Running)
	{
		return TRUE;
	};
};

func void DIA_Mortis_Paket_Info()
{
	AI_Output(other,self,"DIA_Mortis_Paket_15_00");	//�� ���-������ ������ � ���� ������?
	AI_Output(self,other,"DIA_Mortis_Paket_13_01");	//��... ��������� ���, ����� � ��� � �������� ������, � ������, ��� ������ ������� �� ���� � �����-�� ������.
	AI_Output(other,self,"DIA_Mortis_Paket_15_02");	//��� ��� �� ������?
	AI_Output(self,other,"DIA_Mortis_Paket_13_03");	//������� �� ����. �� �� ������, ��� ����� �������� ����� ��� �������� ����� - � ����� ��� ����� ����� ��� ������ ����.
	B_LogEntry(TOPIC_Warehouse,"������ ������, ��� ���������� ������ � ���-�� ��� ������������� � ���� � �������� �������. ��� ���������, ��� ����� ������� ���.");
};


instance DIA_Mortis_Redlight(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 2;
	condition = DIA_Mortis_Redlight_Condition;
	information = DIA_Mortis_Redlight_Info;
	permanent = FALSE;
	description = "�� ������ ������ �������� �������?";
};


func int DIA_Mortis_Redlight_Condition()
{
	if(MIS_Andre_REDLIGHT == LOG_Running)
	{
		return TRUE;
	};
};

func void DIA_Mortis_Redlight_Info()
{
	AI_Output(other,self,"DIA_Mortis_Redlight_15_00");	//�� ������ ������ �������� �������? � ���� ����� ����, ��� ������� �������� �����.
	AI_Output(self,other,"DIA_Mortis_Redlight_13_01");	//��-�... ����� ��� �� �������� �������������, � ��� �� ����� ������ �� ������ ���������� ���������.
	AI_Output(self,other,"DIA_Mortis_Redlight_13_02");	//���� �� ������ ��������� ���-���� ���, ���� �����... ���, �� ������ ����� ���� �������.
	AI_Output(other,self,"DIA_Mortis_Redlight_15_03");	//������, � ��� ������?
	AI_Output(self,other,"DIA_Mortis_Redlight_13_04");	//����� � ������� - ��������, ����� ������ ����� ��� �������. ���� ���� ������ ������� ���-���� ������, �� �� ������� ��� ���.
	B_LogEntry(TOPIC_Redlight,"������ ��������, ��� ���� � ���� ������ �������� ����� � �������� ��������, ��� ����� ����� ��� �������. ����� ����� �������� �������� � ������� ��� �������.");
};


instance DIA_Mortis_CanTeach(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 5;
	condition = DIA_Mortis_CanTeach_Condition;
	information = DIA_Mortis_CanTeach_Info;
	permanent = TRUE;
	description = "� ���� ����� �������.";
};


func int DIA_Mortis_CanTeach_Condition()
{
	if(Mortis_TeachSTR == FALSE)
	{
		return TRUE;
	};
};


var int Mortis_CanTeach_Info_Once;

func void DIA_Mortis_CanTeach_Info()
{
	AI_Output(other,self,"DIA_Mortis_CanTeach_15_00");	//� ���� ����� �������.
	if((other.guild == GIL_MIL) || (other.guild == GIL_PAL))
	{
		AI_Output(self,other,"DIA_Mortis_CanTeach_13_01");	//�������. ���� � ���� ���������� �����, � ���� ������������� ����.
		Mortis_TeachSTR = TRUE;
	}
	else
	{
		AI_Output(self,other,"DIA_Mortis_CanTeach_13_02");	//�������, ������. �� ���� �� �� ������� ����� �� ��� ��� ���������, � �� ���� �������� ����.
	};
	if(Mortis_CanTeach_Info_Once == FALSE)
	{
		Log_CreateTopic(TOPIC_CityTeacher,LOG_NOTE);
		B_LogEntry(TOPIC_CityTeacher,"������, ��������� ��������, ����� ������ ��� �������� ��� ����.");
		Mortis_CanTeach_Info_Once = TRUE;
	};
};


instance DIA_Mortis_Teach(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 100;
	condition = DIA_Mortis_Teach_Condition;
	information = DIA_Mortis_Teach_Info;
	permanent = TRUE;
	description = "� ���� ����� �������.";
};


func int DIA_Mortis_Teach_Condition()
{
	if(Mortis_TeachSTR == TRUE)
	{
		return TRUE;
	};
};

func void DIA_Mortis_Teach_Info()
{
	AI_Output(other,self,"DIA_Mortis_Teach_15_00");	//� ���� ����� �������.
	Info_ClearChoices(DIA_Mortis_Teach);
	Info_AddChoice(DIA_Mortis_Teach,Dialog_Back,DIA_Mortis_Teach_BACK);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR1,B_GetLearnCostAttribute(other,ATR_STRENGTH)),DIA_Mortis_Teach_1);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR5,B_GetLearnCostAttribute(other,ATR_STRENGTH) * 5),DIA_Mortis_Teach_5);
};

func void DIA_Mortis_Teach_BACK()
{
	if(other.attribute[ATR_STRENGTH] >= T_LOW)
	{
		AI_Output(self,other,"DIA_Mortis_Teach_13_00");	//�� � ��� ���������� �����. ���� �� �� ���������� � ��������, ����� ���� ������� �������.
	};
	Info_ClearChoices(DIA_Mortis_Teach);
};

func void DIA_Mortis_Teach_1()
{
	B_TeachAttributePoints(self,other,ATR_STRENGTH,1,T_LOW);
	Info_ClearChoices(DIA_Mortis_Teach);
	Info_AddChoice(DIA_Mortis_Teach,Dialog_Back,DIA_Mortis_Teach_BACK);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR1,B_GetLearnCostAttribute(other,ATR_STRENGTH)),DIA_Mortis_Teach_1);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR5,B_GetLearnCostAttribute(other,ATR_STRENGTH) * 5),DIA_Mortis_Teach_5);
};

func void DIA_Mortis_Teach_5()
{
	B_TeachAttributePoints(self,other,ATR_STRENGTH,5,T_LOW);
	Info_ClearChoices(DIA_Mortis_Teach);
	Info_AddChoice(DIA_Mortis_Teach,Dialog_Back,DIA_Mortis_Teach_BACK);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR1,B_GetLearnCostAttribute(other,ATR_STRENGTH)),DIA_Mortis_Teach_1);
	Info_AddChoice(DIA_Mortis_Teach,b_buildlearnstringforskills(PRINT_LearnSTR5,B_GetLearnCostAttribute(other,ATR_STRENGTH) * 5),DIA_Mortis_Teach_5);
};


instance DIA_Mortis_PICKPOCKET(C_Info)
{
	npc = MIL_314_Mortis;
	nr = 900;
	condition = DIA_Mortis_PICKPOCKET_Condition;
	information = DIA_Mortis_PICKPOCKET_Info;
	permanent = TRUE;
	description = Pickpocket_40;
};


func int DIA_Mortis_PICKPOCKET_Condition()
{
	return C_Beklauen(38,60);
};

func void DIA_Mortis_PICKPOCKET_Info()
{
	Info_ClearChoices(DIA_Mortis_PICKPOCKET);
	Info_AddChoice(DIA_Mortis_PICKPOCKET,Dialog_Back,DIA_Mortis_PICKPOCKET_BACK);
	Info_AddChoice(DIA_Mortis_PICKPOCKET,DIALOG_PICKPOCKET,DIA_Mortis_PICKPOCKET_DoIt);
};

func void DIA_Mortis_PICKPOCKET_DoIt()
{
	B_Beklauen();
	Info_ClearChoices(DIA_Mortis_PICKPOCKET);
};

func void DIA_Mortis_PICKPOCKET_BACK()
{
	Info_ClearChoices(DIA_Mortis_PICKPOCKET);
};

