
const int Value_Lockpick = 10;
const int Value_Key_01 = 3;
const int Value_Key_02 = 3;
const int Value_Key_03 = 3;

instance ItKe_Lockpick(C_Item)
{
	name = "�������";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MULTI;
	value = Value_Lockpick;
	visual = "ItKe_Lockpick.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_Key_01(C_Item)
{
	name = "����";
	mainflag = ITEM_KAT_NONE;
	flags = 0;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_Key_02(C_Item)
{
	name = "����";
	mainflag = ITEM_KAT_NONE;
	flags = 0;
	value = Value_Key_02;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_Key_03(C_Item)
{
	name = "����";
	mainflag = ITEM_KAT_NONE;
	flags = 0;
	value = Value_Key_03;
	visual = "ItKe_Key_03.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_01(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_02(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_03(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_04(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_05(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItKe_City_Tower_06(C_Item)
{
	name = "���� �� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_portaltempelwalkthrough_addon(C_Item)
{
	name = "������ ����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[2] = "��������� ����� � ������� �����, �� �������";
	text[3] = "��������� ���������������� ������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ITKE_Greg_ADDON_MIS(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[2] = "���� �� ������";
	text[3] = "�������� �����.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ITKE_Addon_Tavern_01(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� ��";
	text[2] = "��������� �������";
	text[3] = "� ���������� ������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_addon_esteban(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� ��������";
	text[2] = "��������� ������ �";
	text[3] = "��� �������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_orlan_teleportstation(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� ������";
	text[2] = "��������� ����� � ������";
	text[3] = "� ��� �� ��� �������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_canyonlibrary_hierarchy_books_addon(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Key_01;
	visual = "ItKe_Key_01.3ds";
	material = MAT_METAL;
	description = name;
	text[2] = "����������� ������ � ������ �����";
	text[3] = "����� ����������� ������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_addon_buddler_01(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = 0;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� �� �������";
	text[2] = "�� ������ ��������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance itke_addon_skinner(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = 0;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� �� �������";
	text[2] = "������� ��������.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ITKE_Addon_Thorus(C_Item)
{
	name = NAME_Key;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = 0;
	visual = "ItKe_Key_02.3ds";
	material = MAT_METAL;
	description = "���� ������";
	text[2] = "��������� ��� ������.";
	text[5] = NAME_Value;
	count[5] = value;
};

