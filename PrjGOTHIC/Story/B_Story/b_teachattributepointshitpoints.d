
func int b_teachattributepointshitpoints(var C_Npc slf,var C_Npc oth,var int attrib,var int points,var int teacherMAX)
{
	var string concatText;
	var int kosten;
	var int realAttribute;
	var int money;
	kosten = B_GetLearnCostAttribute(oth,attrib) * points;
	money = kosten * 250;
	if(oth.attribute[ATR_HITPOINTS_MAX] >= teacherMAX)
	{
		concatText = ConcatStrings(PRINT_NoLearnOverPersonalMAX,IntToString(teacherMAX));
		PrintScreen(concatText,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNYOUREBETTER");
		return FALSE;
	};
	if((oth.attribute[ATR_HITPOINTS_MAX] + points) > teacherMAX)
	{
		concatText = ConcatStrings(PRINT_NoLearnOverPersonalMAX,IntToString(teacherMAX));
		PrintScreen(concatText,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNOVERPERSONALMAX");
		return FALSE;
	};
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLP,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	B_RaiseAttribute(oth,ATR_HITPOINTS_MAX,points * 5);
	Npc_ChangeAttribute(oth,ATR_HITPOINTS,points * 5);
	Snd_Play("LevelUP");
	return TRUE;
};

func string B_BuildLearnString(var string text,var int kosten)
{
	var string concatText;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	return concatText;
};

func string b_buildlearnstringforalchemy(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 50;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforsmith(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 50;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforthief(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 65;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforlanguage(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 100;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforrunes(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 85;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforcircles(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 75;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforanimals(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 45;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforhitpoints(var string text,var int kosten)
{
	var string concatText;
	var int money;
	money = kosten * 250;
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforfight(var string text,var int kosten)
{
	var string concatText;
	var int money;
	if(hero.level >= 100)
	{
		money = kosten * 180;
	}
	else if(hero.level >= 70)
	{
		money = kosten * 150;
	}
	else if(hero.level >= 60)
	{
		money = kosten * 125;
	}
	else if(hero.level >= 50)
	{
		money = kosten * 100;
	}
	else if(hero.level >= 40)
	{
		money = kosten * 85;
	}
	else if(hero.level >= 30)
	{
		money = kosten * 60;
	}
	else if(hero.level >= 20)
	{
		money = kosten * 45;
	}
	else if(hero.level >= 10)
	{
		money = kosten * 25;
	}
	else
	{
		money = kosten * 10;
	};
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

func string b_buildlearnstringforskills(var string text,var int kosten)
{
	var string concatText;
	var int money;
	if(hero.level >= 100)
	{
		money = kosten * 70;
	}
	else if(hero.level >= 70)
	{
		money = kosten * 65;
	}
	else if(hero.level >= 60)
	{
		money = kosten * 60;
	}
	else if(hero.level >= 50)
	{
		money = kosten * 55;
	}
	else if(hero.level >= 45)
	{
		money = kosten * 50;
	}
	else if(hero.level >= 40)
	{
		money = kosten * 45;
	}
	else if(hero.level >= 35)
	{
		money = kosten * 40;
	}
	else if(hero.level >= 30)
	{
		money = kosten * 35;
	}
	else if(hero.level >= 25)
	{
		money = kosten * 30;
	}
	else if(hero.level >= 20)
	{
		money = kosten * 25;
	}
	else if(hero.level >= 15)
	{
		money = kosten * 20;
	}
	else if(hero.level >= 10)
	{
		money = kosten * 15;
	}
	else if(hero.level >= 5)
	{
		money = kosten * 10;
	}
	else
	{
		money = kosten * 5;
	};
	concatText = ConcatStrings(text,PRINT_Kosten);
	concatText = ConcatStrings(concatText,IntToString(kosten));
	concatText = ConcatStrings(concatText,PRINT_LP);
	concatText = ConcatStrings(concatText,PRINT_MONEYTEACH);
	concatText = ConcatStrings(concatText,IntToString(money));
	concatText = ConcatStrings(concatText,PRINT_SHULDEN2);
	return concatText;
};

