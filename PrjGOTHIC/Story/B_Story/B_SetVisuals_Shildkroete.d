
prototype Mst_Default_Shildkroete(C_Npc)
{
	name[0] = "Черепаха";
	guild = GIL_MEATBUG;
	aivar[AIV_MM_REAL_ID] = ID_Shildkroete;
	level = 1;
	attribute[ATR_STRENGTH] = 1;
	attribute[ATR_Dexterity] = 1;
	attribute[ATR_HITPOINTS_MAX] = 50;
	attribute[ATR_HITPOINTS] = 50;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 20;
	protection[PROT_EDGE] = 0;
	protection[PROT_POINT] = 0;
	protection[PROT_FIRE] = 0;
	protection[PROT_FLY] = 0;
	protection[PROT_MAGIC] = 0;
	Mdl_SetModelScale(self,0.5,0.5,0.5);
	damagetype = DAM_EDGE;
	senses = SENSE_SMELL;
	senses_range = PERC_DIST_MONSTER_ACTIVE_MAX;
	aivar[AIV_MM_FollowInWater] = FALSE;
	start_aistate = ZS_MM_AllScheduler;
	aivar[AIV_MM_WuselStart] = OnlyRoutine;
};

func void B_SetVisuals_Shildkroete()
{
	Mdl_SetVisual(self,"Schildkroete.mds");
	Mdl_SetVisualBody(self,"SCHILDKROETE_BODY",DEFAULT,DEFAULT,"",DEFAULT,DEFAULT,-1);
};


instance Shildkroete(Mst_Default_Shildkroete)
{
	B_SetVisuals_Shildkroete();
	CreateInvItems(self,ItFoShildkroeteRaw,1);
};

