
func int B_SelectSpell(var C_Npc slf,var C_Npc oth)
{
	var int dK_rnd;
	var int dK_Mega;
	if((slf.npctype == NPCTYPE_FRIEND) && Npc_IsPlayer(oth) && (oth.guild < GIL_SEPERATOR_HUM))
	{
		if((slf.guild == GIL_KDF) || (slf.guild == GIL_KDW) || (slf.aivar[AIV_MagicUser] == MAGIC_ALWAYS))
		{
			if(!Npc_HasItems(slf,ItRu_Sleep))
			{
				CreateInvItems(slf,ItRu_Sleep,1);
			};
			B_ReadySpell(slf,SPL_Sleep,SPL_Cost_Sleep);
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	if((slf.guild == GIL_DMT) && (slf.aivar[AIV_MagicUser] == MAGIC_ALWAYS))
	{
		if(!Npc_HasItems(slf,ItRu_SumSkel))
		{
			CreateInvItems(slf,ItRu_SumSkel,1);
		};
		if(!Npc_HasItems(slf,ItRu_Deathball))
		{
			CreateInvItems(slf,ItRu_Deathball,1);
		};
		if(!Npc_HasItems(slf,ItRu_FullHeal))
		{
			CreateInvItems(slf,ItRu_FullHeal,1);
		};
		if(Hlp_GetInstanceID(slf) == Hlp_GetInstanceID(DMT_1299_OberDementor_DI))
		{
			B_ReadySpell(slf,SPL_Skull,SPL_Cost_Skull);
			return TRUE;
		};
		if(slf.attribute[ATR_HITPOINTS] < 150)
		{
			B_ReadySpell(slf,SPL_FullHeal,SPL_Cost_FullHeal);
			return TRUE;
		};
		if(slf.aivar[AIV_SelectSpell] < 1)
		{
			B_ReadySpell(slf,SPL_SummonSkeleton,SPL_Cost_SummonSkeleton);
			return TRUE;
		}
		else
		{
			B_ReadySpell(slf,SPL_Deathball,SPL_COST_Deathball);
			return TRUE;
		};
		return TRUE;
	};
	if(slf.guild == GIL_KDF)
	{
		if(!Npc_HasItems(slf,ItRu_Firestorm))
		{
			CreateInvItems(slf,ItRu_Firestorm,1);
		};
		if(!Npc_HasItems(slf,ItRu_Concussionbolt))
		{
			CreateInvItems(slf,ItRu_Concussionbolt,1);
		};
		if(!Npc_HasItems(slf,ItRu_FullHeal))
		{
			CreateInvItems(slf,ItRu_FullHeal,1);
		};
		if(slf.attribute[ATR_HITPOINTS] < 200)
		{
			B_ReadySpell(slf,SPL_FullHeal,SPL_Cost_FullHeal);
			return TRUE;
		};
		if(C_NpcHasAttackReasonToKill(slf))
		{
			B_ReadySpell(slf,SPL_Firestorm,SPL_Cost_InstantFireStorm);
			return TRUE;
		}
		else if(oth.guild > GIL_SEPERATOR_HUM)
		{
			B_ReadySpell(slf,SPL_Firestorm,SPL_Cost_InstantFireStorm);
			return TRUE;
		}
		else
		{
			B_ReadySpell(slf,SPL_ConcussionBolt,SPL_COST_Concussionbolt);
			return TRUE;
		};
	};
	if(slf.guild == GIL_KDW)
	{
		if(!Npc_HasItems(slf,ItRu_LightningFlash))
		{
			CreateInvItems(slf,ItRu_LightningFlash,1);
		};
		if(!Npc_HasItems(slf,ItRu_FullHeal))
		{
			CreateInvItems(slf,ItRu_FullHeal,1);
		};
		if(!Npc_HasItems(slf,ItRu_Concussionbolt))
		{
			CreateInvItems(slf,ItRu_Concussionbolt,1);
		};
		if(!Npc_HasItems(slf,ItRu_Waterfist))
		{
			CreateInvItems(slf,ItRu_Waterfist,1);
		};
		if(!Npc_HasItems(slf,ItRu_Acid))
		{
			CreateInvItems(slf,ItRu_Acid,1);
		};
		if(slf.attribute[ATR_HITPOINTS] < 200)
		{
			B_ReadySpell(slf,SPL_FullHeal,SPL_Cost_FullHeal);
			return TRUE;
		};
		if(C_NpcHasAttackReasonToKill(slf))
		{
			if(slf.aivar[AIV_SelectSpell] <= 0)
			{
				B_ReadySpell(slf,SPL_Acid,SPL_Cost_Acid);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] <= 5)
			{
				B_ReadySpell(slf,SPL_LightningFlash,SPL_Cost_LightningFlash);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] <= 8)
			{
				B_ReadySpell(slf,SPL_WaterFist,SPL_Cost_Waterfist);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] >= 10)
			{
				slf.aivar[AIV_SelectSpell] = 0;
			};
			return TRUE;
		}
		else if(oth.guild > GIL_SEPERATOR_HUM)
		{
			if(slf.aivar[AIV_SelectSpell] <= 1)
			{
				B_ReadySpell(slf,SPL_Acid,SPL_Cost_Acid);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] <= 5)
			{
				B_ReadySpell(slf,SPL_LightningFlash,SPL_Cost_LightningFlash);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] <= 8)
			{
				B_ReadySpell(slf,SPL_WaterFist,SPL_Cost_Waterfist);
				return TRUE;
			}
			else if(slf.aivar[AIV_SelectSpell] >= 10)
			{
				slf.aivar[AIV_SelectSpell] = 0;
			};
			return TRUE;
		}
		else
		{
			B_ReadySpell(slf,SPL_ConcussionBolt,SPL_COST_Concussionbolt);
			return TRUE;
		};
	};
	if(slf.guild == GIL_PAL)
	{
		if(slf.fight_tactic == FAI_NAILED)
		{
			return FALSE;
		};
		if(!Npc_HasItems(slf,ItRu_PalHolyBolt))
		{
			CreateInvItems(slf,ItRu_PalHolyBolt,1);
		};
		if(!Npc_HasItems(slf,ItRu_PalRepelEvil))
		{
			CreateInvItems(slf,ItRu_PalRepelEvil,1);
		};
		if(!Npc_HasItems(slf,ItRu_PalFullHeal))
		{
			CreateInvItems(slf,ItRu_PalFullHeal,1);
		};
		if(slf.attribute[ATR_HITPOINTS] < 100)
		{
			B_ReadySpell(slf,SPL_PalFullHeal,SPL_Cost_PalFullHeal);
			return TRUE;
		};
		if((Npc_GetDistToNpc(slf,oth) > FIGHT_DIST_MELEE) && C_NpcIsEvil(oth))
		{
			dK_rnd = Hlp_Random(10);
			if(dK_rnd < 3)
			{
				B_ReadySpell(slf,SPL_PalRepelEvil,SPL_Cost_PalRepelEvil);
			}
			else
			{
				B_ReadySpell(slf,SPL_PalHolyBolt,SPL_Cost_PalHolyBolt);
			};
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	if(slf.guild == GIL_ICEGOLEM)
	{
		if(!Npc_HasItems(slf,ItRu_IceCube))
		{
			CreateInvItems(slf,ItRu_IceCube,1);
		};
		if((Npc_GetDistToNpc(slf,oth) < FIGHT_DIST_MELEE) || Npc_IsInState(oth,ZS_MagicFreeze))
		{
			return FALSE;
		}
		else
		{
			B_ReadySpell(slf,SPL_IceCube,SPL_Cost_IceCube);
			return TRUE;
		};
	};
	if(slf.guild == GIL_FIREGOLEM)
	{
		if(!Npc_HasItems(slf,ItRu_InstantFireball))
		{
			CreateInvItems(slf,ItRu_InstantFireball,1);
		};
		if(Npc_GetDistToNpc(slf,oth) > FIGHT_DIST_MELEE)
		{
			B_ReadySpell(slf,SPL_InstantFireball,SPL_COST_InstantFireball);
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	if(slf.aivar[AIV_MM_REAL_ID] == ID_ORCSHAMAN)
	{
		if(!Npc_HasItems(slf,ItRu_Deathball))
		{
			CreateInvItems(slf,ItRu_Deathball,1);
		};
		if(!Npc_HasItems(slf,ItRu_SummonOrc))
		{
			CreateInvItems(slf,ItRu_SummonOrc,1);
		};
		if(!Npc_HasItems(slf,ItRu_MediumHeal))
		{
			CreateInvItems(slf,ItRu_MediumHeal,1);
		};
		if(slf.attribute[ATR_HITPOINTS] < 150)
		{
			B_ReadySpell(slf,SPL_MediumHeal,SPL_Cost_MediumHeal);
			return TRUE;
		};
		if(slf.aivar[AIV_SelectSpell] < 1)
		{
			B_ReadySpell(slf,SPL_SummonOrc,SPL_Cost_SummonOrc);
			return TRUE;
		}
		else
		{
			B_ReadySpell(slf,SPL_Deathball,SPL_COST_Deathball);
			return TRUE;
		};
		return TRUE;
	};
	if(slf.aivar[AIV_MM_REAL_ID] == ID_DrShaman)
	{
		if(!Npc_HasItems(slf,ItRu_AdanosBall))
		{
			CreateInvItems(slf,ItRu_AdanosBall,1);
		};
		if(Npc_GetDistToNpc(slf,oth) > FIGHT_DIST_MELEE)
		{
			B_ReadySpell(slf,SPL_AdanosBall,SPL_Cost_AdanosBall);
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	if(slf.aivar[AIV_MM_REAL_ID] == ID_MAG_LORD)
	{
		if(!Npc_HasItems(slf,ItRu_Icelance))
		{
			CreateInvItems(slf,ItRu_Icelance,1);
		};
		if(!Npc_HasItems(slf,ItRu_FullHeal))
		{
			CreateInvItems(slf,ItRu_FullHeal,1);
		};
		if(self.attribute[ATR_HITPOINTS] < 150)
		{
			B_ReadySpell(slf,SPL_FullHeal,SPL_Cost_FullHeal);
			return TRUE;
		}
		else
		{
			B_ReadySpell(slf,SPL_IceLance,SPL_Cost_Icelance);
			return TRUE;
		};
	};
	if(Hlp_GetInstanceID(slf) == Hlp_GetInstanceID(VLK_414_Hanna))
	{
		if(!Npc_IsInState(oth,ZS_MagicFreeze) && Npc_HasItems(slf,ItSc_IceCube))
		{
			B_ReadySpell(slf,SPL_IceCube,SPL_Cost_Scroll);
			return TRUE;
		}
		else if(Npc_HasItems(slf,ItSc_Firestorm))
		{
			B_ReadySpell(slf,SPL_Firestorm,SPL_Cost_Scroll);
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	return FALSE;
};

