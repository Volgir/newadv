
const int SPL_Cost_MassFire = 95;
const int SPL_Damage_MassFire = 200;

instance SPELL_MASSFIRE(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_Damage_MassFire;
	damagetype = DAM_FIRE;
	targetCollectAlgo = TARGET_COLLECT_FOCUS_FALLBACK_NONE;
	targetCollectType = TARGET_TYPE_NPCS;
};


func int Spell_Logic_MassFire(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll6))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_MassFire)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_MassFire()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll6;
	}
	else
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_MassFire;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_Heal_HeavenLight = 100;
const int SPL_Cost_HeavenLight = 20;

instance Spell_HeavenLight(C_Spell_Proto)
{
	time_per_mana = 0;
	spellType = SPELL_GOOD;
	targetCollectAlgo = TARGET_COLLECT_FOCUS;
	targetCollectRange = 1000;
	targetCollectType = TARGET_TYPE_NPCS;
	damage_per_level = SPL_Heal_HeavenLight;
};


func int Spell_Logic_HeavenLight(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_HeavenLight)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_HeavenLight()
{
	if(Npc_GetActiveSpell(self) == SPL_HeavenLight)
	{
		if(Npc_GetActiveSpellIsScroll(self))
		{
			self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll2;
		}
		else
		{
			self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_HeavenLight;
		};
	};
};


const int SPL_Cost_Acid = 5;
const int SPL_Damage_Acid = 1;
const int SPL_Time_Acid = 8;
const int SPL_DmgPerSec_Acid = 2;

instance Spell_Acid(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_Damage_Acid;
	damagetype = DAM_Magic;
};


func int Spell_Logic_Acid(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_Acid)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_Acid()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Acid;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_Cost_AdanosBall = 35;
const int SPL_Damage_AdanosBall = 120;

instance Spell_AdanosBall(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_Damage_AdanosBall;
	damagetype = DAM_Magic;
};


func int Spell_Logic_AdanosBall(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_AdanosBall)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_AdanosBall()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_AdanosBall;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_Cost_Explosion = 60;
const int SPL_Damage_explosion = 165;

instance Spell_Explosion(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_Damage_explosion;
	damagetype = DAM_FIRE;
};


func int Spell_Logic_Explosion(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll5))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_Explosion)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_Explosion()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll5;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Explosion;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_Cost_Quake = 5;
const int SPL_Damage_Quake = 100;

instance Spell_Quake(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_Damage_Quake;
	targetCollectAlgo = TARGET_COLLECT_NONE;
	targetCollectRange = 0;
};


func int Spell_Logic_Quake(var int manaInvested)
{
	if(manaInvested >= 1)
	{
		return SPL_SENDCAST;
	};
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll))
	{
		return SPL_NEXTLEVEL;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_Quake)
	{
		return SPL_NEXTLEVEL;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_Quake()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Quake;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_COST_swampball = 15;
const int SPL_DAMAGE_swampball = 75;

instance Spell_Swampfist(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_DAMAGE_swampball;
	damagetype = DAM_Magic;
};


func int Spell_Logic_Swampfist(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_COST_swampball)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
	return SPL_SENDSTOP;
};

func void Spell_Cast_Swampfist()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_swampball;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


instance Spell_Swampfistsmall(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_DAMAGE_swampball;
	damagetype = DAM_Magic;
};


func int Spell_Logic_Swampfistsmall(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_COST_swampball)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
	return SPL_SENDSTOP;
};

func void Spell_Cast_Swampfistsmall()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_swampball;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_COST_IceBall = 15;
const int SPL_DAMAGE_IceBall = 75;

instance Spell_IceBall(C_Spell_Proto)
{
	time_per_mana = 0;
	damage_per_level = SPL_DAMAGE_IceBall;
	damagetype = DAM_Magic;
};


func int Spell_Logic_IceBall(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_COST_IceBall)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
	return SPL_SENDSTOP;
};

func void Spell_Cast_IceBall()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_IceBall;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_COST_FIRELIGHT = 5;
const int SPL_Damage_PYRO_PER_SEC = 35;

instance SPELL_FIRELIGHT(C_Spell_Proto)
{
	time_per_mana = 250;
	damage_per_level = SPL_Damage_PYRO_PER_SEC;
	spellType = SPELL_BAD;
	damagetype = DAM_FIRE;
	targetCollectAlgo = TARGET_COLLECT_FOCUS;
	targetCollectRange = 1000;
};


func int spell_logic_firelight(var int manaInvested)
{
	if((self.guild == GIL_FIREGOLEM) || (self.aivar[AIV_MM_REAL_ID] == ID_FIREWARAN) || (self.aivar[AIV_MM_REAL_ID] == ID_DRAGON_FIRE))
	{
		B_Say(self,self,"$DONTWORK");
		return SPL_SENDSTOP;
	};
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_COST_FIRELIGHT))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_FIRELIGHT;
		return SPL_NEXTLEVEL;
	};
	if(self.attribute[ATR_MANA] >= SPL_COST_FIRELIGHT)
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_FIRELIGHT;
		return SPL_NEXTLEVEL;
	}
	else
	{
		return SPL_SENDSTOP;
	};
	return SPL_SENDSTOP;
};

func void Spell_Cast_firelight()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_Cost_Scroll;
	}
	else
	{
		self.attribute[ATR_MANA] = self.attribute[ATR_MANA] - SPL_COST_IceBall;
	};
	self.aivar[AIV_SelectSpell] += 1;
};


const int SPL_Cost_SummonIceGolem = 60;

instance Spell_SummonIceGolem(C_Spell_Proto)
{
	time_per_mana = 0;
	spellType = SPELL_BAD;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonIceGolem(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonIceGolem)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_SummonIceGolem()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonIceGolem;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		Wld_StopEffect("SLOW_MOTION");
		Wld_SpawnNpcRange(self,Summoned_IceGolem,1,500);
	}
	else
	{
		Wld_SpawnNpcRange(self,IceGolem,1,500);
	};
};


const int SPL_Cost_SummonIceWolf = 50;

instance Spell_SummonIceWolf(C_Spell_Proto)
{
	time_per_mana = 0;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonIceWolf(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonIceWolf)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_SummonIceWolf()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonIceWolf;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		Wld_StopEffect("SLOW_MOTION");
		Wld_SpawnNpcRange(self,Summoned_IceWolf,1,500);
	}
	else
	{
		Wld_SpawnNpcRange(self,IceWolf,1,500);
	};
};


const int SPL_Cost_SummonFireGolem = 50;

instance Spell_SummonFireGolem(C_Spell_Proto)
{
	time_per_mana = 0;
	spellType = SPELL_BAD;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonFireGolem(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonFireGolem)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_SummonFireGolem()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonFireGolem;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		Wld_StopEffect("SLOW_MOTION");
		Wld_SpawnNpcRange(self,Summoned_FireGolem,1,500);
	}
	else
	{
		Wld_SpawnNpcRange(self,FireGolem,1,500);
	};
};


const int SPL_Cost_SummonOrc = 100;

instance Spell_SummonOrc(C_Spell_Proto)
{
	time_per_mana = 0;
	spellType = SPELL_BAD;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonOrc(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll4))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonOrc)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_SummonOrc()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll4;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonOrc;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		Wld_StopEffect("SLOW_MOTION");
		Wld_SpawnNpcRange(self,SkeletonOrc,1,500);
	}
	else
	{
		Wld_SpawnNpcRange(self,SkeletonOrc,1,500);
	};
};


const int SPL_Cost_SummonFireWaran = 35;

instance Spell_SummonFireWaran(C_Spell_Proto)
{
	time_per_mana = 0;
	spellType = SPELL_BAD;
	targetCollectAlgo = TARGET_COLLECT_NONE;
};


func int Spell_Logic_SummonFireWaran(var int manaInvested)
{
	if(Npc_GetActiveSpellIsScroll(self) && (self.attribute[ATR_MANA] >= SPL_Cost_Scroll2))
	{
		return SPL_SENDCAST;
	}
	else if(self.attribute[ATR_MANA] >= SPL_Cost_SummonFireWaran)
	{
		return SPL_SENDCAST;
	}
	else
	{
		return SPL_SENDSTOP;
	};
};

func void Spell_Cast_SummonFireWaran()
{
	if(Npc_GetActiveSpellIsScroll(self))
	{
		self.attribute[ATR_MANA] -= SPL_Cost_Scroll2;
	}
	else
	{
		self.attribute[ATR_MANA] -= SPL_Cost_SummonFireWaran;
	};
	self.aivar[AIV_SelectSpell] += 1;
	if(Npc_IsPlayer(self))
	{
		Wld_StopEffect("SLOW_MOTION");
		Wld_SpawnNpcRange(self,Summoned_FireWaran,1,500);
	}
	else
	{
		Wld_SpawnNpcRange(self,FireWaran,1,500);
	};
};

func int Spell_ProcessMana(var int manaInvested)
{
	var int activeSpell;
	activeSpell = Npc_GetActiveSpell(self);
	if(activeSpell == SPL_PalLight)
	{
		return Spell_Logic_PalLight(manaInvested);
	};
	if(activeSpell == SPL_PalLightHeal)
	{
		return Spell_Logic_PalLightHeal(manaInvested);
	};
	if(activeSpell == SPL_PalHolyBolt)
	{
		return Spell_Logic_PalHolyBolt(manaInvested);
	};
	if(activeSpell == SPL_PalMediumHeal)
	{
		return Spell_Logic_PalMediumHeal(manaInvested);
	};
	if(activeSpell == SPL_PalRepelEvil)
	{
		return Spell_Logic_PalRepelEvil(manaInvested);
	};
	if(activeSpell == SPL_PalFullHeal)
	{
		return Spell_Logic_PalFullHeal(manaInvested);
	};
	if(activeSpell == SPL_PalDestroyEvil)
	{
		return Spell_Logic_PalDestroyEvil(manaInvested);
	};
	if(activeSpell == SPL_PalTeleportSecret)
	{
		return Spell_Logic_PalTeleportSecret(manaInvested);
	};
	if(activeSpell == SPL_TeleportSeaport)
	{
		return Spell_Logic_TeleportSeaport(manaInvested);
	};
	if(activeSpell == SPL_TeleportMonastery)
	{
		return Spell_Logic_TeleportMonastery(manaInvested);
	};
	if(activeSpell == SPL_TeleportFarm)
	{
		return Spell_Logic_TeleportFarm(manaInvested);
	};
	if(activeSpell == SPL_TeleportXardas)
	{
		return Spell_Logic_TeleportXardas(manaInvested);
	};
	if(activeSpell == SPL_TeleportPassNW)
	{
		return Spell_Logic_TeleportPassNW(manaInvested);
	};
	if(activeSpell == SPL_TeleportPassOW)
	{
		return Spell_Logic_TeleportPassOW(manaInvested);
	};
	if(activeSpell == SPL_TeleportOC)
	{
		return Spell_Logic_TeleportOC(manaInvested);
	};
	if(activeSpell == SPL_TeleportOWDemonTower)
	{
		return Spell_Logic_TeleportOWDemonTower(manaInvested);
	};
	if(activeSpell == SPL_TeleportTaverne)
	{
		return Spell_Logic_TeleportTaverne(manaInvested);
	};
	if(activeSpell == SPL_Light)
	{
		return Spell_Logic_Light(manaInvested);
	};
	if(activeSpell == SPL_Firebolt)
	{
		return Spell_Logic_Firebolt(manaInvested);
	};
	if(activeSpell == SPL_Icebolt)
	{
		return Spell_Logic_IceBolt(manaInvested);
	};
	if(activeSpell == SPL_Zap)
	{
		return Spell_Logic_Zap(manaInvested);
	};
	if(activeSpell == SPL_LightHeal)
	{
		return Spell_Logic_LightHeal(manaInvested);
	};
	if(activeSpell == SPL_SummonGoblinSkeleton)
	{
		return Spell_Logic_SummonGoblinSkeleton(manaInvested);
	};
	if(activeSpell == SPL_InstantFireball)
	{
		return Spell_Logic_InstantFireball(manaInvested);
	};
	if(activeSpell == SPL_SummonWolf)
	{
		return Spell_Logic_SummonWolf(manaInvested);
	};
	if(activeSpell == SPL_WindFist)
	{
		return Spell_Logic_WindFist(manaInvested);
	};
	if(activeSpell == SPL_Sleep)
	{
		return Spell_Logic_Sleep(manaInvested);
	};
	if(activeSpell == SPL_MediumHeal)
	{
		return Spell_Logic_MediumHeal(manaInvested);
	};
	if(activeSpell == SPL_LightningFlash)
	{
		return Spell_Logic_LightningFlash(manaInvested);
	};
	if(activeSpell == SPL_ChargeFireball)
	{
		return Spell_Logic_ChargeFireball(manaInvested);
	};
	if(activeSpell == SPL_ChargeZap)
	{
		return Spell_Logic_ChargeZap(manaInvested);
	};
	if(activeSpell == SPL_SummonSkeleton)
	{
		return Spell_Logic_SummonSkeleton(manaInvested);
	};
	if(activeSpell == SPL_Fear)
	{
		return Spell_Logic_Fear(manaInvested);
	};
	if(activeSpell == SPL_IceCube)
	{
		return Spell_Logic_IceCube(manaInvested);
	};
	if(activeSpell == SPL_ChargeZap)
	{
		return Spell_Logic_ChargeZap(manaInvested);
	};
	if(activeSpell == SPL_SummonGolem)
	{
		return Spell_Logic_SummonGolem(manaInvested);
	};
	if(activeSpell == SPL_DestroyUndead)
	{
		return Spell_Logic_DestroyUndead(manaInvested);
	};
	if(activeSpell == SPL_Pyrokinesis)
	{
		return Spell_Logic_Pyrokinesis(manaInvested);
	};
	if(activeSpell == SPL_Firestorm)
	{
		return Spell_Logic_Firestorm(manaInvested);
	};
	if(activeSpell == SPL_IceWave)
	{
		return Spell_Logic_IceWave(manaInvested);
	};
	if(activeSpell == SPL_SummonDemon)
	{
		return Spell_Logic_SummonDemon(manaInvested);
	};
	if(activeSpell == SPL_FullHeal)
	{
		return Spell_Logic_FullHeal(manaInvested);
	};
	if(activeSpell == SPL_Firerain)
	{
		return Spell_Logic_Firerain(manaInvested);
	};
	if(activeSpell == SPL_BreathOfDeath)
	{
		return Spell_Logic_BreathOfDeath(manaInvested);
	};
	if(activeSpell == SPL_MassDeath)
	{
		return Spell_Logic_Massdeath(manaInvested);
	};
	if(activeSpell == SPL_ArmyOfDarkness)
	{
		return Spell_Logic_ArmyOfDarkness(manaInvested);
	};
	if(activeSpell == SPL_Shrink)
	{
		return Spell_Logic_Shrink(manaInvested);
	};
	if(activeSpell == SPL_TrfSheep)
	{
		return Spell_Logic_TrfSheep(manaInvested);
	};
	if(activeSpell == SPL_TrfScavenger)
	{
		return Spell_Logic_TrfScavenger(manaInvested);
	};
	if(activeSpell == SPL_TrfGiantRat)
	{
		return Spell_Logic_TrfGiantRat(manaInvested);
	};
	if(activeSpell == SPL_TrfMeatBug)
	{
		return Spell_Logic_TrfMeatBug(manaInvested);
	};
	if(activeSpell == SPL_TrfWolf)
	{
		return Spell_Logic_TrfWolf(manaInvested);
	};
	if(activeSpell == SPL_TrfWaran)
	{
		return Spell_Logic_TrfWaran(manaInvested);
	};
	if(activeSpell == SPL_TrfSnapper)
	{
		return Spell_Logic_TrfSnapper(manaInvested);
	};
	if(activeSpell == SPL_TrfWarg)
	{
		return Spell_Logic_TrfWarg(manaInvested);
	};
	if(activeSpell == SPL_TrfFireWaran)
	{
		return Spell_Logic_TrfFireWaran(manaInvested);
	};
	if(activeSpell == SPL_TrfLurker)
	{
		return Spell_Logic_TrfLurker(manaInvested);
	};
	if(activeSpell == SPL_TrfShadowbeast)
	{
		return Spell_Logic_TrfShadowbeast(manaInvested);
	};
	if(activeSpell == SPL_TrfDragonSnapper)
	{
		return Spell_Logic_TrfDragonSnapper(manaInvested);
	};
	if(activeSpell == SPL_Charm)
	{
		return Spell_Logic_Charm(manaInvested);
	};
	if(activeSpell == SPL_MasterOfDisaster)
	{
		return Spell_Logic_MasterOfDisaster(manaInvested);
	};
	if(activeSpell == SPL_ConcussionBolt)
	{
		return Spell_Logic_Concussionbolt(manaInvested);
	};
	if(activeSpell == SPL_MassFire)
	{
		return Spell_Logic_MassFire(manaInvested);
	};
	if(activeSpell == SPL_HeavenLight)
	{
		return Spell_Logic_HeavenLight(manaInvested);
	};
	if(activeSpell == SPL_Acid)
	{
		return Spell_Logic_Acid(manaInvested);
	};
	if(activeSpell == SPL_AdanosBall)
	{
		return Spell_Logic_AdanosBall(manaInvested);
	};
	if(activeSpell == SPL_Explosion)
	{
		return Spell_Logic_Explosion(manaInvested);
	};
	if(activeSpell == SPL_Deathbolt)
	{
		return Spell_Logic_Deathbolt(manaInvested);
	};
	if(activeSpell == SPL_Deathball)
	{
		return Spell_Logic_Deathball(manaInvested);
	};
	if(activeSpell == SPL_Thunderstorm)
	{
		return Spell_Logic_Thunderstorm(manaInvested);
	};
	if(activeSpell == SPL_WaterFist)
	{
		return Spell_Logic_Waterfist(manaInvested);
	};
	if(activeSpell == SPL_Whirlwind)
	{
		return Spell_Logic_Whirlwind(manaInvested);
	};
	if(activeSpell == SPL_Geyser)
	{
		return Spell_Logic_Geyser(manaInvested);
	};
	if(activeSpell == SPL_Quake)
	{
		return Spell_Logic_Quake(manaInvested);
	};
	if(activeSpell == SPL_IceBall)
	{
		return Spell_Logic_IceBall(manaInvested);
	};
	if(activeSpell == SPL_Inflate)
	{
		return Spell_Logic_Inflate(manaInvested);
	};
	if(activeSpell == SPL_IceLance)
	{
		return Spell_Logic_Icelance(manaInvested);
	};
	if(activeSpell == SPL_Swarm)
	{
		return Spell_Logic_Swarm(manaInvested);
	};
	if(activeSpell == SPL_GreenTentacle)
	{
		return Spell_Logic_Greententacle(manaInvested);
	};
	if(activeSpell == SPL_SummonGuardian)
	{
		return Spell_Logic_SummonGuardian(manaInvested);
	};
	if(activeSpell == SPL_Energyball)
	{
		return Spell_Logic_EnergyBall(manaInvested);
	};
	if(activeSpell == SPL_SuckEnergy)
	{
		return Spell_Logic_SuckEnergy(manaInvested);
	};
	if(activeSpell == SPL_Skull)
	{
		return Spell_Logic_Skull(manaInvested);
	};
	if(activeSpell == SPL_SummonZombie)
	{
		return Spell_Logic_SummonZombie(manaInvested);
	};
	if(activeSpell == SPL_SummonMud)
	{
		return Spell_Logic_SummonMud(manaInvested);
	};
	if(activeSpell == SPL_Swampfist)
	{
		return Spell_Logic_Swampfist(manaInvested);
	};
	if(activeSpell == SPL_swampfistsmall)
	{
		return Spell_Logic_Swampfistsmall(manaInvested);
	};
	if(activeSpell == SPL_FireLight)
	{
		return spell_logic_firelight(manaInvested);
	};
	if(activeSpell == SPL_SummonIceGolem)
	{
		return Spell_Logic_SummonIceGolem(manaInvested);
	};
	if(activeSpell == SPL_SummonIceWolf)
	{
		return Spell_Logic_SummonIceWolf(manaInvested);
	};
	if(activeSpell == SPL_SummonFireGolem)
	{
		return Spell_Logic_SummonFireGolem(manaInvested);
	};
	if(activeSpell == SPL_SummonOrc)
	{
		return Spell_Logic_SummonOrc(manaInvested);
	};
	if(activeSpell == SPL_SummonFireWaran)
	{
		return Spell_Logic_SummonFireWaran(manaInvested);
	};
	return FALSE;
};

