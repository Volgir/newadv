
prototype Mst_Default_Skeleton_Mage(C_Npc)
{
	name[0] = "������-���";
	guild = GIL_SKELETON;
	aivar[AIV_MM_REAL_ID] = ID_MAG_LORD;
	level = 50;
	attribute[ATR_STRENGTH] = 150;
	attribute[ATR_Dexterity] = 150;
	attribute[ATR_HITPOINTS_MAX] = 300;
	attribute[ATR_HITPOINTS] = 300;
	attribute[ATR_MANA_MAX] = 200;
	attribute[ATR_MANA] = 200;
	protection[PROT_BLUNT] = 25;
	protection[PROT_EDGE] = 70;
	protection[PROT_POINT] = 1000;
	protection[PROT_FIRE] = 80;
	protection[PROT_FLY] = 40;
	protection[PROT_MAGIC] = 40;
	damagetype = DAM_Magic;
	fight_tactic = FAI_HUMAN_NORMAL;
	EquipItem(self,ItMw_1h_Nov_Mace);
	senses = SENSE_HEAR | SENSE_SEE | SENSE_SMELL;
	senses_range = PERC_DIST_ORC_ACTIVE_MAX;
	aivar[AIV_MM_FollowTime] = 5;
	aivar[AIV_MM_FollowInWater] = FALSE;
	aivar[AIV_MM_Packhunter] = TRUE;
	start_aistate = ZS_MM_AllScheduler;
	aivar[AIV_MM_RestStart] = OnlyRoutine;
};

func void B_SetVisuals_Skeleton_Mage()
{
	Mdl_SetVisual(self,"HumanS.mds");
	Mdl_ApplyOverlayMds(self,"humans_1hST1.mds");
	Mdl_ApplyOverlayMds(self,"humans_2hST3.mds");
	Mdl_ApplyOverlayMds(self,"humans_BowT1.mds");
	Mdl_ApplyOverlayMds(self,"humans_CBowT1.mds");
	B_SetNpcVisual(self,MALE,"Ske_Head",0,0,itar_kdf_skel);
};


instance SkeletonMage(Mst_Default_Skeleton_Mage)
{
	B_SetVisuals_Skeleton_Mage();
};

instance SkeletonMage_Angar(Mst_Default_Skeleton_Mage)
{
	B_SetVisuals_Skeleton_Mage();
	CreateInvItems(self,ItAm_Mana_Angar_MIS,1);
	CreateInvItems(self,ItPo_Perm_Mana,1);
};

instance SecretLibrarySkeleton(Mst_Default_Skeleton_Mage)
{
	B_SetVisuals_Skeleton_Mage();
};

instance Summoned_Skeleton3(Mst_Default_Skeleton_Mage)
{
	name[0] = "��������� ������-���";
	guild = gil_summoned_skeleton;
	aivar[AIV_MM_REAL_ID] = ID_Summoned_SkeletonMage;
	level = 0;
	attribute[ATR_STRENGTH] = 100;
	attribute[ATR_Dexterity] = 150;
	attribute[ATR_HITPOINTS_MAX] = 150;
	attribute[ATR_HITPOINTS] = 150;
	attribute[ATR_MANA_MAX] = 0;
	attribute[ATR_MANA] = 0;
	protection[PROT_BLUNT] = 50;
	protection[PROT_EDGE] = 125;
	protection[PROT_POINT] = 1000;
	protection[PROT_FIRE] = 125;
	protection[PROT_FLY] = 125;
	fight_tactic = FAI_HUMAN_NORMAL;
	aivar[AIV_PARTYMEMBER] = TRUE;
	B_SetAttitude(self,ATT_FRIENDLY);
	start_aistate = ZS_MM_Rtn_Summoned;
	B_SetVisuals_Skeleton_Mage();
	EquipItem(self,ItMw_1h_Nov_Mace);
};

