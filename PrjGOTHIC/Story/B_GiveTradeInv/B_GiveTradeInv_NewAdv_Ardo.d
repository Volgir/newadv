
var int Ardo_ItemsGiven_Chapter_1;
var int Ardo_ItemsGiven_Chapter_2;
var int Ardo_ItemsGiven_Chapter_3;
var int Ardo_ItemsGiven_Chapter_4;
var int Ardo_ItemsGiven_Chapter_5;

func void B_GiveTradeInv_Ardo(var C_Npc slf)
{
	if((Kapitel >= 1) && (Ardo_ItemsGiven_Chapter_1 == FALSE))
	{
		/*CreateInvItems(slf,ITMW_1H_VLK_AXE,1);
		CreateInvItems(slf,ITMW_SENSE,1);
		CreateInvItems(slf,ITMW_SHORTSWORD3,1);
		CreateInvItems(slf,ITMW_ADDON_PIR2HAXE,1);
		CreateInvItems(slf,ITRW_BOW_L_03,1);
		CreateInvItems(slf,ITMI_SILVERRING,1);
		CreateInvItems(slf,ITPO_HEALTH_01,4);*/
		
		CreateInvItems(slf,ItMi_Gold,100);
		CreateInvItems(slf,ItFo_Bread,11);
		CreateInvItems(slf,ItFoMuttonRaw,7);
		CreateInvItems(slf,ItFo_Bacon,1);
		CreateInvItems(slf,ItFo_Sausage,4);
		CreateInvItems(slf,ItFo_Milk, 4);
		CreateInvItems(slf,ITMI_COAL,20);
		CreateInvItems(slf,ITMI_MOLERATLUBRIC_MIS,1);
		CreateInvItems(slf,ITPL_MUSHROOM_01,10);
		CreateInvItems(slf,ITPL_MUSHROOM_02,5);
		CreateInvItems(slf,ITPL_PLANEBERRY,19);
		CreateInvItems(slf,ITPL_SPEED_HERB_01,1);
		CreateInvItems(slf,ItRw_Arrow,50);
		
		CreateInvItems(slf,ITAT_LURKERCLAW,6);
		CreateInvItems(slf,ITAT_LURKERSKIN,1);
		CreateInvItems(slf,ITAT_SHEEPFUR,2);
		CreateInvItems(slf,ITAT_WING,4);
		CreateInvItems(slf,ITAT_TEETH,4);
		CreateInvItems(slf,ITFO_APPLE,23);
		
		Ardo_ItemsGiven_Chapter_2 = TRUE;
	};
	if((Kapitel >= 3) && (Ardo_ItemsGiven_Chapter_3 == FALSE))
	{
		CreateInvItems(slf,ItMi_Gold,100);
		CreateInvItems(slf,ItFo_Bread,11);
		CreateInvItems(slf,ItFoMuttonRaw,7);
		CreateInvItems(slf,ItFo_Bacon,1);
		CreateInvItems(slf,ItFo_Sausage,4);
		CreateInvItems(slf,ItFo_Milk, 4);
		CreateInvItems(slf,ITMI_COAL,20);
		CreateInvItems(slf,ITMI_MOLERATLUBRIC_MIS,1);
		CreateInvItems(slf,ITPL_MUSHROOM_01,10);
		CreateInvItems(slf,ITPL_MUSHROOM_02,5);
		CreateInvItems(slf,ITPL_PLANEBERRY,19);
		CreateInvItems(slf,ITPL_SPEED_HERB_01,1);
		CreateInvItems(slf,ItRw_Arrow,50);
		
		CreateInvItems(slf,ITAT_LURKERCLAW,6);
		CreateInvItems(slf,ITAT_LURKERSKIN,1);
		CreateInvItems(slf,ITAT_SHEEPFUR,2);
		CreateInvItems(slf,ITAT_WING,4);
		CreateInvItems(slf,ITAT_TEETH,4);
		CreateInvItems(slf,ITFO_APPLE,23);

		Ardo_ItemsGiven_Chapter_3 = TRUE;
	};
	if((Kapitel >= 4) && (Ardo_ItemsGiven_Chapter_4 == FALSE))
	{
		CreateInvItems(slf,ItMi_Gold,100);
		CreateInvItems(slf,ItFo_Bread,11);
		CreateInvItems(slf,ItFoMuttonRaw,7);
		CreateInvItems(slf,ItFo_Bacon,1);
		CreateInvItems(slf,ItFo_Sausage,4);
		CreateInvItems(slf,ItFo_Milk, 4);
		CreateInvItems(slf,ITMI_COAL,20);
		CreateInvItems(slf,ITMI_MOLERATLUBRIC_MIS,1);
		CreateInvItems(slf,ITPL_MUSHROOM_01,10);
		CreateInvItems(slf,ITPL_MUSHROOM_02,5);
		CreateInvItems(slf,ITPL_PLANEBERRY,19);
		CreateInvItems(slf,ITPL_SPEED_HERB_01,1);
		CreateInvItems(slf,ItRw_Arrow,50);
		
		CreateInvItems(slf,ITAT_LURKERCLAW,6);
		CreateInvItems(slf,ITAT_LURKERSKIN,1);
		CreateInvItems(slf,ITAT_SHEEPFUR,2);
		CreateInvItems(slf,ITAT_WING,4);
		CreateInvItems(slf,ITAT_TEETH,4);
		CreateInvItems(slf,ITFO_APPLE,23);

		Ardo_ItemsGiven_Chapter_4 = TRUE;
	};
	if((Kapitel >= 5) && (Ardo_ItemsGiven_Chapter_5 == FALSE))
	{
		CreateInvItems(slf,ItMi_Gold,100);
		CreateInvItems(slf,ItFo_Bread,11);
		CreateInvItems(slf,ItFoMuttonRaw,7);
		CreateInvItems(slf,ItFo_Bacon,1);
		CreateInvItems(slf,ItFo_Sausage,4);
		CreateInvItems(slf,ItFo_Milk, 4);
		CreateInvItems(slf,ITMI_COAL,20);
		CreateInvItems(slf,ITMI_MOLERATLUBRIC_MIS,1);
		CreateInvItems(slf,ITPL_MUSHROOM_01,10);
		CreateInvItems(slf,ITPL_MUSHROOM_02,5);
		CreateInvItems(slf,ITPL_PLANEBERRY,19);
		CreateInvItems(slf,ITPL_SPEED_HERB_01,1);
		CreateInvItems(slf,ItRw_Arrow,50);
		
		CreateInvItems(slf,ITAT_LURKERCLAW,6);
		CreateInvItems(slf,ITAT_LURKERSKIN,1);
		CreateInvItems(slf,ITAT_SHEEPFUR,2);
		CreateInvItems(slf,ITAT_WING,4);
		CreateInvItems(slf,ITAT_TEETH,4);
		CreateInvItems(slf,ITFO_APPLE,23);

		Ardo_ItemsGiven_Chapter_5 = TRUE;
	};
};
