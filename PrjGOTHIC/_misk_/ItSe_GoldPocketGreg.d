
instance ItSe_GoldPocketGreg(C_Item)
{
	name = "������� �������";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = 100;
	visual = "ItMi_Pocket.3ds";
	scemeName = "MAPSEALED";
	material = MAT_LEATHER;
	description = name;
	text[2] = "������,";
	text[3] = "���� ������� ����������� �������� � �������� �� �����.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItMi_GoldCupGreg(C_Item)
{
	name = "������� �����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_GoldCup;
	visual = "ItMi_GoldCup.3DS";
	material = MAT_METAL;
	description = name;
	text[2] = "������,";
	text[3] = "���� ����� ����������� �������� � �������� �� �����.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItMi_SilverChaliceGreg(C_Item)
{
	name = "���������� ����";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_SilverChalice;
	visual = "ItMi_SilverChalice.3DS";
	material = MAT_METAL;
	description = name;
	text[2] = "������,";
	text[3] = "��� ���� ����������� �������� � �������� �� �����.";
	text[5] = NAME_Value;
	count[5] = value;
};

instance ItAm_Prot_Point_01_Greg(C_Item)
{
	name = NAME_Amulett;
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MISSION;
	value = Value_Am_ProtPoint;
	visual = "ItAm_Prot_Point_01.3ds";
	material = MAT_METAL;
	wear = WEAR_EFFECT;
	effect = "SPELLFX_ITEMGLIMMER";
	text[2] = "������,";
	text[3] = "���� ������ ����������� �������� � �������� �� �����.";
	text[5] = NAME_Value;
	count[5] = value;
	inv_zbias = INVCAM_ENTF_AMULETTE_STANDARD;
};

instance ItMi_UNITOR(C_Item)
{
	name = "������";
	mainflag = ITEM_KAT_NONE;
	flags = ITEM_MULTI;
	value = 0;
	visual = "ItMi_Focus.3DS";
	material = MAT_STONE;
	wear = WEAR_EFFECT;
	effect = "SPELLFX_MANAPOTION";
	description = name;
	text[2] = "���� ������ ��� ��� �������.";
};

