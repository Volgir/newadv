
instance PAL_207_Girion_DI(Npc_Default)
{
	name[0] = "������";
	guild = GIL_PAL;
	id = 2070;
	voice = 8;
	flags = 0;
	npctype = NPCTYPE_FRIEND;
	aivar[AIV_PARTYMEMBER] = TRUE;
	aivar[AIV_ToughGuy] = TRUE;
	aivar[AIV_ToughGuyNewsOverride] = TRUE;
	B_SetAttributesToChapter(self,4);
	fight_tactic = FAI_HUMAN_MASTER;
	EquipItem(self,ItMw_1h_Pal_Sword);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_Pony",Face_N_Lefty,BodyTex_N,itar_pal_m);
	Mdl_SetModelFatness(self,1);
	Mdl_ApplyOverlayMds(self,"Humans_Militia.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,60);
	daily_routine = Rtn_Start_2070;
};


func void Rtn_Start_2070()
{
	TA_Sit_Bench(8,0,23,0,"SHIP_DECK_18");
	TA_Sleep(23,0,8,0,"SHIP_IN_04");
};

