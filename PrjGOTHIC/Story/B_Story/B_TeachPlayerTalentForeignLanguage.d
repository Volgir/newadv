
func int B_TeachPlayerTalentForeignLanguage(var C_Npc slf,var C_Npc oth,var int Language)
{
	var int kosten;
	var int money;
	kosten = B_GetLearnCostTalent(oth,NPC_TALENT_FOREIGNLANGUAGE,Language);
	money = kosten * 100;
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLearnPoints,-1,-1,FONT_ScreenSmall,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	Log_CreateTopic(TOPIC_Language,LOG_NOTE);
	if(Language == LANGUAGE_1)
	{
		Npc_SetTalentSkill(oth,NPC_TALENT_FOREIGNLANGUAGE,1);
		player_talent_foreignlanguage[LANGUAGE_1] = TRUE;
		B_LogEntry(TOPIC_Language,LogText_Addon_Language_1);
		PrintScreen("�������: ���� ��������",-1,-1,FONT_Screen,2);
	};
	if(Language == LANGUAGE_2)
	{
		Npc_SetTalentSkill(oth,NPC_TALENT_FOREIGNLANGUAGE,2);
		player_talent_foreignlanguage[LANGUAGE_2] = TRUE;
		B_LogEntry(TOPIC_Language,LogText_Addon_Language_2);
		PrintScreen("�������: ���� ������",-1,-1,FONT_Screen,2);
	};
	if(Language == LANGUAGE_3)
	{
		Npc_SetTalentSkill(oth,NPC_TALENT_FOREIGNLANGUAGE,3);
		player_talent_foreignlanguage[LANGUAGE_3] = TRUE;
		B_LogEntry(TOPIC_Language,LogText_Addon_Language_3);
		PrintScreen("�������: ���� ������",-1,-1,FONT_Screen,2);
	};
	return TRUE;
};

