
instance DIA_Kardif_EXIT(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 999;
	condition = DIA_Kardif_EXIT_Condition;
	information = DIA_Kardif_EXIT_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};


func int DIA_Kardif_EXIT_Condition()
{
	if(Kardif_OneQuestion == FALSE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_EXIT_Info()
{
	AI_StopProcessInfos(self);
};


instance DIA_Kardif_PICKPOCKET(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 900;
	condition = DIA_Kardif_PICKPOCKET_Condition;
	information = DIA_Kardif_PICKPOCKET_Info;
	permanent = TRUE;
	description = Pickpocket_60;
};


func int DIA_Kardif_PICKPOCKET_Condition()
{
	return C_Beklauen(55,85);
};

func void DIA_Kardif_PICKPOCKET_Info()
{
	Info_ClearChoices(DIA_Kardif_PICKPOCKET);
	Info_AddChoice(DIA_Kardif_PICKPOCKET,Dialog_Back,DIA_Kardif_PICKPOCKET_BACK);
	Info_AddChoice(DIA_Kardif_PICKPOCKET,DIALOG_PICKPOCKET,DIA_Kardif_PICKPOCKET_DoIt);
};

func void DIA_Kardif_PICKPOCKET_DoIt()
{
	B_Beklauen();
	Info_ClearChoices(DIA_Kardif_PICKPOCKET);
};

func void DIA_Kardif_PICKPOCKET_BACK()
{
	Info_ClearChoices(DIA_Kardif_PICKPOCKET);
};


instance DIA_Kardif_Hi(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_Hi_Condition;
	information = DIA_Kardif_Hi_Info;
	permanent = FALSE;
	description = "��� ����?";
};


func int DIA_Kardif_Hi_Condition()
{
	if(Kardif_OneQuestion == FALSE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_Hi_Info()
{
	AI_Output(other,self,"DIA_Kardif_Hi_15_00");	//��� ����?
	AI_Output(self,other,"DIA_Kardif_Hi_14_01");	//���� �� ������ ���-������ ������, ���������.
	Log_CreateTopic(TOPIC_CityTrader,LOG_NOTE);
	B_LogEntry(TOPIC_CityTrader,"������ ������� ��������� � ������� � ������.");
};


instance DIA_Kardif_Hallo(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_Hallo_Condition;
	information = DIA_Kardif_Hallo_Info;
	permanent = FALSE;
	description = "����� �������� �� ������, �� ������ ����� �������...";
};


func int DIA_Kardif_Hallo_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Kardif_Hi) && (Kardif_Deal == 0))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Hallo_Info()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_15_00");	//����� �������� �� ������, �� ������ ����� �������...
	AI_Output(self,other,"DIA_Kardif_Hallo_14_01");	//� ���� ����� �� ����� ����?
	AI_Output(other,self,"DIA_Kardif_Hallo_15_02");	//��� ��� ������ ������� ��� ��������.
	AI_Output(self,other,"DIA_Kardif_Hallo_14_03");	//� ������� � ���� ���� ������?
	AI_Output(self,other,"DIA_Kardif_Hallo_14_04");	//������ ����������, ������� � ������ ����, ����� ������ 10 ������� �����.
	Log_CreateTopic(TOPIC_CityTrader,LOG_NOTE);
	B_LogEntry(TOPIC_CityTrader,"������, �������� ������ � ������, �������������� �����������.");
	Info_ClearChoices(DIA_Kardif_Hallo);
	Info_AddChoice(DIA_Kardif_Hallo,"� �� ����� ������� �� ���������� ������ 5 �����.",DIA_Kardif_Hallo_Angebot);
	Info_AddChoice(DIA_Kardif_Hallo,"������ �� ���� - � ����� ������ ��� ���������� � ������ �����.",DIA_Kardif_Hallo_Hart);
	Info_AddChoice(DIA_Kardif_Hallo,"������, ������������.",DIA_Kardif_Hallo_Zehn);
};

func void DIA_Kardif_Hallo_Zehn()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Zehn_15_00");	//������, ������������.
	AI_Output(self,other,"DIA_Kardif_Hallo_Zehn_14_01");	//�� �������� �������� ������ (����������). � ������ � ����� �������.
	Kardif_Deal = 10;
	Info_ClearChoices(DIA_Kardif_Hallo);
};

func void DIA_Kardif_Hallo_Angebot()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Angebot_15_00");	//� �� ����� ������� �� ���������� ������ 5 �����.
	AI_Output(self,other,"DIA_Kardif_Hallo_Angebot_14_01");	//���, 5 ������� �����? �� ������ �������� ����? (������ ���������� ���) - ����� �������� �� 7.
	Info_ClearChoices(DIA_Kardif_Hallo);
	Info_AddChoice(DIA_Kardif_Hallo,"���, ��� �� ������. ����� ����� 6!",DIA_Kardif_Hallo_KeinDeal);
	Info_AddChoice(DIA_Kardif_Hallo,"������������, 7 ������� ����� - �������� ����.",DIA_Kardif_Hallo_Sieben);
};

func void DIA_Kardif_Hallo_Hart()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Hart_15_00");	//������ �� ���� - � ����� ������ ��� ���������� � ������ �����.
	AI_Output(self,other,"DIA_Kardif_Hallo_Hart_14_01");	//������, ������... � �������� � �� 7.
	Info_ClearChoices(DIA_Kardif_Hallo);
	Info_AddChoice(DIA_Kardif_Hallo,"������������, 7 ������� ����� - �������� ����.",DIA_Kardif_Hallo_Sieben);
	Info_AddChoice(DIA_Kardif_Hallo,"���, ��� �� ��� �� ������ ����� ���� ����������.",DIA_Kardif_Hallo_Ablehnen);
};

func void DIA_Kardif_Hallo_Sieben()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Sieben_15_00");	//������������, 7 ������� ����� - �������� ����.
	AI_Output(self,other,"DIA_Kardif_Hallo_Sieben_14_01");	//(����������) ������ �������. ��, ���� ���� ����� ����� ���-�� ������, ��������� �� ���.
	Kardif_Deal = 7;
	Info_ClearChoices(DIA_Kardif_Hallo);
};

func void DIA_Kardif_Hallo_Ablehnen()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Ablehnen_15_00");	//���, ��� �� ��� �� ������ ����� ���� ����������.
	AI_Output(self,other,"DIA_Kardif_Hallo_Ablehnen_14_01");	//��, �������... (��������) - ������, 5 ������� �����. �� ��� ��� ��������� ����!
	Info_ClearChoices(DIA_Kardif_Hallo);
	Info_AddChoice(DIA_Kardif_Hallo,"� ������ ������ ���� ����� ������� �� ����?",DIA_Kardif_Hallo_Fuenf);
};

func void DIA_Kardif_Hallo_Fuenf()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_Fuenf_15_00");	//� ������ ������ ���� ����� ������� �� ����?
	AI_Output(self,other,"DIA_Kardif_Hallo_Fuenf_14_01");	//��, � ������ ������ �� � ������ �����. ��, ��� �� �� �� ���� - ������ ���� ������.
	Kardif_Deal = 5;
	Info_ClearChoices(DIA_Kardif_Hallo);
};

func void DIA_Kardif_Hallo_KeinDeal()
{
	AI_Output(other,self,"DIA_Kardif_Hallo_KeinDeal_15_00");	//���, ��� �� ������. ����� ����� 6!
	AI_Output(self,other,"DIA_Kardif_Hallo_KeinDeal_14_01");	//�� ����������� �������, ����� � ����. ��, ���� �� ��� �����������, ���������� ���� ��������� � 6 ������� �����.
	Kardif_Deal = 6;
	Info_ClearChoices(DIA_Kardif_Hallo);
};


instance DIA_Kardif_TRADE(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_TRADE_Condition;
	information = DIA_Kardif_TRADE_Info;
	permanent = TRUE;
	trade = TRUE;
	description = "��� ��� ���-������ ������.";
};


func int DIA_Kardif_TRADE_Condition()
{
	if(Kardif_OneQuestion == FALSE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_TRADE_Info()
{
	B_GiveTradeInv(self);
	AI_Output(other,self,"DIA_Kardif_TRADE_15_00");	//��� ��� ���-������ ������.
};


instance DIA_Kardif_TradeInfo(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_TradeInfo_Condition;
	information = DIA_Kardif_TradeInfo_Info;
	permanent = TRUE;
	description = "��� ����� ����������.";
};


func int DIA_Kardif_TradeInfo_Condition()
{
	if((Kardif_OneQuestion == FALSE) && (Kardif_Deal > 0))
	{
		return TRUE;
	};
};

func void DIA_Kardif_TradeInfo_Info()
{
	AI_Output(other,self,"DIA_Kardif_TradeInfo_15_00");	//��� ����� ����������.
	Kardif_OneQuestion = TRUE;
};

func void B_SayKardifZuwenigGold()
{
	AI_Output(self,other,"B_SayKardifZuwenigGold_14_00");	//�����������, ����� � ���� ����� ���������� ������.
};


instance DIA_Kardif_Buerger(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Buerger_Condition;
	information = DIA_Kardif_Buerger_Info;
	permanent = TRUE;
	description = "��� ��������� ������������ ������ � ���� ������?";
};


var int DIA_Kardif_Buerger_permanent;

func int DIA_Kardif_Buerger_Condition()
{
	if((DIA_Kardif_Buerger_permanent == FALSE) && (Kardif_OneQuestion == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Buerger_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Buerger_15_00");	//��� ��������� ������������ ������ � ���� ������?
		AI_Output(self,other,"DIA_Kardif_Buerger_14_01");	//�����, � �����, �� ������ �� ��� �� �������. ������������ ����������� ������� ����� - ��� �����.
		AI_Output(self,other,"DIA_Kardif_Buerger_14_02");	//���� ��������� �� ����� ���������, �� � ���� ���� ������, � ������ ����������� �������� ������ ��� ������.
		AI_Output(self,other,"DIA_Kardif_Buerger_14_03");	//�������� � ������� ���� ����� ����������� ���� - � ���� �������, ���� ���� ��������� ��� ������.
		DIA_Kardif_Buerger_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Lehmar(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Lehmar_Condition;
	information = DIA_Kardif_Lehmar_Info;
	permanent = TRUE;
	description = "��� ������ ������?";
};


var int DIA_Kardif_Lehmar_permanent;

func int DIA_Kardif_Lehmar_Condition()
{
	if((DIA_Kardif_Lehmar_permanent == FALSE) && (Kardif_OneQuestion == TRUE) && (DIA_Kardif_Buerger_permanent == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Lehmar_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Add_15_00");	//��� ������ ������?
		AI_Output(self,other,"DIA_Kardif_Add_14_01");	//(�������) ���� �� ������ ������ ���, ���� ����� ��������� � ��� ��������.
		AI_Output(self,other,"DIA_Kardif_Add_14_02");	//������ ��� ����� ����� �������� ������� ���������.
		AI_Output(self,other,"DIA_Kardif_Add_14_03");	//��������� ��� ��������, �� ������ ����� ��� � �����...
		DIA_Kardif_Lehmar_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Arbeit(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Arbeit_Condition;
	information = DIA_Kardif_Arbeit_Info;
	permanent = TRUE;
	description = "��� � ���� ����� ������?";
};


var int DIA_Kardif_Arbeit_permanent;

func int DIA_Kardif_Arbeit_Condition()
{
	if((DIA_Kardif_Arbeit_permanent == FALSE) && (Kardif_OneQuestion == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Arbeit_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Arbeit_15_00");	//��� � ���� ����� ������?
		AI_Output(self,other,"DIA_Kardif_Arbeit_14_01");	//�� ���� �� ������� ������ �����, � �����. ���� ����� ���������� � �������� � ������ ����� ������.
		AI_Output(self,other,"DIA_Kardif_Arbeit_14_02");	//�� ���� � ���� ���� ��������� ���, �� ������ ������� ������� �� ��������. �� ������� ��� �� �������, �� ��������� �� ������.
		DIA_Kardif_Arbeit_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Addon_Kardif_MissingPeople(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Addon_Kardif_MissingPeople_Condition;
	information = DIA_Addon_Kardif_MissingPeople_Info;
	permanent = TRUE;
	description = "��� �� ������ � ��������� ���������?";
};


var int DIA_Addon_Kardif_MissingPeople_permanent;

func int DIA_Addon_Kardif_MissingPeople_Condition()
{
	if((DIA_Addon_Kardif_MissingPeople_permanent == FALSE) && (Kardif_OneQuestion == TRUE) && (SC_HearedAboutMissingPeople == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Addon_Kardif_MissingPeople_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Addon_Kardif_MissingPeople_15_00");	//��� �� ������ � ��������� ���������?
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_01");	//���, ��� � ����, - ��� ��, ��� �� ��������� ��� ������� ��������� �����.
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_02");	//�������, ��� ������ ����� ��������� �����, � �����. �����, ���� ����� ����������� �������.
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_03");	//� ������ ����� ������ ���� �������� ����.
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_04");	//���� ������ ����� ������, �������� � ���������.
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_05");	//� ���� ������ � ������ ����� ������, � �����, ��� �� ��� ��� �� ���������� ����� �������.
		AI_Output(self,other,"DIA_Addon_Kardif_MissingPeople_14_06");	//������, �������� ����� �� ����� �� ��������, ���� ����� ���-�� ����� - � ���� ������� ����� �����.
		Log_CreateTopic(TOPIC_Addon_WhoStolePeople,LOG_MISSION);
		Log_SetTopicStatus(TOPIC_Addon_WhoStolePeople,LOG_Running);
		B_LogEntry(TOPIC_Addon_WhoStolePeople,"������ �������, ��� ��� ����� ���������� � ��������� ����� � ���������, ���������� �������� � ������ ����� ������, � ����� � ��������, ��������� ����� � ������.");
		DIA_Addon_Kardif_MissingPeople_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Lernen(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Lernen_Condition;
	information = DIA_Kardif_Lernen_Info;
	permanent = TRUE;
	description = "� � ���� ����� ����� ��������� ����-������?";
};


var int DIA_Kardif_Lernen_permanent;

func int DIA_Kardif_Lernen_Condition()
{
	if((DIA_Kardif_Lernen_permanent == FALSE) && (Kardif_OneQuestion == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Lernen_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Lernen_15_00");	//� � ���� ����� ����� ��������� ����-������?
		AI_Output(self,other,"DIA_Kardif_Lernen_14_01");	//�����, � �������� �������� ���� ��������� ������� �����.
		AI_Output(self,other,"DIA_Kardif_Lernen_14_02");	//����, ������, - ������� ������. �������, �� ������ ������ ���� ����� �������.
		AI_Output(self,other,"DIA_Kardif_Lernen_14_03");	//������ ������ � ������ ����������. � ����� - ��������, ����� ���� �� �������. �� ���� ������� ������ � ����.
		AI_Output(self,other,"DIA_Kardif_Lernen_14_04");	//� ���� �� ��������� ����� �� ���, ����� ����� � ������� ������ - �� ����� ���� � �����... �����... ������.
		AI_Output(other,self,"DIA_Kardif_Lernen_15_05");	//� ��� ��� ����� ���� ���� �����?
		AI_Output(self,other,"DIA_Kardif_Lernen_14_06");	//(������) ����, ������ ������ �������� ����� ������ �����, ������ ��� � ���� ������� ��� ���.
		AI_Output(self,other,"DIA_Kardif_Lernen_14_07");	//������ ��������� � �������� ��������, �� ����������� ������� ��. � ���� �� ������ ����� ��������������� �����, ����� � �������� � ���� � ���� �����.
		AI_Output(self,other,"DIA_Kardif_Lernen_14_08");	//��� ������ ����� �����. �� ����� � �������� ���� ������� �� ����� ��������. (��������) � ������ ��� ����� � ��� ���� ������ �� ��� ����������.
		Log_CreateTopic(TOPIC_CityTeacher,LOG_NOTE);
		B_LogEntry(TOPIC_CityTeacher,"����, ������ � �������� ��������, ����� ������� ���� �������.");
		B_LogEntry(TOPIC_CityTeacher,"����� ����� ������ ��� �������� ��� ��������.");
		B_LogEntry(TOPIC_CityTeacher,"������ ����� ������� ���� ��������� �������� ���������� �������. �� ��������� �� ������� � �������� ��������.");
		B_LogEntry(TOPIC_CityTeacher,"����� ����� �������� ��� ������� ������������� �����. �� ����� � �������� ��������.");
		Log_CreateTopic(TOPIC_CityTrader,LOG_NOTE);
		B_LogEntry(TOPIC_CityTrader,"������� ������ ����� � ������� �� � ������.");
		DIA_Kardif_Lernen_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Diebeswerk(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Diebeswerk_Condition;
	information = DIA_Kardif_Diebeswerk_Info;
	permanent = TRUE;
	description = "� ��� �� ����� �����-������ '������' ������ ...?";
};


var int DIA_Kardif_Diebeswerk_permanent;

func int DIA_Kardif_Diebeswerk_Condition()
{
	if((DIA_Kardif_Diebeswerk_permanent == FALSE) && (DIA_Kardif_Arbeit_permanent == TRUE) && (Kardif_OneQuestion == TRUE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Diebeswerk_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Diebeswerk_15_00");	//� ��� �� ����� �����-������ '������' ������, ��� �� �������� ��������� ���������� �����?
		AI_Output(self,other,"DIA_Kardif_Diebeswerk_14_01");	//����-������ �������, ��������? ���...
		AI_PlayAni(self,"T_SEARCH");
		AI_Output(self,other,"DIA_Kardif_Diebeswerk_14_02");	//...�������� ���������� � �������. ��������, �� ������ ������ ����.
		DIA_Kardif_Diebeswerk_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Diebeswerk2(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Diebeswerk2_Condition;
	information = DIA_Kardif_Diebeswerk2_Info;
	permanent = TRUE;
	description = "���� ���-������ '���������' ��� ����?";
};


var int DIA_Kardif_Diebeswerk2_permanent;

func int DIA_Kardif_Diebeswerk2_Condition()
{
	if((DIA_Kardif_Diebeswerk2_permanent == FALSE) && (DIA_Kardif_Diebeswerk_permanent == TRUE) && (DIA_Kardif_Arbeit_permanent == TRUE) && (Kardif_OneQuestion == TRUE) && (other.guild != GIL_KDF) && (other.guild != GIL_NOV) && (other.guild != GIL_MIL) && (other.guild != GIL_PAL))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Diebeswerk2_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Diebeswerk2_15_00");	//���� ���-������ '���������' ��� ����?
		AI_Output(self,other,"DIA_Kardif_Diebeswerk2_14_01");	//��, ���� ���-��� - �� ��� ������������ ����, ������ ���� �� ������� ��������������� ���������.
		AI_Output(other,self,"DIA_Kardif_Diebeswerk2_15_02");	//����������, ��� ��� � ����?
		AI_Output(self,other,"DIA_Kardif_Diebeswerk2_14_03");	//��, � ������, �������� ������� �� �����, ������ ������ �����, ��� ����.
		AI_Output(other,self,"DIA_Kardif_Diebeswerk2_15_04");	//�?
		AI_Output(self,other,"DIA_Kardif_Diebeswerk2_14_05");	//��� ��� ���� ����� ��������, ��������� ���������� ��� ���� �������� ���������.
		AI_Output(self,other,"DIA_Kardif_Diebeswerk2_14_06");	//�������, ��� ���� ����� ����� � ����� ��������� ���������. �� �� ������ �� ������ �� ����, �������?
		DIA_Kardif_Diebeswerk2_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_Zurueck(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_Zurueck_Condition;
	information = DIA_Kardif_Zurueck_Info;
	permanent = TRUE;
	description = Dialog_Back;
};


func int DIA_Kardif_Zurueck_Condition()
{
	if(Kardif_OneQuestion == TRUE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_Zurueck_Info()
{
	Kardif_OneQuestion = FALSE;
	Info_ClearChoices(DIA_Kardif_Zurueck);
};


instance DIA_Kardif_DOPE(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 5;
	condition = DIA_Kardif_DOPE_Condition;
	information = DIA_Kardif_DOPE_Info;
	permanent = TRUE;
	description = "��� ��� ����� ������ ������?";
};


var int DIA_Kardif_DOPE_perm;

func int DIA_Kardif_DOPE_Condition()
{
	if((MIS_Andre_REDLIGHT == LOG_Running) && (Kardif_OneQuestion == TRUE) && (DIA_Kardif_DOPE_perm == FALSE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_DOPE_Info()
{
	var C_Item heroArmor;
	heroArmor = Npc_GetEquippedArmor(other);
	AI_Output(other,self,"DIA_Kardif_DOPE_15_01");	//��� ��� ����� ������ ������?
	if(Hlp_IsItem(heroArmor,ITAR_Mil_L) == TRUE)
	{
		AI_Output(self,other,"DIA_Kardif_DOPE_14_00");	//������ �� ����� - � �� ���� ���� � ��������� ������.
	}
	else
	{
		AI_Output(self,other,"DIA_Kardif_DOPE_14_02");	//������ �� �����.
		AI_Output(other,self,"DIA_Kardif_DOPE_15_03");	//�����, ����� ���?
		AI_Output(self,other,"DIA_Kardif_DOPE_14_04");	//� �� �� ����� ����� ��������� � �������� - �� ����� ��� ����� ��������.
		DIA_Kardif_DOPE_perm = TRUE;
	};
};


instance DIA_Kardif_Paket(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 3;
	condition = DIA_Kardif_Paket_Condition;
	information = DIA_Kardif_Paket_Info;
	permanent = TRUE;
	description = "�� ���-������ ������ � ���� �������� �����?";
};


var int DIA_Kardif_Paket_perm;

func int DIA_Kardif_Paket_Condition()
{
	if((MIS_Andre_WAREHOUSE == LOG_Running) && (Kardif_OneQuestion == TRUE) && (DIA_Kardif_Paket_perm == FALSE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Paket_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Paket_15_00");	//�� ���-������ ������ � ���� �������� �����?
		AI_Output(self,other,"DIA_Kardif_Paket_14_01");	//���, ��� ������� ���� ����� - ������� �������. �, ��������, ������ ������������� �����!
		AI_Output(other,self,"DIA_Kardif_Paket_15_02");	//�� ������� �������� ����. �� ���-�� ������. �����, ����������!
		AI_Output(self,other,"DIA_Kardif_Paket_14_03");	//������, ������ - ���� ������ ������� ������� � ��� �������. �� �� ���������.
		AI_Output(self,other,"DIA_Kardif_Paket_14_04");	//�� ������, ��� ������ ��� �������� ����� � ������, ��, ������� ��, �� ��� ���������� � ����. ��� ���, ��� � ����.
		DIA_Kardif_Paket_perm = TRUE;
		B_LogEntry(TOPIC_Warehouse,"������ ������� � ���������. � ����� ����� ���� ��� �������� �����, ������� �� ����� �������.");
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_SENDATTILA(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_SENDATTILA_Condition;
	information = DIA_Kardif_SENDATTILA_Info;
	important = TRUE;
};


func int DIA_Kardif_SENDATTILA_Condition()
{
	if((MIS_ThiefGuild_sucked == TRUE) || ((Diebesgilde_Okay >= 3) && Npc_IsInState(self,ZS_Talk)))
	{
		return TRUE;
	};
};

func void DIA_Kardif_SENDATTILA_Info()
{
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_00");	//��, ��, ��� ����. � ���� ���-��� ���� ��� ����.
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_01");	//���� ������ ����� ����� ���������� � �����.
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_02");	//��� ��� �� �� ����� ���� �����, �� �������� ���� �������� ���� �������.
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_03");	//�� ����� ����������� � �����. �� ������ ������ �������.
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_04");	//��� ���������� ��������� - �� ��� ����������! ���� �� ��� ��������� �������� �������.
	AI_Output(other,self,"DIA_Kardif_SENDATTILA_15_05");	//��� �������� ���� ������?
	AI_Output(self,other,"DIA_Kardif_SENDATTILA_14_06");	//� ��� �� ������� ���� - �� ���� �������� ��������� �� ���, ��� ����. (����������)
	if(Kardif_Deal == 0)
	{
		AI_Output(self,other,"DIA_Kardif_Hallo_14_04");	//������ ����������, ������� � ������ ����, ����� ������ 10 ������� �����.
		Kardif_Deal = 10;
	};
	Wld_InsertNpc(VLK_494_Attila,"NW_CITY_ENTRANCE_01");
};


instance DIA_Kardif_Kerl(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_Kerl_Condition;
	information = DIA_Kardif_Kerl_Info;
	permanent = TRUE;
	description = "��� �������� ���� ������?";
};


var int DIA_Kardif_Kerl_permanent;

func int DIA_Kardif_Kerl_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Kardif_SENDATTILA) && (Attila.aivar[AIV_TalkedToPlayer] == FALSE) && (Kardif_OneQuestion == TRUE) && (DIA_Kardif_Kerl_permanent == FALSE))
	{
		return TRUE;
	};
};

func void DIA_Kardif_Kerl_Info()
{
	if(B_GiveInvItems(other,self,ItMi_Gold,Kardif_Deal))
	{
		AI_Output(other,self,"DIA_Kardif_Kerl_15_00");	//��� �������� ���� ������?
		AI_Output(self,other,"DIA_Kardif_Kerl_14_01");	//��, �� �������� �������, ���������� � ������� - �� �� �� ����� ��������. �����-�� ��... ��������.
		AI_Output(other,self,"DIA_Kardif_Kerl_15_02");	//� ��� ����?
		AI_Output(self,other,"DIA_Kardif_Kerl_14_03");	//��� ����? ����� �� ������ �� ����, � ��� ���, ��� �� ������ �� �� ����.
		AI_Output(self,other,"DIA_Kardif_Kerl_14_04");	//� ��� ������ ���� ���-�� �������� - ��, ��� �� �� �� ����, � �����, �� ������ �����, ���������� � ���. ��� ������ ���� ���������.
		AI_Output(other,self,"DIA_Kardif_Kerl_15_05");	//��... ���� ������ � ���, ��� ����...
		DIA_Kardif_Kerl_permanent = TRUE;
	}
	else
	{
		B_SayKardifZuwenigGold();
	};
};


instance DIA_Kardif_DEFEATEDATTILA(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_DEFEATEDATTILA_Condition;
	information = DIA_Kardif_DEFEATEDATTILA_Info;
	permanent = FALSE;
	description = "���� ������� ������� ����� ����!";
};


func int DIA_Kardif_DEFEATEDATTILA_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Attila_Hallo))
	{
		return TRUE;
	};
};

func void DIA_Kardif_DEFEATEDATTILA_Info()
{
	AI_Output(other,self,"DIA_Kardif_DEFEATEDATTILA_15_00");	//���� ������� ������� ����� ����!
	AI_Output(self,other,"DIA_Kardif_DEFEATEDATTILA_14_01");	//��, ������ ��� ���� �����? � ������ ������� ����������.
	AI_Output(self,other,"DIA_Kardif_DEFEATEDATTILA_14_02");	//���� ���-�� ����� �������� ����, � �����, � ���� ���� ������ �������.
	B_GivePlayerXP(XP_Kardif_Blame4Attila);
	B_KillNpc(Attila);
	Npc_RemoveInvItem(Attila,ItMi_OldCoin);
};


instance DIA_Kardif_Zeichen(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 2;
	condition = DIA_Kardif_Zeichen_Condition;
	information = DIA_Kardif_Zeichen_Info;
	permanent = FALSE;
	description = "(�������� ������ �����)";
};


func int DIA_Kardif_Zeichen_Condition()
{
	if(Knows_SecretSign == TRUE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_Zeichen_Info()
{
	AI_PlayAni(other,"T_YES");
	AI_Output(self,other,"DIA_Kardif_Zeichen_14_00");	//��, �� ������ ���� � ������. ������. � ����� ������, � ���� ���-��� ���� ��� ����.
	AI_Output(self,other,"DIA_Kardif_Zeichen_14_01");	//(���������) ���� ���� ����������� �������, �������. � ������ ��������� �� ������ ������. ������ ������� ���� ������ ���� �������.
	CreateInvItems(self,ItKe_Lockpick,20);
};


instance DIA_Kardif_Crew(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 51;
	condition = DIA_Kardif_Crew_Condition;
	information = DIA_Kardif_Crew_Info;
	permanent = FALSE;
	description = "��� ��� ��� ����� �������.";
};


func int DIA_Kardif_Crew_Condition()
{
	if(MIS_SCKnowsWayToIrdorath == TRUE)
	{
		return TRUE;
	};
};

func void DIA_Kardif_Crew_Info()
{
	AI_Output(other,self,"DIA_Kardif_Crew_15_00");	//��� ��� ��� ����� �������.
	AI_Output(self,other,"DIA_Kardif_Crew_14_01");	//������� ������ �������, �������. �� �� ������� �������� �� ���� ��������. ����������� �� ��� ����� �������� �����.
	AI_Output(self,other,"DIA_Kardif_Crew_14_02");	//�� � ������� ��������� ����� �������� ������� ��������� ���������� ������, � �������� ������ ������� �� ��� �������� �������.
	AI_Output(other,self,"DIA_Kardif_Crew_15_03");	//��� � ���� ����� ��������?
	AI_Output(self,other,"DIA_Kardif_Crew_14_04");	//�� ����������� �� �� ������. � ������� ����������.
	if(Npc_IsDead(Jack) == FALSE)
	{
		if(SCGotCaptain == FALSE)
		{
			Log_CreateTopic(Topic_Captain,LOG_MISSION);
			Log_SetTopicStatus(Topic_Captain,LOG_Running);
			B_LogEntry(Topic_Captain,"������ �������� ���� � ������� �����. ��������, �� ������ ������ ���");
		};
		AI_Output(self,other,"DIA_Kardif_Crew_14_05");	//���, �������� �� ������ ������. �� ��������� � ���� �����, ������� � ���� �����. � ���, ��� �������� �������� ����, ��� ��� �������, ��� ���� �����.
	};
};


var int rabota;
var int MisMoe;

instance DIA_KARDIF_GERALT(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 4;
	condition = dia_kardif_geralt_condition;
	information = dia_kardif_geralt_info;
	permanent = FALSE;
	description = "� ��� ��� ������ �������� �� ��������?";
};


func int dia_kardif_geralt_condition()
{
	return TRUE;
};

func void dia_kardif_geralt_info()
{
	AI_Output(other,self,"DIA_Kardif_Geralt1");	//� ��� ��� ������ �������� �� ��������?
	AI_Output(self,other,"DIA_Kardif_Geralt2");	//������ ��������...���� ���������� ���� ���������� ���������.
	AI_Output(self,other,"DIA_Kardif_Geralt3");	//������ ���, �� �� ����� �� �������� �����, � ���, ��� �� ���������, ����� ������ ������� �����!
	AI_Output(other,self,"DIA_Kardif_Geralt4");	//����� ����, � ����� ���� ���-������ ����������?
	AI_Output(self,other,"DIA_Kardif_Geralt5");	//��������...���-�� ��������.
	AI_PlayAni(self,"R_SCRATCHHEAD");
	AI_Output(self,other,"DIA_Kardif_Geralt6");	//�����-��, � �� ����� �����, ��� ����� � ����� � �������?
	AI_Output(other,self,"DIA_Kardif_Geralt7");	//��, � ��� �����. ��� �� �� �����?
	AI_Output(self,other,"DIA_Kardif_Geralt8");	//�����...� ��, ������, ���� � ���� ��������. ������ ���, ��-�� ���� �� ��� ��������� ������ �������...
	AI_Output(other,self,"DIA_Kardif_Geralt9");	//������?
	AI_Output(self,other,"DIA_Kardif_Geralt10");	//���� ������� �������� ������ �� ���� ��������, ��� �������� ���� �������. ������ �������, � ����� ��������...
	AI_Output(self,other,"DIA_Kardif_Geralt11");	//������� � � �������, ��� ���� ���� ������� ������� ���, ����� �� �������� �������� ������ �� ����� - �� � ������ ���� � ���� �� ������.
	AI_Output(self,other,"DIA_Kardif_Geralt12");	//��, � ���� �� ��������� - ������, �� ��� ����� ��������� �� �����.
	mis_moebored = LOG_Running;
	Log_CreateTopic(TOPIC_MOEBORED,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_MOEBORED,LOG_Running);
	if(moeisbeaten == TRUE)
	{
		B_GivePlayerXP(150);
		AI_Output(other,self,"DIA_Kardif_Geralt13");	//� ��� ���� � ��� ����, ��� ���������, �� �� ��� �� � �����...�������� ������ ��������� ��� ��������� �������� ������.
		AI_Output(self,other,"DIA_Kardif_Geralt14");	//��?! ��� �, ����� ������� �� ����.
		AI_Output(self,other,"DIA_Kardif_Geralt15");	//����� ��������� - ������� �� ���. ������� ������� ����� ������.
		B_LogEntry(TOPIC_MOEBORED,"���� � ���� ���������� �� ������ � �������, ��� ����� ��������� �� ��������� �������� ������ �� �����. ���� ������ ���� ���� ������ � ����� � �������. ������, � ��� ���������� � ��� � ����� ��� ����, ����� �� ������� �������� ���� ���� � �� ����. ������� �����, ��� �� ���� ��� �������� ������ ����� ���������� � ��� � ���������� ������� ��� ��������� ���������� ���������������.");
	}
	else
	{
		AI_Output(other,self,"DIA_Kardif_Geralt16");	//�����, �������� ���-������ ������� ��� ��������!
		AI_Output(self,other,"DIA_Kardif_Geralt17");	//��������-��������...(����������).
		AI_Output(self,other,"DIA_Kardif_Geralt18");	//���� ��������� - ������� �� ���. ������� ������� ����� ������.
		B_LogEntry(TOPIC_MOEBORED,"���� � ���� ���������� �� ������ � �������, ��� ����� ��������� �� ��������� �������� ������ �� �����. ���� ������ ���� ���� ������ � ����� � �������.");
	};
};


instance DIA_KARDIF_Moe(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 14;
	condition = dia_kardif_moe_condition;
	information = dia_kardif_moe_info;
	permanent = FALSE;
	description = "������ ��...";
};


func int dia_kardif_moe_condition()
{
	if((mis_moebored == LOG_Running) && ((moebeatme == TRUE) || (moedont == TRUE)))
	{
		return TRUE;
	};
};

func void dia_kardif_moe_info()
{
	AI_Output(other,self,"DIA_Kardif_Moe1");	//������ ��...
	if(moedont == TRUE)
	{
		B_GivePlayerXP(200);
		AI_Output(self,other,"DIA_Kardif_Moe2");	//� ������.
		AI_Output(other,self,"DIA_Kardif_Moe3");	//�� ������ �� ����� �������� ������ �� �����.
		AI_Output(self,other,"DIA_Kardif_Moe4");	//��� �������� �������! ������ ������, � �� ������, ��� ���� ��� �������.
		AI_Output(other,self,"DIA_Kardif_Moe5");	//� �������� ���� �������, ������ �� �������� ���� � ���� �� ������?
		AI_Output(self,other,"DIA_Kardif_Moe6");	//�������, ��� �� �������� ����� ������, ��� ��. ��, � ������ �� ��������� ������� ����� ������.
		AI_Output(self,other,"DIA_Kardif_Moe7");	//����, � ���� � ���� ������� ����, ������ ���, ���������� ������� �����. ����� ����������?
		AI_Output(other,self,"DIA_Kardif_Moe8");	//������!
		AI_Output(self,other,"DIA_Kardif_Moe9");	//������, �� ������������?!
		AI_Output(other,self,"DIA_Kardif_Moe10");	//������������!
		AI_Output(self,other,"DIA_Kardif_Moe11");	//�������! ���, ������, ������ ���� ���� �� �������. �� ������� ���������.
		B_GiveInvItems(self,other,ItMi_Gold,15);
		AI_Output(self,other,"DIA_Kardif_Moe12");	//������ � ������� ������, ��������, � ���� �������� ������� ��������� ��� ����.
		mis_moebored = LOG_SUCCESS;
		Log_SetTopicStatus(TOPIC_MOEBORED,LOG_SUCCESS);
		B_LogEntry(TOPIC_MOEBORED,"������ ���� ���� � ���� �� ������. ������ ���� � ���� �������� �� ���� �� 15 ������� �����! ����� ��� ����� ������ �������� � �������, ��������, � ������� ����� ������� ������� ��� ����.");
		AI_StopProcessInfos(self);
	}
	else if(moebeatme == TRUE)
	{
		AI_Output(self,other,"DIA_Kardif_Moe13");	//� ������, ��� ���� �� ���� ������� ��� ���������...
		AI_Output(self,other,"DIA_Kardif_Moe14");	//���� ����� � ��������� �������.
		AI_Output(other,self,"DIA_Kardif_Moe15");	//������, � �� ����� � ���� ��������?
		AI_Output(self,other,"DIA_Kardif_Moe16");	//�����! ������, �� �� ��� �� �����...
		mis_moebored = LOG_FAILED;
		Log_SetTopicStatus(TOPIC_MOEBORED,LOG_FAILED);
		B_LogEntry(TOPIC_MOEBORED,"������ �� ���� ���� � ���� �� ������.");
	};
};


instance DIA_KARDIF_PERMJOBPAY(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 100;
	condition = dia_kardif_permjobpay_condition;
	information = dia_kardif_permjobpay_info;
	permanent = TRUE;
	description = "��� ������ ���� ������?";
};


func int dia_kardif_permjobpay_condition()
{
	if(mis_moebored == LOG_SUCCESS)
	{
		return TRUE;
	};
};

func void dia_kardif_permjobpay_info()
{
	var int daynow;
	var int sumpay;
	var int kardifpayday;
	daynow = Wld_GetDay();
	AI_Output(other,self,"DIA_Kardif_PermJobPay_01_00");	//��� ������ ���� ������?
	if(kardifpayday < daynow)
	{
		AI_Output(self,other,"DIA_Kardif_PermJobPay_01_01");	//�������, ��������! ���, ����� ���� ����.
		sumpay = 15 * (daynow - kardifpayday);
		B_GiveInvItems(self,other,ItMi_Gold,sumpay);
		kardifpayday = Wld_GetDay();
	}
	else
	{
		AI_Output(self,other,"DIA_Kardif_PermJobPay_01_03");	//�������, ���� ������, �� ���� �� ��� ������� �������.
	};
};


instance DIA_KARDIF_R(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 6;
	condition = dia_kardif_r_condition;
	information = dia_kardif_r_info;
	permanent = FALSE;
	important = TRUE;
};


func int dia_kardif_r_condition()
{
	if(mis_moebored == LOG_SUCCESS)
	{
		return TRUE;
	};
};

func void dia_kardif_r_info()
{
	AI_Output(self,other,"DIA_Kardif_R1");	//�������-��, � ���� ���� ��������� ��� ����.
	AI_Output(other,self,"DIA_Kardif_R2");	//����������!
	AI_Output(self,other,"DIA_Kardif_R3");	//��������� ���� ����� ����� ��� �������
	AI_Output(self,other,"DIA_Kardif_R4");	//�� ���-��� ����� � ����, �� ������������ �� ����, ��� ��� ����� � ���� �� ����.
	AI_Output(self,other,"DIA_Kardif_R5");	//�� ������ ������ ������ �����, �� � ��� ��� � ��� ����� �� �����.
	AI_Output(self,other,"DIA_Kardif_R6");	//��� � ������ � ���� ��� ������, ���� �� �� ���������, �� ������ ����� ���.
	mis_rkardif = LOG_Running;
	Log_CreateTopic(TOPIC_RKARDIF,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_RKARDIF,LOG_Running);
	B_LogEntry(TOPIC_RKARDIF,"������� ������ ������� ������ �������, �� �� �� ��� ��� ����� �� ������. ������ �����, ����� � ������ ������, �� ��� ����� ��������.");
};


var int Proval;

instance DIA_KARDIF_MISSION(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 6;
	condition = dia_kardif_mission_condition;
	information = dia_kardif_mission_info;
	permanent = FALSE;
	description = "������ �������...";
};


func int dia_kardif_mission_condition()
{
	if(Npc_KnowsInfo(hero,dia_garvell_arena) || (garvell_dolg == TRUE))
	{
		return TRUE;
	};
};

func void dia_kardif_mission_info()
{
	if(win_garvell == TRUE)
	{
		AI_Output(other,self,"dia_kardif_mission1");	//� �������� �� ���� �����.
		AI_Output(self,other,"Dia_Kardif_mission2");	//�?
		AI_Output(other,self,"dia_kardif_mission3");	//�� ������� ������� �������! �����, �� ��� ��������.
		AI_Output(self,other,"Dia_Kardif_mission4");	//��-��... �������! ���, ����� ���� ����.
		B_GivePlayerXP(XP_LobartBugs);
		Log_SetTopicStatus(TOPIC_RKARDIF,LOG_SUCCESS);
		B_LogEntry(TOPIC_RKARDIF,"� ��������� ������� � ��������. �� ��� ����� �������.");
		B_GiveInvItems(self,other,ItMi_Gold,50);
		mis_rkardif = LOG_SUCCESS;
	}
	else if(lost_garvell == TRUE)
	{
		AI_Output(other,self,"dia_kardif_mission4");	//� ���������, � �� ������� �����, � �� ���� ���� ����� ��� ��� ����� ��������!
		AI_Output(self,other,"Dia_Kardif_Mission5");	//���?! ���... ���, ������ ������! �����, �������� � ���� ������ �� ������ �� �������...
		Log_SetTopicStatus(TOPIC_RKARDIF,LOG_SUCCESS);
		B_LogEntry(TOPIC_RKARDIF,"� ���������, � �������� ���� �������. ������ ��� ����� ���, � ������ � ������ � ������.");
		mis_rkardif = LOG_FAILED;
		Proval = TRUE;
	}
	else
	{
		AI_Output(other,self,"Dia_Kardif_Mission6");	//� ������ ������.
		AI_Output(self,other,"Dia_Kardif_Mission7");	//�������, ����� �� ����.
		B_GivePlayerXP(XP_LobartBugs);
		Log_SetTopicStatus(TOPIC_RKARDIF,LOG_SUCCESS);
		B_LogEntry(TOPIC_RKARDIF,"� ����� ������� ���� ������, � �� ��� ����� �������.");
		B_GiveInvItems(other,self,ItMi_Gold,100);
		mis_rkardif = LOG_SUCCESS;
		AI_Output(self,other,"Dia_Kardif_Mission8");	//� ��� ���� �������!
		B_GiveInvItems(self,other,ItMi_Gold,50);
	};
};


instance DIA_kardif_mission2(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 16;
	condition = dia_kardif_mission2_condition;
	information = dia_kardif_mission2_info;
	permanent = FALSE;
	important = TRUE;
};


func int dia_kardif_mission2_condition()
{
	if(Npc_KnowsInfo(hero,DIA_Lord_Hagen_Pass) && (mis_moebored == LOG_SUCCESS))
	{
		return TRUE;
	};
};

func void dia_kardif_mission2_info()
{
	AI_Output(self,other,"Dia_Kardif_misiion2_14_01");	//������, ��� �� �����! � ���� ���� ����� ������� ��� ����.
	AI_Output(other,self,"Dia_Kardif_Mission2_15_02");	//�����������.
	AI_Output(self,other,"Dia_Kardif_misiion2_14_03");	//���� � ���, ��� � ���� ������������� �������...
	AI_Output(self,other,"Dia_Kardif_misiion2_14_04");	//��� ������ ����� ����� ����, �������� 100 ������ ������ ����.
	AI_Output(other,self,"Dia_Kardif_Mission2_15_05");	//� �� ������, ��� �� � ������ ���� ����?
	AI_Output(self,other,"Dia_Kardif_misiion2_14_06");	//��, � ���������.
	mis_rkardif2 = LOG_Running;
	Log_CreateTopic(TOPIC_RKARDIF2,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_RKARDIF2,LOG_Running);
	B_LogEntry(TOPIC_RKARDIF2,"� ������� ������������� �������, ������� ��� ����� �������� ��� ����� 100 ������ ������ ����, � ���������...");
};


instance DIA_KARDIF_MEAT(C_Info)
{
	npc = VLK_431_Kardif;
	nr = 26;
	condition = dia_kardif_meat_condition;
	information = dia_kardif_meat_info;
	permanent = FALSE;
};


func int dia_kardif_meat_condition()
{
	if(Npc_KnowsInfo(hero,DIA_kardif_mission2) && (Npc_HasItems(other,ItFoMuttonRaw) >= 100))
	{
		return TRUE;
	};
};

func void dia_kardif_meat_info()
{
	AI_Output(other,self,"Dia_Kardif_meat_15_01");	//� ������ ����.
	AI_Output(self,other,"Dia_Kardif_meat_14_02");	//�������! ����� ��� ����.
	B_GiveInvItems(other,self,ItFoMuttonRaw,100);
	AI_Output(self,other,"Dia_Kardif_meat_14_03");	//���, ���� �������!
	B_GiveInvItems(self,other,ItMi_Gold,150);
	B_GivePlayerXP(200);
	Log_SetTopicStatus(TOPIC_RKARDIF2,LOG_SUCCESS);
	B_LogEntry(TOPIC_RKARDIF2,"� ������ ������� ����.");
	mis_rkardif2 = LOG_SUCCESS;
};

