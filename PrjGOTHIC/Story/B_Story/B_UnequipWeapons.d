
func void B_UnequipWeapons(var C_Npc slf)
{
	var C_Item weapon;
	if(Npc_HasEquippedMeleeWeapon(slf))
	{
		weapon = Npc_GetEquippedMeleeWeapon(slf);
		Npc_RemoveInvItem(slf,Hlp_GetInstanceID(weapon));
		CreateInvItem(slf,Hlp_GetInstanceID(weapon));
	};
	if(Npc_HasEquippedRangedWeapon(slf))
	{
		weapon = Npc_GetEquippedRangedWeapon(slf);
		Npc_RemoveInvItem(slf,Hlp_GetInstanceID(weapon));
		CreateInvItem(slf,Hlp_GetInstanceID(weapon));
	};
};

func void B_NpcSetJailed(var C_Npc slf)
{
	B_UnequipWeapons(slf);
	slf.attribute[ATR_STRENGTH] = Condition_VLKDolch - 1;
	slf.attribute[ATR_Dexterity] = Condition_Kurzbogen - 1;
	if((slf.guild == GIL_KDF) || (slf.guild == GIL_KDW) || (slf.guild == GIL_PAL) || (slf.aivar[AIV_MagicUser] == MAGIC_ALWAYS))
	{
		if(Npc_IsDrawingSpell(slf))
		{
			AI_UnreadySpell(slf);
		};
		slf.attribute[ATR_MANA] = SPL_Cost_Scroll - 1;
		slf.attribute[ATR_MANA_MAX] = SPL_Cost_Scroll - 1;
	};
};

func void B_NpcSetReleased(var C_Npc slf)
{
	if(slf.attribute[ATR_STRENGTH] != slf.aivar[REAL_STRENGTH])
	{
		slf.attribute[ATR_STRENGTH] = slf.aivar[REAL_STRENGTH];
	};
	if(slf.attribute[ATR_Dexterity] != slf.aivar[REAL_DEXTERITY])
	{
		slf.attribute[ATR_Dexterity] = slf.aivar[REAL_DEXTERITY];
	};
	if(((slf.guild == GIL_KDF) || (slf.guild == GIL_KDW) || (slf.guild == GIL_PAL) || (slf.aivar[AIV_MagicUser] == MAGIC_ALWAYS)) && (slf.attribute[ATR_MANA_MAX] != slf.aivar[REAL_MANA_MAX]))
	{
		slf.attribute[ATR_MANA_MAX] = slf.aivar[REAL_MANA_MAX];
		slf.attribute[ATR_MANA] = slf.attribute[ATR_MANA_MAX];
	};
	AI_EquipBestMeleeWeapon(slf);
	AI_EquipBestRangedWeapon(slf);
};

