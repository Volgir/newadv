
func int C_PlayerHasFakeGuild(var C_Npc slf,var C_Npc oth)
{
	var C_Item itm;
	var int NSC_CommentRangerArmor;
	itm = Npc_GetEquippedArmor(oth);
	NSC_CommentRangerArmor = TRUE;
	if(slf.aivar[AIV_IgnoresArmor] == TRUE)
	{
		return FALSE;
	};
	if(slf.aivar[AIV_IgnoresFakeGuild] == TRUE)
	{
		return FALSE;
	};
	if(C_NpcIsGateGuard(slf))
	{
		return FALSE;
	};
	if((slf.guild == GIL_BDT) && C_PlayerIsFakeBandit(slf,oth))
	{
		if(!Npc_HasEquippedArmor(oth))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		};
	};
	if(!Npc_HasEquippedArmor(oth))
	{
		return FALSE;
	}
	else if(slf.guild == oth.guild)
	{
		if(Hlp_IsItem(itm,ITAR_RANGER_Addon))
		{
			if(NSC_CommentRangerArmor == TRUE)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			};
		};
		if(oth.guild == GIL_NONE)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_MIL)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_PAL)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_SLD)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_DJG)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_NOV)
		{
			return FALSE;
		}
		else if(oth.guild == GIL_KDF)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		};
	}
	else
	{
		return FALSE;
	};
};

