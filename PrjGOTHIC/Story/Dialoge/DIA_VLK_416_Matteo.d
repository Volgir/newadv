
const int APP_Matteo = 4;
var int matteo_lehrling_day;

instance DIA_Matteo_EXIT(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 999;
	condition = DIA_Matteo_EXIT_Condition;
	information = DIA_MAtteo_EXIT_Info;
	permanent = TRUE;
	description = Dialog_Ende;
};


func int DIA_Matteo_EXIT_Condition()
{
	return TRUE;
};

func void DIA_MAtteo_EXIT_Info()
{
	AI_StopProcessInfos(self);
};


instance DIA_Matteo_PICKPOCKET(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 900;
	condition = DIA_Matteo_PICKPOCKET_Condition;
	information = DIA_Matteo_PICKPOCKET_Info;
	permanent = TRUE;
	description = Pickpocket_80;
};


func int DIA_Matteo_PICKPOCKET_Condition()
{
	return C_Beklauen(80,150);
};

func void DIA_Matteo_PICKPOCKET_Info()
{
	Info_ClearChoices(DIA_Matteo_PICKPOCKET);
	Info_AddChoice(DIA_Matteo_PICKPOCKET,Dialog_Back,DIA_Matteo_PICKPOCKET_BACK);
	Info_AddChoice(DIA_Matteo_PICKPOCKET,DIALOG_PICKPOCKET,DIA_Matteo_PICKPOCKET_DoIt);
};

func void DIA_Matteo_PICKPOCKET_DoIt()
{
	B_Beklauen();
	Info_ClearChoices(DIA_Matteo_PICKPOCKET);
};

func void DIA_Matteo_PICKPOCKET_BACK()
{
	Info_ClearChoices(DIA_Matteo_PICKPOCKET);
};


instance DIA_Matteo_Hallo(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 1;
	condition = DIA_Matteo_Hallo_Condition;
	information = DIA_MAtteo_Hallo_Info;
	permanent = FALSE;
	important = TRUE;
};


func int DIA_Matteo_Hallo_Condition()
{
	if(Npc_IsInState(self,ZS_Talk))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_Hallo_Info()
{
	AI_Output(self,other,"DIA_Matteo_Hallo_09_00");	//��� ���� ������?
};


instance DIA_Matteo_SellWhat(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 1;
	condition = DIA_Matteo_SellWhat_Condition;
	information = DIA_MAtteo_SellWhat_Info;
	permanent = FALSE;
	description = "��� �� ��������?";
};


func int DIA_Matteo_SellWhat_Condition()
{
	return TRUE;
};

func void DIA_MAtteo_SellWhat_Info()
{
	AI_Output(other,self,"DIA_Matteo_SellWhat_15_00");	//��� �� ��������?
	AI_Output(self,other,"DIA_Matteo_SellWhat_09_01");	//� ���� ���������� ���, ��� ����� ������������ � ����� �����������. ������, ������, ��������... ���� �������.
	AI_Output(self,other,"DIA_Matteo_SellWhat_09_02");	//� ���� ���� ����� ������� ��������� �� ������.
	AI_Output(self,other,"DIA_Matteo_SellWhat_09_03");	//������� ��������� ������� �� ���� �������� - ������ �����. ���� ��� ���������?
	if(Knows_Matteo == FALSE)
	{
		Log_CreateTopic(TOPIC_CityTrader,LOG_NOTE);
		B_LogEntry(TOPIC_CityTrader,"����� ������ ��������� � ����� ����� ������. �� ������� ����������, ������ � ������ ������.");
		Knows_Matteo = TRUE;
	};
};


instance DIA_Matteo_TRADE(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 800;
	condition = DIA_Matteo_TRADE_Condition;
	information = DIA_Matteo_TRADE_Info;
	permanent = TRUE;
	description = "������ ��� ���� ������.";
	trade = TRUE;
};


func int DIA_Matteo_TRADE_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_SellWhat))
	{
		return TRUE;
	};
};


var int Matteo_TradeNewsPermanent;

func void DIA_Matteo_TRADE_Info()
{
	B_GiveTradeInv(self);
	AI_Output(other,self,"DIA_Matteo_TRADE_15_00");	//������ ��� ���� ������.
	if((Kapitel == 3) && (MIS_RescueBennet != LOG_SUCCESS) && (Matteo_TradeNewsPermanent == FALSE))
	{
		AI_Output(self,other,"DIA_Matteo_TRADE_09_01");	//� ��� ���, ��� �������� ���������� ������, ��������� ��������� ����� ����������� ���� ��������.
		AI_Output(self,other,"DIA_Matteo_TRADE_09_02");	//� �������, ��� ����������, ����� ����� ������ �������.
		Matteo_TradeNewsPermanent = 1;
	};
	if((Kapitel == 5) && (Matteo_TradeNewsPermanent < 2))
	{
		AI_Output(self,other,"DIA_Matteo_TRADE_09_03");	//������, �������� ������������� �������� ��������� �� ���� ���. ��� ���� ��������� ������ �� ����� �������.
		AI_Output(self,other,"DIA_Matteo_TRADE_09_04");	//��� ������, ��� �� ����� ���������� ���� ������, ��� �����, ����� �� ��� ������������ ���� ����� �� ��������� ������.
		Matteo_TradeNewsPermanent = 2;
	};
};


var int Matteo_LeatherBought;

instance DIA_Matteo_LEATHER(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 850;
	condition = DIA_Matteo_LEATHER_Condition;
	information = DIA_Matteo_LEATHER_Info;
	permanent = TRUE;
	description = "������ ������������ ������. ������: ������ 15, ������ 15. (100 ������)";
};


func int DIA_Matteo_LEATHER_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_SellWhat) && (Matteo_LeatherBought == FALSE))
	{
		return TRUE;
	};
};

func void DIA_Matteo_LEATHER_Info()
{
	AI_Output(other,self,"DIA_Matteo_LEATHER_15_00");	//������, ����� ��� ��� �������.
	if(B_GiveInvItems(other,self,ItMi_Gold,100))
	{
		AI_Output(self,other,"DIA_Matteo_LEATHER_09_01");	//��� ���� ����������. (����������)
		B_GiveInvItems(self,other,ITAR_Bau_L,1);
		Matteo_LeatherBought = TRUE;
	}
	else
	{
		AI_Output(self,other,"DIA_Matteo_LEATHER_09_02");	//��� ������� ����� �������� - �� ���, �����������, ����� ����� �����. ��� ��� �����������, ����� � ���� ����� ���������� ������.
	};
};


instance DIA_Matteo_Paladine(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 2;
	condition = DIA_Matteo_Paladine_Condition;
	information = DIA_MAtteo_Paladine_Info;
	permanent = FALSE;
	description = "��� �� ������ � ���������?";
};


func int DIA_Matteo_Paladine_Condition()
{
	if(other.guild != GIL_PAL)
	{
		return TRUE;
	};
};

func void DIA_MAtteo_Paladine_Info()
{
	AI_Output(other,self,"DIA_Matteo_Paladine_15_00");	//��� �� ������ � ���������?
	AI_Output(self,other,"DIA_Matteo_Paladine_09_01");	//� ��� ���, ��� ��� ������� ������� � �����, � ���� �� ��� ���� ��������.
	AI_Output(self,other,"DIA_Matteo_Paladine_09_02");	//��������� ���, ����� � ��� � ������� �������, ��������� ���������� ��� ������ � ������ ��������, ��� ��� ��� �����!
	AI_Output(other,self,"DIA_Matteo_Paladine_15_03");	//�?
	AI_Output(self,other,"DIA_Matteo_Paladine_09_04");	//������� ��, ��� ���������� ����!
	AI_Output(self,other,"DIA_Matteo_Paladine_09_05");	//�� � ���� ��� ���� ����� � ���� ������, ����� ��� ���������� ������� ��� ������ ��� ���� ������!
	AI_Output(self,other,"DIA_Matteo_Paladine_09_06");	//� ����� ��� ������� ������ � ������������ �������� ���� �������!
};


instance DIA_Matteo_Confiscated(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 2;
	condition = DIA_Matteo_Confiscated_Condition;
	information = DIA_MAtteo_Confiscated_Info;
	permanent = FALSE;
	description = "�������� ������� ���� ������?";
};


func int DIA_Matteo_Confiscated_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_Paladine))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_Confiscated_Info()
{
	AI_Output(other,self,"DIA_Matteo_Confiscated_15_00");	//�������� ������� ���� ������?
	AI_Output(self,other,"DIA_Matteo_Confiscated_09_01");	//���, ��� � ���� ��������� �� ������ �����.
	AI_Output(self,other,"DIA_Matteo_Confiscated_09_02");	//��� ������ ��������� ��������� ����� ������ �� ����.
	AI_Output(self,other,"DIA_Matteo_Confiscated_09_03");	//��� ��� �������, ��� ��� �� ������� ���. ���� �� ������� ��������.
};


instance DIA_Matteo_HelpMeToOV(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 3;
	condition = DIA_Matteo_HelpMeToOV_Condition;
	information = DIA_MAtteo_HelpMeToOV_Info;
	permanent = FALSE;
	description = "�� ������ ������ ��� ������� � ������� �������?";
};


func int DIA_Matteo_HelpMeToOV_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_Paladine) && (Player_IsApprentice == APP_NONE) && (other.guild == GIL_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_HelpMeToOV_Info()
{
	AI_Output(other,self,"DIA_Matteo_HelpMeToOV_15_00");	//�� ������ ������ ��� ������� � ������� �������?
	AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_01");	//(�����������) ���? � ��� ���� ����� ���?
	AI_Output(other,self,"DIA_Matteo_HelpMeToOV_15_02");	//� ���� ������ ���������...
	AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_03");	//���-���... � �� ������� ����� ���� ����������?
	if(Torwache_305.aivar[AIV_TalkedToPlayer] == TRUE)
	{
		AI_Output(other,self,"DIA_Matteo_HelpMeToOV_15_04");	//(������� ����) ��, ������ �� ����!
		AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_05");	//(�������) ��� ��� ������� ��� ���� �����!
	}
	else
	{
		AI_Output(other,self,"DIA_Matteo_HelpMeToOV_15_06");	//� �� �������, ����� ��� ���� ��������.
		AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_07");	//��������, �� ����.
	};
	AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_08");	//� �� ���� ��������� ������ � ���� ��������� - ��� ���� �� ��������.
	AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_09");	//�� ���� ���� �� ������� ��, ��� �������, ������� ������, �������������� � ������, ��� �� ������ ����.
	AI_Output(self,other,"DIA_Matteo_HelpMeToOV_09_10");	//������ ��� � ��� ���� ������.
};

func void B_Matteo_Preis()
{
	AI_Output(self,other,"DIA_Matteo_HelpMeNow_09_01");	//������, ��� ����� ����� ��� ����.
	AI_Output(self,other,"DIA_Matteo_HelpMeNow_09_02");	//(������) ������ � ���: ��������� ��� ����� ��� ����?
	AI_Output(other,self,"DIA_Matteo_HelpMeNow_15_03");	//�� ��� �� ���������?
	AI_Output(self,other,"DIA_Matteo_HelpMeNow_09_04");	//� ���� ������ ���� - � ����, ��� ��, ���� �� ����� ����������� ����� �����.
	AI_Output(self,other,"DIA_Matteo_HelpMeNow_09_05");	//�� ��� ���� ��������� � ��������� �����.
};


instance DIA_Matteo_HelpMeNow(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 3;
	condition = DIA_Matteo_HelpMeNow_Condition;
	information = DIA_MAtteo_HelpMeNow_Info;
	permanent = FALSE;
	description = "��� �� ������ ������ ��� ������� � ������� �������?";
};


func int DIA_Matteo_HelpMeNow_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HelpMeToOV) && (Player_IsApprentice == APP_NONE) && (other.guild == GIL_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_HelpMeNow_Info()
{
	AI_Output(other,self,"DIA_Matteo_HelpMeNow_15_00");	//��� �� ������ ������ ��� ������� � ������� �������?
	B_Matteo_Preis();
};


instance DIA_Matteo_LehrlingLater(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 3;
	condition = DIA_Matteo_LehrlingLater_Condition;
	information = DIA_MAtteo_LehrlingLater_Info;
	permanent = FALSE;
	description = "������ ��� ����� �������� ������ �� ��������.";
};


func int DIA_Matteo_LehrlingLater_Condition()
{
	if((Player_IsApprentice == APP_NONE) && (other.guild != GIL_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_LehrlingLater_Info()
{
	AI_Output(other,self,"DIA_Matteo_LehrlingLater_15_00");	//������ ��� ����� �������� ������ �� ��������.
	B_Matteo_Preis();
};


instance DIA_Matteo_PriceOfHelp(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 3;
	condition = DIA_Matteo_PriceOfHelp_Condition;
	information = DIA_MAtteo_PriceOfHelp_Info;
	permanent = FALSE;
	description = "��� �� ������ �� ���� ������?";
};


func int DIA_Matteo_PriceOfHelp_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HelpMeNow) || Npc_KnowsInfo(other,DIA_Matteo_LehrlingLater))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_PriceOfHelp_Info()
{
	AI_Output(other,self,"DIA_Matteo_PriceOfHelp_15_00");	//��� �� ������ �� ���� ������?
	AI_Output(self,other,"DIA_Matteo_PriceOfHelp_09_01");	//100 ������� �����.
	Info_ClearChoices(DIA_Matteo_PriceOfHelp);
	Info_AddChoice(DIA_Matteo_PriceOfHelp,"��� ��� ���������...",DIA_Matteo_PriceOfHelp_Wow);
	Info_AddChoice(DIA_Matteo_PriceOfHelp,"�� ��, �������!",DIA_Matteo_PriceOfHelp_Cutthroat);
};

func void B_Matteo_RegDichAb()
{
	AI_Output(self,other,"B_Matteo_RegDichAb_09_00");	//����������� ��� �����! ��� �� ���� ������ � ���� � ����.
	AI_Output(other,self,"B_Matteo_RegDichAb_15_01");	//� ���?
	AI_Output(self,other,"B_Matteo_RegDichAb_09_02");	//� ��������, ��� ��� ������.
	AI_Output(self,other,"B_Matteo_RegDichAb_09_03");	//������, ���������� ��������, ��� ����� �� ������ ��� ����.
	AI_Output(self,other,"B_Matteo_RegDichAb_09_04");	//�� ��� ��������� ������������ ��������� ���������� � ����� ������� - ��� ��������, ��� ������ � ��� ����.
	AI_Output(self,other,"B_Matteo_RegDichAb_09_05");	//� ����, ����� �� ����� �� ��� ���� ����. �� ������ ������ - ������� - ���� ����� ����������� �������.
	AI_Output(self,other,"B_Matteo_RegDichAb_09_06");	//������� ��� ��� ������, � � ������ ����.
	MIS_Matteo_Gold = LOG_Running;
	Log_CreateTopic(TOPIC_Matteo,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_Matteo,LOG_Running);
	B_LogEntry(TOPIC_Matteo,"���������� �������� ������� ������ ������ ������ 100 ������� �����. ���� � ����� �� ���, �� ������� ��� ������� � ������� �������.");
};

func void DIA_Matteo_PriceOfHelp_Cutthroat()
{
	AI_Output(other,self,"DIA_Matteo_PriceOfHelp_Cutthroat_15_00");	//�� ��, �������!
	B_Matteo_RegDichAb();
	Info_ClearChoices(DIA_Matteo_PriceOfHelp);
};

func void DIA_Matteo_PriceOfHelp_Wow()
{
	AI_Output(other,self,"DIA_Matteo_PriceOfHelp_Wow_15_00");	//��� ��� ���������...
	B_Matteo_RegDichAb();
	Info_ClearChoices(DIA_Matteo_PriceOfHelp);
};


instance DIA_Matteo_WoGritta(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 2;
	condition = DIA_Matteo_WoGritta_Condition;
	information = DIA_MAtteo_WoGritta_Info;
	permanent = FALSE;
	description = "��� ��� ����� ��� ������?";
};


func int DIA_Matteo_WoGritta_Condition()
{
	if((MIS_Matteo_Gold == LOG_Running) && (Gritta.aivar[AIV_TalkedToPlayer] == FALSE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_WoGritta_Info()
{
	AI_Output(other,self,"DIA_Matteo_WoGritta_15_00");	//��� ��� ����� ��� ������?
	AI_Output(self,other,"DIA_Matteo_WoGritta_09_01");	//��� � ��� ������� - ��� ���������� �������� - ��� ��� ��������� ���� ��� �����, ������ �� �������.
};


instance DIA_Matteo_GoldRunning(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 500;
	condition = DIA_Matteo_GoldRunning_Condition;
	information = DIA_MAtteo_GoldRunning_Info;
	permanent = TRUE;
	description = "��� ���� 100 �������!";
};


func int DIA_Matteo_GoldRunning_Condition()
{
	if((MIS_Matteo_Gold == LOG_Running) && (Npc_KnowsInfo(other,DIA_Gritta_WantsMoney) || Npc_IsDead(Gritta)))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_GoldRunning_Info()
{
	AI_Output(other,self,"DIA_Matteo_GoldRunning_15_00");	//��� ���� 100 �������!
	if(Npc_IsDead(Gritta))
	{
		AI_Output(self,other,"DIA_Matteo_GoldRunning_09_01");	//�� ���� ������ ����� ������! � �� �������, ��� ����� ������� ��!
		AI_Output(self,other,"DIA_Matteo_GoldRunning_09_02");	//� �� ���� ����� �������� ��������� � ����� ����. �� ������ ������ � ����� ������! �� ���� ���� ���������� �� ����!
		MIS_Matteo_Gold = LOG_FAILED;
		B_CheckLog();
		AI_StopProcessInfos(self);
		return;
	};
	if(B_GiveInvItems(other,self,ItMi_Gold,100))
	{
		if(Npc_HasItems(Gritta,ItMi_Gold) < 100)
		{
			AI_Output(self,other,"DIA_Matteo_GoldRunning_09_03");	//�? ���� �����-������ ��������?
			AI_Output(other,self,"DIA_Matteo_GoldRunning_15_04");	//������ ������, � ��� ������ �� ��������.
			AI_Output(self,other,"DIA_Matteo_GoldRunning_09_05");	//������! �� �������� ���� ����� ������.
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_GoldRunning_09_06");	//�� ������ ��������� �� ���? �� - � �� ���������, ����� �� ��������� ��� ������ �� ���!
			AI_Output(self,other,"DIA_Matteo_GoldRunning_09_07");	//�� ��� �� - 100 ������� ���� 100 �������.
			AI_Output(self,other,"DIA_Matteo_GoldRunning_09_08");	//�� �������� ���� ����� ������.
		};
		MIS_Matteo_Gold = LOG_SUCCESS;
		B_GivePlayerXP(XP_Matteo_Gold);
	}
	else
	{
		AI_Output(self,other,"DIA_Matteo_GoldRunning_09_09");	//� ���� �������� �� ������? � �� ���� 100 ������� �����.
	};
};


instance DIA_Matteo_Zustimmung(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 4;
	condition = DIA_Matteo_Zustimmung_Condition;
	information = DIA_MAtteo_Zustimmung_Info;
	permanent = TRUE;
	description = "������ ��� ����� �������� ������ �� ��������!";
};


var int DIA_Matteo_Zustimmung_perm;

func int DIA_Matteo_Zustimmung_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HowCanYouHelp) && ((MIS_Matteo_Gold == LOG_Running) || (MIS_Matteo_Gold == LOG_SUCCESS)) && (Player_IsApprentice == APP_NONE) && (DIA_Matteo_Zustimmung_perm == FALSE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_Zustimmung_Info()
{
	AI_Output(other,self,"DIA_Matteo_Zustimmung_15_00");	//������ ��� ����� �������� ������ �� ��������!
	if(MIS_Matteo_Gold == LOG_SUCCESS)
	{
		AI_Output(self,other,"DIA_Matteo_Zustimmung_09_01");	//�� ��������, � ������� ���� ��������.
		AI_Output(self,other,"DIA_Matteo_Zustimmung_09_02");	//������ ������� ������� �� ���� � ���� ������ �������.
		B_GivePlayerXP(XP_Zustimmung);
		B_LogEntry(TOPIC_Lehrling,"������ ���� ��� ���� ���������, ���� � ������ ����� �������� ������� �������.");
		DIA_Matteo_Zustimmung_perm = TRUE;
	}
	else
	{
		AI_Output(self,other,"DIA_Matteo_Zustimmung_09_03");	//����� ���� �������. ������� ������� ���� ����� ������ � ������� ��� ������!
	};
};


instance DIA_Matteo_HowCanYouHelp(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 4;
	condition = DIA_Matteo_HowCanYouHelp_Condition;
	information = DIA_MAtteo_HowCanYouHelp_Info;
	permanent = FALSE;
	description = "��� ������ �� ����������� ������ ���?";
};


func int DIA_Matteo_HowCanYouHelp_Condition()
{
	if((Npc_KnowsInfo(other,DIA_Matteo_HelpMeNow) || Npc_KnowsInfo(other,DIA_Matteo_LehrlingLater)) && (Player_IsApprentice == APP_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_HowCanYouHelp_Info()
{
	AI_Output(other,self,"DIA_Matteo_HowCanYouHelp_15_00");	//��� ������ �� ����������� ������ ���?
	AI_Output(self,other,"DIA_Matteo_HowCanYouHelp_09_01");	//��� ������. � ��������� ���� �������, ����� ������� ������ ������� �������� ����� ���� � �������.
	if(other.guild == GIL_NONE)
	{
		AI_Output(self,other,"DIA_Matteo_HowCanYouHelp_09_02");	//���� ��������, �� ������������� ������� � ����������� ������ � ������� ������� � ������� �������. ������ �����, �� ������� ���-������ ����������.
	};
	Log_CreateTopic(TOPIC_Lehrling,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_Lehrling,LOG_Running);
	B_LogEntry(TOPIC_Lehrling,"������ ����� ������ ��� ����� �������� ������ �� ����������.");
};


instance DIA_Matteo_WoAlsLehrling(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 4;
	condition = DIA_Matteo_WoAlsLehrling_Condition;
	information = DIA_MAtteo_WoAlsLehrling_Info;
	permanent = FALSE;
	description = "� � ���� � ���� ��������� � �������?";
};


func int DIA_Matteo_WoAlsLehrling_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HowCanYouHelp) && (Player_IsApprentice == APP_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_WoAlsLehrling_Info()
{
	AI_Output(other,self,"DIA_Matteo_WoAlsLehrling_15_00");	//� � ���� � ���� ��������� � �������?
	AI_Output(self,other,"DIA_Matteo_WoAlsLehrling_09_01");	//� ������ ������� �� ���� �����.
	AI_Output(self,other,"DIA_Matteo_WoAlsLehrling_09_02");	//��� ����� ���� ������ �����, ������-������ ������, ������� ������ ��� �������  �����������.
	AI_Output(self,other,"DIA_Matteo_WoAlsLehrling_09_03");	//���� �� ��� ����������� ������� ����.
	AI_Output(self,other,"DIA_Matteo_WoAlsLehrling_09_04");	//�� �����, ����� � ���� ���� �������� ������ �������. ����� ������ ��������.
	Log_CreateTopic(TOPIC_Lehrling,LOG_MISSION);
	Log_SetTopicStatus(TOPIC_Lehrling,LOG_Running);
	B_LogEntry(TOPIC_Lehrling,"� ���� ����� �������� �������-������� �������, ������� ������, �������� ������� ��� �������� �����������.");
	B_LogEntry(TOPIC_Lehrling,"������ ��� � ����� ��������, � ������ �������� ��������� ������ ��������.");
};


instance DIA_Matteo_WieZustimmung(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 5;
	condition = DIA_Matteo_WieZustimmung_Condition;
	information = DIA_MAtteo_WieZustimmung_Info;
	permanent = FALSE;
	description = "��� ��� �������� ��������� ������ ��������?";
};


func int DIA_Matteo_WieZustimmung_Condition()
{
	if((Npc_KnowsInfo(other,DIA_Matteo_WoAlsLehrling) || Npc_KnowsInfo(other,DIA_Matteo_WarumNichtBeiDir)) && (Player_IsApprentice == APP_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_WieZustimmung_Info()
{
	AI_Output(other,self,"DIA_Matteo_WieZustimmung_15_00");	//��� ��� �������� ��������� ������ ��������?
	AI_Output(self,other,"DIA_Matteo_WieZustimmung_09_01");	//�� ������ ������ ������� ��. ��� � �������� � ����.
	AI_Output(self,other,"DIA_Matteo_WieZustimmung_09_02");	//�� ���� ���� �� ���� �� ��� ����� ������ ����, ���� �� ��������� � �������! ��� ��� ���������� ����������� ��!
	B_LogEntry(TOPIC_Lehrling,"� ����� �������� ��������� ��������, ������ ���� � ������� ���� � ������ ������� ����� ����.");
};


instance DIA_Matteo_WarumNichtBeiDir(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 6;
	condition = DIA_Matteo_WarumNichtBeiDir_Condition;
	information = DIA_MAtteo_WarumNichtBeiDir_Info;
	permanent = FALSE;
	description = "� ������ �� �� �������� ���� � �������?";
};


func int DIA_Matteo_WarumNichtBeiDir_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HowCanYouHelp) && (Player_IsApprentice == APP_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_WarumNichtBeiDir_Info()
{
	AI_Output(other,self,"DIA_Matteo_WarumNichtBeiDir_15_00");	//� ������ �� �� �������� ���� � �������?
	AI_Output(self,other,"DIA_Matteo_WarumNichtBeiDir1");	//� ���� ��� ���� ������, ����...
	AI_Output(self,other,"DIA_Matteo_WarumNichtBeiDir2");	//...�� ����������� �������� ���� ��������, ����� ���� ����� ���� ����.
	AI_Output(self,other,"DIA_Matteo_WarumNichtBeiDir3");	//��, ��� � ��� �������, ���� ����� �������� ��������� ���� �������� ��������.
	B_LogEntry(TOPIC_Lehrling,"������ ������� ���� � �������, ���� � ������ ��������� ������ �������� ��������");
};


instance DIA_Matteo_OtherWay(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 6;
	condition = DIA_Matteo_OtherWay_Condition;
	information = DIA_MAtteo_OtherWay_Info;
	permanent = FALSE;
	description = "� ���� ������ ������ ������� � ������� �������?";
};


func int DIA_Matteo_OtherWay_Condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_HowCanYouHelp) && (Mil_305_schonmalreingelassen == FALSE) && (Player_IsApprentice == APP_NONE) && (other.guild == GIL_NONE))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_OtherWay_Info()
{
	AI_Output(other,self,"DIA_Matteo_OtherWay_15_00");	//� ���� ������ ������ ������� � ������� �������?
	AI_Output(self,other,"DIA_Matteo_OtherWay_09_01");	//��������... ���� � ���-������ ��������, � ��� ���� �����.
};


instance DIA_Matteo_Minenanteil(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 3;
	condition = DIA_Matteo_Minenanteil_Condition;
	information = DIA_MAtteo_Minenanteil_Info;
	description = "� ���� ����� ������ ������� ���� � ����� ��������������� ��������.";
};


func int DIA_Matteo_Minenanteil_Condition()
{
	if((other.guild == GIL_KDF) && (MIS_Serpentes_MinenAnteil_KDF == LOG_Running) && Npc_KnowsInfo(other,DIA_Matteo_SellWhat))
	{
		return TRUE;
	};
};

func void DIA_MAtteo_Minenanteil_Info()
{
	AI_Output(other,self,"DIA_Matteo_Minenanteil_15_00");	//� ����, � ���� ����� ������ ������� ���� � ����� ��������������� ��������. ��� ������ �� ����?
	AI_Output(self,other,"DIA_Matteo_Minenanteil_09_01");	//(������) �����? ��. � ������ ��� �������? ������� �� ����, ��� � ���� ��. �������, ���� �����.
	B_GivePlayerXP(XP_Ambient);
};


var int matteo_startguild;

instance dia_matteo_lehrling(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 5;
	condition = dia_matteo_lehrling_condition;
	information = dia_matteo_lehrling_info;
	permanent = TRUE;
	description = "����� � ���� ����� ����� ��������?";
};


func int dia_matteo_lehrling_condition()
{
	if(Npc_KnowsInfo(other,DIA_Matteo_WarumNichtBeiDir) && (Player_IsApprentice == APP_NONE))
	{
		return TRUE;
	};
};

func void dia_matteo_lehrling_info()
{
	var int stimmen;
	stimmen = 0;
	AI_Output(other,self,"DIA_Harad_LEHRLING_15_00");	//����� � ���� ����� ����� ��������?
	if(MIS_Matteo_Gold == LOG_SUCCESS)
	{
		if(MIS_Matteo_Gold == LOG_SUCCESS)
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING1");	//�� ������ ��� ��� ������, ��� �� ������� �� ����� ������!
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING2");	//�� ��� ��� �� �������� ����� �������.
		};
		AI_Output(self,other,"DIA_Matteo_LEHRLING3");	//��� �������� ����� ������, �� �� ������ ���������� � ������ ����� ������.
		stimmen = stimmen + 1;
		AI_Output(self,other,"DIA_Matteo_LEHRLING4");	//� ������ ������� ...
		if(Thorben.aivar[AIV_TalkedToPlayer] == TRUE)
		{
			if(MIS_Thorben_GetBlessings == LOG_SUCCESS)
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING5");	//������, ��� ������ ��� ���� ���� �������������.
				stimmen = stimmen + 1;
			}
			else
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING6");	//������ ������ ������ �����, ����� �� ������� ������������� �����. � �����, ��� ������� ����.
			};
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING7");	//������ �������, ��� ������� �� ����� ����.
		};
		if(Bosper.aivar[AIV_TalkedToPlayer] == TRUE)
		{
			if((MIS_Bosper_Bogen == LOG_SUCCESS) || (MIS_Bosper_WolfFurs == LOG_SUCCESS))
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING8");	//������, �������, ��� � ��� ������� �� ��� �� ��������.
				AI_Output(self,other,"DIA_Matteo_LEHRLING9");	//� ��� ��, ��� ������� ��� �������.
				stimmen = stimmen + 1;
			}
			else
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING10");	//������ �������� ����, ����� � ��� ���� ����������� ���� �� ����������� ���� � ��� �������.
				AI_Output(self,other,"DIA_Matteo_LEHRLING11");	//�������� - �� ������ ������� ������� ���� �����. ����, �������, ���� ����� ��� �����.
			};
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING12");	//������ ���� �� �����, ��� �� �����
		};
		if(Constantino.aivar[AIV_TalkedToPlayer] == TRUE)
		{
			if(B_GetGreatestPetzCrime(self) == CRIME_NONE)
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING13");	//����������� �������, ��� �� ������ ����� ���� ��������
				stimmen = stimmen + 1;
			}
			else
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING14");	//����������� �������, ��� �� ����������� � ������������ � ������ - ��� ������?
				AI_Output(self,other,"DIA_Matteo_LEHRLING15");	//���� ��� ���, �� ���� ����� ������� ��� �������� ��� ����� �������!
			};
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING16");	//����������� ������� �� ������ � ����.
		};
		if(Harad.aivar[AIV_TalkedToPlayer] == TRUE)
		{
			if((MIS_Harad_Orc == LOG_SUCCESS) || (MIS_HakonBandits == LOG_SUCCESS))
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING17");	//�����, ������� ���� ������� � �������� �����.
				stimmen = stimmen + 1;
			}
			else if((MIS_Harad_Orc == LOG_Running) || (MIS_HakonBandits == LOG_Running))
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING18");	//�����, ����������, ��� �� ������ � ����.
			}
			else
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING19");	//����� ���� ��� �� ���� �� �����.
			};
		};
		if(stimmen >= 4)
		{
			if(stimmen == 5)
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING20");	//��� ��������, ��� �� ������� ��������� ���� ��������!
			}
			else
			{
				AI_Output(self,other,"DIA_Matteo_LEHRLING21");	//��� ��������, ��� �� ������� ��������� ������� ��������. ����� ����������, ����� ���� �������� � �������.
			};
			AI_Output(self,other,"DIA_Matteo_LEHRLING22");	//�� ����� ����� ���� ��������?
			Info_ClearChoices(dia_matteo_lehrling);
			Info_AddChoice(dia_matteo_lehrling,"������ - � ������� ��� ����.",dia_matteo_lehrling_later);
			Info_AddChoice(dia_matteo_lehrling,"� ����� ����� ����� ��������!",dia_matteo_lehrling_ok);
		}
		else
		{
			AI_Output(self,other,"DIA_Matteo_LEHRLING23");	//���� ����� �������� ���������, �� ������� ����, �������  ��������. ����� �� �� ������� ����� �������� � ������ ����� ������.
			AI_Output(self,other,"DIA_Matteo_LEHRLING24");	//������� �� ������ ���������� �� ����� ���������, ������� ��� �� ������� � ����.
		};
	}
	else
	{
		AI_Output(self,other,"DIA_Matteo_LEHRLING25");	//�� ������, ��� �������� ���, ��� ���� �� ���-�� �����.
	};
};

func void dia_matteo_lehrling_ok()
{
	AI_Output(other,self,"DIA_Matteo_LEHRLING_OK1");	//� �����!
	AI_Output(self,other,"DIA_Matteo_LEHRLING_OK2");	//�������!
	AI_Output(self,other,"DIA_Matteo_LEHRLING_OK3");	//���, ������ ��� ������. � �� ����, ����� ��� ������ ������ ��������.
	CreateInvItems(self,itar_torg2,1);
	B_GiveInvItems(self,other,itar_torg2,1);
	B_RaiseAttribute(other,ATR_Dexterity,3);
	Player_IsApprentice = APP_Matteo;
	Npc_ExchangeRoutine(Lothar,"START");
	matteo_startguild = other.guild;
	matteo_lehrling_day = Wld_GetDay();
	Wld_AssignRoomToGuild("torg",GIL_NONE);
	MIS_Apprentice = LOG_SUCCESS;
	B_LogEntry(Topic_Bonus,"������ ������ ���� � �������. ������ � ����� ������� � ������� �������.");
	Log_CreateTopic(TOPIC_CityTeacher,LOG_NOTE);
	B_GivePlayerXP(XP_Lehrling);
	Info_ClearChoices(dia_matteo_lehrling);
};

func void dia_matteo_lehrling_later()
{
	AI_Output(other,self,"DIA_Matteo_LEHRLING_Later1");	//������ - � ������� ��� ����.
	AI_Output(self,other,"DIA_Matteo_LEHRLING_Later2");	//�����, � ������� ��� �������.
	Info_ClearChoices(dia_matteo_lehrling);
};


instance Dia_Matteo_Rabota(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 8;
	condition = dia_matteo_rabota_condition;
	information = dia_matteo_rabota_info;
	permanent = FALSE;
	description = "��� � ������ ������? ";
};


func int dia_matteo_rabota_condition()
{
	if(Player_IsApprentice == APP_Matteo)
	{
		return TRUE;
	};
};

func void dia_matteo_rabota_info()
{
	AI_Output(other,self,"DIA_Matteo_Rabota1");	//��� � ������ ������?
	AI_Output(self,other,"DIA_Matteo_Rabota2");	//�����������, ��� �� ���!
	AI_Output(self,other,"DIA_Matteo_Rabota3");	//�����, �� ������ ��������� ��� ��������� ���������, � � ���� �������� �� � ���� �� ������� ����.
	AI_Output(self,other,"DIA_Matteo_Rabota4");	//��� ����������� � ������������ ��������� ���������� � ������� ����: �����, �������, �������� � ��� �����. �����, �� �����.
	B_LogEntry(Topic_Bonus,"������ ����� �������� � ���� ��������� ��������� �� ������� ����. ���������, ������� � ���� �������: �������, �����, ��������, ������, ��������, �����������...");
};


instance DIA_MATTEO_SELLORNAMENTS(C_Info)
{
	npc = VLK_416_Matteo;
	nr = 25;
	condition = Dia_Matteo_SellOrn_Condition;
	information = Dia_Matteo_SellOrn_Info;
	permanent = TRUE;
	description = "� ������ ��������� ��������� ��� ����...";
};


func int Dia_Matteo_SellOrn_Condition()
{
	if(Npc_KnowsInfo(hero,Dia_Matteo_Rabota))
	{
		return TRUE;
	};
};

func void Dia_Matteo_SellOrn_Info()
{
	AI_Output(other,self,"Dia_Matteo_SellOrn_15_00");	//� ������ ��������� ��������� ��� ����...
	if((Npc_HasItems(other,ItMi_GoldCandleHolder) > 0) || (Npc_HasItems(other,ItMi_GoldNecklace) > 0) || (Npc_HasItems(other,ItMi_SilverRing) > 0) || (Npc_HasItems(other,ItMi_SilverCup) > 0) || (Npc_HasItems(other,ItMi_SilverPlate) > 0) || (Npc_HasItems(other,ItMi_GoldPlate) > 0) || (Npc_HasItems(other,ItMi_GoldCup) > 0) || (Npc_HasItems(other,ItMi_GoldRing) > 0) || (Npc_HasItems(other,ItMi_SilverChalice) > 0) || (Npc_HasItems(other,ItMi_JeweleryChest) > 0) || (Npc_HasItems(other,ItMi_GoldChalice) > 0) || (Npc_HasItems(other,ItMi_GoldChest) > 0) || (Npc_HasItems(other,ItMi_SilverCandleHolder) > 0) || (Npc_HasItems(other,ItMi_SilverNecklace) > 0))
	{
		if(Npc_HasItems(other,ItMi_GoldCandleHolder) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_01");	//������� ����������? ���������, ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldCandleHolder) * Value_GoldCandleHolder) / 2);
			B_GiveInvItems(other,self,ItMi_GoldCandleHolder,Npc_HasItems(other,ItMi_GoldCandleHolder));
		};
		if(Npc_HasItems(other,ItMi_SilverCandleHolder) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_02");	//���������� ����������? �������, ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverCandleHolder) * Value_SilverCandleHolder) / 2);
			B_GiveInvItems(other,self,ItMi_SilverCandleHolder,Npc_HasItems(other,ItMi_SilverCandleHolder));
		};
		if(Npc_HasItems(other,ItMi_GoldNecklace) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_03");	//��������? �� ������? �����������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldNecklace) * Value_GoldNecklace) / 2);
			B_GiveInvItems(other,self,ItMi_GoldNecklace,Npc_HasItems(other,ItMi_GoldNecklace));
		};
		if(Npc_HasItems(other,ItMi_SilverNecklace) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_04");	//��������? �� �������? �������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverNecklace) * Value_SilverNecklace) / 2);
			B_GiveInvItems(other,self,ItMi_SilverNecklace,Npc_HasItems(other,ItMi_SilverNecklace));
		};
		if(Npc_HasItems(other,ItMi_GoldRing) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_05");	//������� ������! ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldRing) * Value_GoldRing) / 2);
			B_GiveInvItems(other,self,ItMi_GoldRing,Npc_HasItems(other,ItMi_GoldRing));
		};
		if(Npc_HasItems(other,ItMi_SilverRing) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_06");	//���������� ������! ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverRing) * Value_SilverRing) / 2);
			B_GiveInvItems(other,self,ItMi_SilverRing,Npc_HasItems(other,ItMi_SilverRing));
		};
		if(Npc_HasItems(other,ItMi_GoldPlate) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_07");	//������� �� ������, ���������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldPlate) * Value_GoldPlate) / 2);
			B_GiveInvItems(other,self,ItMi_GoldPlate,Npc_HasItems(other,ItMi_GoldPlate));
		};
		if(Npc_HasItems(other,ItMi_SilverPlate) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_08");	//������� �� �������, �������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverPlate) * Value_SilverPlate) / 2);
			B_GiveInvItems(other,self,ItMi_SilverPlate,Npc_HasItems(other,ItMi_SilverPlate));
		};
		if(Npc_HasItems(other,ItMi_GoldChalice) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_09");	//������� ����? �������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldChalice) * Value_GoldChalice) / 2);
			B_GiveInvItems(other,self,ItMi_GoldChalice,Npc_HasItems(other,ItMi_GoldChalice));
		};
		if(Npc_HasItems(other,ItMi_SilverChalice) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_10");	//���������� ����? �������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverChalice) * Value_SilverChalice) / 2);
			B_GiveInvItems(other,self,ItMi_SilverChalice,Npc_HasItems(other,ItMi_SilverChalice));
		};
		if(Npc_HasItems(other,ItMi_GoldCup) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_11");	//������� �����! ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldCup) * Value_GoldCup) / 2);
			B_GiveInvItems(other,self,ItMi_GoldCup,Npc_HasItems(other,ItMi_GoldCup));
		};
		if(Npc_HasItems(other,ItMi_SilverCup) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_12");	//���������� �����! ����� ��� ����!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_SilverCup) * Value_SilverCup) / 2);
			B_GiveInvItems(other,self,ItMi_SilverCup,Npc_HasItems(other,ItMi_SilverCup));
		};
		if(Npc_HasItems(other,ItMi_JeweleryChest) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_13");	//���, ��������! ��� ��� � � ���������������! ������ ���������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_JeweleryChest) * Value_JeweleryChest) / 2);
			B_GiveInvItems(other,self,ItMi_JeweleryChest,Npc_HasItems(other,ItMi_JeweleryChest));
		};
		if(Npc_HasItems(other,ItMi_GoldChest) > 0)
		{
			AI_Output(self,other,"Dia_Matteo_SellOrn_09_14");	//��������? ��� �, �������!
			B_GiveInvItems(self,other,ItMi_Gold,(Npc_HasItems(other,ItMi_GoldChest) * Value_GoldChest) / 2);
			B_GiveInvItems(other,self,ItMi_GoldChest,Npc_HasItems(other,ItMi_GoldChest));
		};
		AI_Output(self,other,"Dia_Matteo_SellOrn_09_15");	//�� ���� ������� ���� ���������! ������� ���, ����� � ���� ���-������ ��������!
	}
	else
	{
		AI_Output(self,other,"Dia_Matteo_SellOrn_09_16");	//�� � ���� �� ������ ���...
	};
};

