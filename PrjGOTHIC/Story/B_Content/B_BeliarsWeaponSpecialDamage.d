
var int RavenBlitz;

func void B_BeliarsWeaponSpecialDamage(var C_Npc oth,var C_Npc slf)
{
	var int RavenRandy;
	var int DamageRandy;
	var C_Item otherweap;
	var C_Item Armor;
	var int PoisonProtection;
	var int EdgeProtection;
	var int PointProtection;
	var int FireProtection;
	var int MagicProtection;
	var int NpcHasMuttonRaw;
	var int NpcHasLurMuttonRaw;
	if(Hlp_GetInstanceID(slf) == Hlp_GetInstanceID(Raven))
	{
		Wld_PlayEffect("spellFX_BELIARSRAGE_COLLIDE",slf,slf,0,0,0,FALSE);
		if(RavenBlitz <= 0)
		{
			Wld_PlayEffect("spellFX_BELIARSRAGE",oth,oth,0,0,0,FALSE);
			B_MagicHurtNpc(slf,oth,50);
			RavenBlitz += 1;
		}
		else if(RavenBlitz >= 3)
		{
			RavenRandy = Hlp_Random(3);
			if(RavenRandy <= 50)
			{
				RavenBlitz = 0;
			};
		}
		else
		{
			RavenBlitz += 1;
		};
	}
	else if(Hlp_GetInstanceID(oth) == Hlp_GetInstanceID(hero))
	{
		DamageRandy = Hlp_Random(100);
		if(C_ScHasReadiedBeliarsWeapon() && (DamageRandy <= BeliarDamageChance))
		{
			if(slf.aivar[AIV_MM_REAL_ID] == ID_DRAGON_UNDEAD)
			{
				Wld_PlayEffect("spellFX_BELIARSRAGE",oth,oth,0,0,0,FALSE);
				B_MagicHurtNpc(slf,oth,100);
			}
			else if(slf.flags != NPC_FLAG_IMMORTAL)
			{
				Wld_PlayEffect("spellFX_BELIARSRAGE",slf,slf,0,0,0,FALSE);
				B_MagicHurtNpc(oth,slf,100);
			};
			Wld_PlayEffect("spellFX_BELIARSRAGE_COLLIDE",hero,hero,0,0,0,FALSE);
		};
		if(C_ScHasReadiedBeliarsWeapon() && (DamageRandy <= 50))
		{
			Wld_PlayEffect("spellFX_BELIARSRAGE_COLLIDE",hero,hero,0,0,0,FALSE);
		};
	};
	if(Npc_IsPlayer(oth))
	{
		otherweap = Npc_GetReadiedWeapon(oth);
		NpcHasMuttonRaw = Npc_HasItems(slf,ItFoMuttonRaw);
		NpcHasLurMuttonRaw = Npc_HasItems(slf,ItFoMuttonRaw);
		if(Npc_HasEquippedArmor(slf))
		{
			Armor = Npc_GetEquippedArmor(slf);
			PointProtection = Armor.protection[PROT_POINT];
			PoisonProtection = Armor.protection[PROT_BLUNT];
			EdgeProtection = Armor.protection[PROT_EDGE];
			FireProtection = Armor.protection[PROT_FIRE];
			MagicProtection = Armor.protection[PROT_MAGIC];
		}
		else
		{
			PointProtection = slf.protection[PROT_POINT];
			PoisonProtection = slf.protection[PROT_BLUNT];
			EdgeProtection = slf.protection[PROT_EDGE];
			FireProtection = slf.protection[PROT_FIRE];
			MagicProtection = slf.protection[PROT_MAGIC];
		};
		if(Hlp_IsItem(otherweap,ItMw_BeliarWeapon_Fire))
		{
			Wld_PlayEffect("VOB_MAGICBURN",slf,slf,0,0,0,FALSE);
		};
		if(Hlp_IsItem(otherweap,ItRw_Addon_FireBow))
		{
			Wld_PlayEffect("spellFX_FIREBOLT_COLLIDE",slf,slf,0,0,0,FALSE);
			if(slf.flags != NPC_FLAG_IMMORTAL)
			{
				Npc_ChangeAttribute(slf,ATR_HITPOINTS,-slf.protection[PROT_POINT]);
			};
			if(Npc_IsDead(slf))
			{
				B_GiveDeathXP(oth,slf);
			};
		};
		if(Hlp_IsItem(otherweap,ItRw_Addon_MagicBow))
		{
			Wld_PlayEffect("spellFX_ICEBOLT_COLLIDE",slf,slf,0,0,0,FALSE);
			if(slf.flags != NPC_FLAG_IMMORTAL)
			{
				Npc_ChangeAttribute(slf,ATR_HITPOINTS,-slf.protection[PROT_POINT]);
			};
			if(Npc_IsDead(slf))
			{
				B_GiveDeathXP(oth,slf);
			};
		};
		if(Hlp_IsItem(otherweap,ItMW_Addon_Stab05))
		{
			Wld_PlayEffect("spellFX_Adanosball",slf,slf,0,0,0,FALSE);
		};
		if(Hlp_IsItem(otherweap,ItMW_Addon_Stab04))
		{
			Wld_PlayEffect("VOB_MAGICBURN",slf,slf,0,0,0,FALSE);
		};
		if(Hlp_IsItem(otherweap,ItRw_Addon_MagicCrossbow))
		{
			Wld_PlayEffect("spellFX_BELIARSRAGE_COLLIDE",slf,slf,0,0,0,FALSE);
			if(slf.flags != NPC_FLAG_IMMORTAL)
			{
				Npc_ChangeAttribute(slf,ATR_HITPOINTS,-slf.protection[PROT_POINT]);
			};
			if(Npc_IsDead(slf))
			{
				B_GiveDeathXP(oth,slf);
			};
		};
	};
};

