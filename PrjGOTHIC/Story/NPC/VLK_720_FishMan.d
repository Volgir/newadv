
instance VLK_720_FishMan(Npc_Default)
{
	name[0] = NAME_FishMan;
	guild = GIL_VLK;
	id = 720;
	voice = 6;
	flags = 0;
	npctype = NPCTYPE_AMBIENT;
	aivar[AIV_ToughGuy] = TRUE;
	aivar[AIV_ToughGuyNewsOverride] = TRUE;
	B_SetAttributesToChapter(self,2);
	fight_tactic = FAI_HUMAN_COWARD;
	EquipItem(self,ItMw_1h_Vlk_Dagger);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_Bald",Face_L_Normal_GorNaBar,BodyTex_L,ITAR_Bau_L);
	Mdl_SetModelFatness(self,0);
	Mdl_ApplyOverlayMds(self,"Humans_Arrogance.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,25);
	daily_routine = Rtn_Start_720;
};


func void Rtn_Start_720()
{
	TA_Pick_FP(8,0,12,0,"FISHMAN10");
	TA_Smalltalk(12,0,15,0,"FISHMAN1");
	TA_Cook_Pan(15,0,18,0,"FISHMAN11");
	TA_Stand_Eating(18,0,20,0,"FISHMAN2");
	TA_Sleep(20,0,8,0,"FISHMAN2");
};

