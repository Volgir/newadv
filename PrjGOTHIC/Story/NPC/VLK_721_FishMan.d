
instance VLK_721_FishMan(Npc_Default)
{
	name[0] = NAME_FishMan;
	guild = GIL_VLK;
	id = 721;
	voice = 6;
	flags = 0;
	npctype = NPCTYPE_AMBIENT;
	aivar[AIV_ToughGuy] = TRUE;
	aivar[AIV_ToughGuyNewsOverride] = TRUE;
	B_SetAttributesToChapter(self,2);
	fight_tactic = FAI_HUMAN_NORMAL;
	EquipItem(self,ItMw_1h_Sld_Sword);
	B_CreateAmbientInv(self);
	B_SetNpcVisual(self,MALE,"Hum_Head_FatBald",Face_P_Gilbert,BodyTex_L,ITAR_Bau_L);
	Mdl_SetModelFatness(self,0);
	Mdl_ApplyOverlayMds(self,"Humans_Arrogance.mds");
	B_GiveNpcTalents(self);
	B_SetFightSkills(self,30);
	daily_routine = Rtn_Start_721;
};


func void Rtn_Start_721()
{
	TA_Stand_Eating(8,0,9,0,"FISHMAN4");
	TA_Sit_Chair(9,0,12,0,"NW_CITY_HABOUR_TAVERN01_05");
	TA_Smalltalk(12,0,15,0,"FISHMAN3");
	TA_Pick_FP(15,0,20,0,"FISHMAN10");
	TA_Sit_Campfire(20,0,22,0,"FISHMAN13");
	TA_Sleep(22,0,8,0,"FISHMAN4");
};

