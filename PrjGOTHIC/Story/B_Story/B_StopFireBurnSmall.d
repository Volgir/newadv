
var int FireBurnSmallCounter;

func void B_StopFireBurnSmall()
{
	Npc_PercEnable(self,PERC_ASSESSMAGIC,B_AssessMagic);
	Npc_ClearAIQueue(self);
	AI_Standup(self);
	if(self.guild < GIL_SEPERATOR_HUM)
	{
		B_AssessDamage();
		AI_ContinueRoutine(self);
	}
	else
	{
		Npc_SetTempAttitude(self,ATT_HOSTILE);
		AI_ContinueRoutine(self);
	};
};

func int ZS_FireBurnSmall()
{
	Npc_PercEnable(self,PERC_ASSESSSTOPMAGIC,B_StopFireBurnSmall);
	if(!Npc_HasBodyFlag(self,BS_FLAG_INTERRUPTABLE))
	{
		AI_Standup(self);
	}
	else
	{
		AI_StandupQuick(self);
	};
	if(self.guild < GIL_SEPERATOR_HUM)
	{
		AI_PlayAni(self,"T_STAND_2_LIGHTNING_VICTIM");
	};
	if(FireBurnSmallCounter > 0)
	{
		FireBurnSmallCounter = 0;
	};
	return TRUE;
};

func int ZS_FireBurnSmall_Loop()
{
	var C_Item Armor;
	var int FireProtection;
	var int FireBurnSmallDamage;
	FireBurnSmallDamage = SPL_PYRO_DAMAGE_PER_SEC - FireProtection;
	if(Npc_HasEquippedArmor(self))
	{
		Armor = Npc_GetEquippedArmor(self);
		FireProtection = Armor.protection[PROT_FIRE];
	}
	else
	{
		FireProtection = self.protection[PROT_FIRE];
	};
	if(FireBurnSmallCounter > 4)
	{
		Npc_ClearAIQueue(self);
		AI_Standup(self);
		return LOOP_END;
	};
	if(Npc_GetStateTime(self) >= 1)
	{
		FireBurnSmallCounter += 1;
		Npc_SetStateTime(self,0);
		if((self.protection[PROT_FIRE] != IMMUNE) && (SPL_PYRO_DAMAGE_PER_SEC > FireProtection))
		{
			B_MagicHurtNpc(other,self,FireBurnSmallDamage);
		}
		else
		{
			Npc_ClearAIQueue(self);
			AI_Standup(self);
			return LOOP_END;
		};
		if(self.attribute[ATR_HITPOINTS] <= 0)
		{
			Npc_ClearAIQueue(self);
			AI_Standup(self);
			return LOOP_END;
		};
		return LOOP_CONTINUE;
	};
	return LOOP_CONTINUE;
};

func void ZS_FireBurnSmall_End()
{
};

