
func int B_TeachPlayerTalentMakeArrows(var C_Npc slf,var C_Npc oth,var int arrow)
{
	var int kosten;
	var int money;
	kosten = B_GetLearnCostTalent(oth,NPC_TALENT_MakeArrows,arrow);
	money = kosten * 50;
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLearnPoints,-1,-1,FONT_ScreenSmall,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	Log_CreateTopic(TOPIC_MakeArrows,LOG_NOTE);
	B_LogEntry(TOPIC_MakeArrows,"������ � ����:");
	if(arrow == Make_Arrow)
	{
		player_talent_makearrow[0] = TRUE;
		B_LogEntry(TOPIC_MakeArrows,"...��������� ������� ������ � �����.");
	};
	if(arrow == Make_FireArrow)
	{
		player_talent_makearrow[1] = TRUE;
		B_LogEntry(TOPIC_MakeArrows,"...��������� �������� ������ � �����.");
	};
	if(arrow == Make_MagicArrow)
	{
		player_talent_makearrow[2] = TRUE;
		B_LogEntry(TOPIC_MakeArrows,"...��������� ���������� ������ � �����.");
	};
	if(arrow == Make_PoisonArrow)
	{
		player_talent_makearrow[5] = TRUE;
		B_LogEntry(TOPIC_MakeArrows,"...��������� ����������� ������ � �����.");
	};
	PrintScreen(PRINT_LearnTakeAnimalTrophy,-1,-1,FONT_Screen,2);
	Npc_SetTalentSkill(oth,NPC_TALENT_MakeArrows,1);
	return TRUE;
};

