
func int B_TeachPlayerTalentSmith(var C_Npc slf,var C_Npc oth,var int waffe)
{
	var int kosten;
	var int money;
	kosten = B_GetLearnCostTalent(oth,NPC_TALENT_SMITH,waffe);
	money = kosten * 100;
	if(oth.lp < kosten)
	{
		PrintScreen(PRINT_NotEnoughLearnPoints,-1,-1,FONT_ScreenSmall,2);
		B_Say(slf,oth,"$NOLEARNNOPOINTS");
		return FALSE;
	};
	if(Npc_HasItems(oth,ItMi_Gold) < money)
	{
		PrintScreen(Print_NotEnoughGold,-1,-1,FONT_Screen,2);
		B_Say(slf,oth,"$ShitNoGold");
		return FALSE;
	};
	oth.lp = oth.lp - kosten;
	Npc_RemoveInvItems(oth,ItMi_Gold,money);
	Log_CreateTopic(TOPIC_TalentSmith,LOG_NOTE);
	B_LogEntry(TOPIC_TalentSmith,"����� �������� ������, ������ ����� ��� ����� ����� �����. � ������ �������� ������� �� � ���� ���������� �����, � ����� ������� ����� �� ����������. ������ ������ �������� ������� ������ ����������, ��������� ������ ������ ��������.");
	if(waffe == WEAPON_Common)
	{
		player_talent_smith[WEAPON_Common] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '���������� ����' ��� �� ����� ������� ������������.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Special_01)
	{
		player_talent_smith[WEAPON_1H_Special_01] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '�������� ������� ����' ��� ����� 1 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_2H_Special_01)
	{
		player_talent_smith[WEAPON_2H_Special_01] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '���������� ������� ����' ��� ����� 2 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Special_02)
	{
		player_talent_smith[WEAPON_1H_Special_02] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '����������� ������� ����' ��� ����� 2 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_2H_Special_02)
	{
		player_talent_smith[WEAPON_2H_Special_02] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '�������� ���������� ������� ����' ��� ����� 3 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Special_03)
	{
		player_talent_smith[WEAPON_1H_Special_03] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '������� ������� ����' ��� ����� 3 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_2H_Special_03)
	{
		player_talent_smith[WEAPON_2H_Special_03] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '�������� ������� ������� ����' ��� ����� 4 ����� ����.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Special_04)
	{
		player_talent_smith[WEAPON_1H_Special_04] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '������� ������ ��������' ��� ����� 4 ����� ���� � 5 �������� ����� �������.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_2H_Special_04)
	{
		player_talent_smith[WEAPON_2H_Special_04] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"��� '�������� ������� ������ ��������' ��� ����� 5 ������ ���� � 5 �������� ����� �������.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Harad_01)
	{
		player_talent_smith[WEAPON_1H_Harad_01] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���������� ���.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Harad_02)
	{
		player_talent_smith[WEAPON_1H_Harad_02] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���������� ������� ���.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Harad_03)
	{
		player_talent_smith[WEAPON_1H_Harad_03] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ��������� ������.");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == WEAPON_1H_Harad_04)
	{
		player_talent_smith[WEAPON_1H_Harad_04] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"����� ������� ��� ������ ����� '��� ��������' - ������ �� ������ ������� � ����!");
		PrintScreen(PRINT_LearnSmith,-1,-1,FONT_Screen,2);
	};
	if(waffe == Weapon_sharpen)
	{
		player_talent_smith[13] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ ������ ���� ������� ������� ������");
		PrintScreen(PRINT_LearnSmith4,-1,-1,FONT_Screen,2);
	};
	if(waffe == Armor_Mil_L2)
	{
		player_talent_smith[14] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���� ������ ������ ���������. ���������� ��� ��������� �������: ��������� ����������� �����, 3 ������� ����� � 1 ���� ��������.");
		PrintScreen(PRINT_LearnSmith2,-1,-1,FONT_Screen,2);
	};
	if(waffe == Armor_MIl_L3)
	{
		player_talent_smith[15] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���� ������������ ������ ������ ���������. ���������� ��� ��������� �������: 3 ��������� ����������� �����, 5 ������� ����� � 2 ���� ��������.");
		PrintScreen(PRINT_LearnSmith2,-1,-1,FONT_Screen,2);
	};
	if(waffe == Armor_Mil_M2)
	{
		player_talent_smith[16] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���� ������� ������ ���������. ���������� ��� ��������� �������: 5 ��������� ����������� �����, 2 ������� ����� � 2 ���� ��������.");
		PrintScreen(PRINT_LearnSmith2,-1,-1,FONT_Screen,2);
	};
	if(waffe == Armor_Mil_M3)
	{
		player_talent_smith[17] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� �������� ���� ������������ ������� ������ ���������. ���������� ��� ��������� �������: 7 ��������� ����������� �����, 3 ������� ����� � 3 ���� ��������.");
		PrintScreen(PRINT_LearnSmith2,-1,-1,FONT_Screen,2);
	};
	if(waffe == Armor_Mil_Gv)
	{
		player_talent_smith[18] = TRUE;
		B_LogEntry(TOPIC_TalentSmith,"������ � ���� ������� ������ ���������. ���������� ��� ����� �������: 12 ��������� ����������� ����� � 2 ����� ����.");
		PrintScreen(PRINT_LearnSmith3,-1,-1,FONT_Screen,2);
	};
	Npc_SetTalentSkill(oth,NPC_TALENT_SMITH,1);
	return TRUE;
};

