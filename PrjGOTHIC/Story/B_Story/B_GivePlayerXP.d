
var int LevelUpsDuringTransform;

func void B_GivePlayerXP(var int add_xp)
{
	var string concatText;
	if(hero.level == 0)
	{
		hero.exp_next = 500;
	};
	hero.exp += add_xp;
	concatText = PRINT_XPGained;
	concatText = ConcatStrings(concatText,IntToString(add_xp));
	PrintScreen(concatText,-1,YPOS_XPGained,FONT_ScreenSmall,2);
	if(hero.exp >= hero.exp_next)
	{
		hero.level += 1;
		if(PlayerIsTransformed == TRUE)
		{
			LevelUpsDuringTransform += 1;
		};
		hero.exp_next += (hero.level + 1) * 500;
		if(normal == TRUE)
		{
			hero.attribute[ATR_HITPOINTS_MAX] += HP_PER_LEVEL;
			hero.attribute[ATR_HITPOINTS] += HP_PER_LEVEL;
		};
		hero.lp += LP_PER_LEVEL;
		PrintScreen(PRINT_LevelUp,-1,YPOS_LevelUp,FONT_Screen,2);
		Snd_Play("LevelUp");
	};
	B_CheckLog();
};

func void B_GiveDeathXP(var C_Npc Killer,var C_Npc victim)
{
	if((Npc_IsPlayer(Killer) || ((Killer.aivar[AIV_PARTYMEMBER] == TRUE) && !Npc_IsPlayer(victim))) && (victim.aivar[AIV_VictoryXPGiven] == FALSE) && (victim.level != 0))
	{
		if(normal == TRUE)
		{
			B_GivePlayerXP(victim.level * 10);
			victim.aivar[AIV_VictoryXPGiven] = TRUE;
		}
		else
		{
			B_GivePlayerXP(victim.level * 5);
			victim.aivar[AIV_VictoryXPGiven] = TRUE;
		};
	};
};

