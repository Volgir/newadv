
class C_STATUSBAR
{
	var int posx;
	var int posy;
	var int sizex;
	var int sizey;
	var int value;
	var int maxrange;
	var string backtex;
	var string bartex;
	var string temptex;
	var int bShow;
};

class C_CRAFT
{
	var string reagent[6];
	var string out;
	var int skill;
	var int skillVal;
	var int id;
};

class Prs_StackPos
{
	var int pos;
};

instance StackPos(Prs_StackPos)
{
};


func void testWhile()
{
	var int size;
	var int loopStart;
	size = 0;
	loopStart = StackPos.pos;
	size += 9;
	if(size < 100)
	{
		StackPos.pos = loopStart;
	};
	printtext(ConcatStrings("Iter = : ",IntToString(size)),22,37,FONT_ScreenSmall,10,Hlp_Random(40));
};


prototype CRAFT(C_CRAFT)
{
	reagent[0] = "";
	reagent[1] = "";
	reagent[2] = "";
	reagent[3] = "";
	reagent[4] = "";
	reagent[5] = "";
	out = "";
	skill = 0;
	skillVal = 0;
	id = 0;
};

prototype StatusBar(C_STATUSBAR)
{
	posx = 0;
	posy = 0;
	sizex = 0;
	sizey = 0;
	value = 0;
	maxrange = 0;
	backtex = "";
	bartex = "";
	bShow = 0;
};

